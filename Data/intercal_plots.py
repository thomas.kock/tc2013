"""
Make plots for intercalibration data set of TIA probes used during cruise M160
"""

import glob, os
import numpy, netCDF4
import matplotlib.pyplot as plt

VARKEYS = ["Time", "Pressure", "TemperaturePT100", "Conductivity", "OxygenSatuSS", "FluoCycl"]

basepath = "/home/thomas/devl/tc2013/Data/m160/intercal"
ctm_files = glob.glob(os.path.join(basepath, "*.nc"))

data = {}
for f in ctm_files:
    with netCDF4.Dataset(f, "r") as ds:
        sn_idx = f.find("CTM") + 3
        probe_sn = int(f[sn_idx:sn_idx+4].strip('_'))
        print(f"Loading data of CTM{probe_sn}")
        if probe_sn not in data.keys():
            data[probe_sn] = numpy.zeros((len(VARKEYS), ds.dimensions["time"].size))
        for i, k in enumerate(VARKEYS):
            try:
                data[probe_sn][i] = ds.variables[k][:]
            except KeyError:
                data[probe_sn][i] = numpy.nan
                print(f"CTM{probe_sn} does not have {k} Data, this might be normal")
print("Data loaded")

f, a = plt.subplots(2,3, figsize=(16,10), dpi=100)
for k, v in data.items():
    a[0,0].plot(v[0], v[1], label=k)
    a[0,1].plot(v[0], v[2], label=k)
    a[1,0].plot(v[0], v[3], label=k)
    a[1,1].plot(v[0], v[4], label=k)
    a[0,2].plot(v[0], v[5], label=k)
    a[1,2].scatter(v[2], v[1])

plt.show()