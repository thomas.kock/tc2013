"""
Script to produce plots about the evolution of data quality for a sample TIA transect. The 
collection of functions is tailormade for this purpose and probably not reuseable somewhere
else.

Author: Thomas Kock
E-Mail: thomas.kock@hereon.de
Date: 2022-09-23

Notes:

Overall Idea is to produce a visualisation of improvement over the course of post processing

Uses two data sets:
1. more or less raw NC files, i.e. timeseries from each CTD, despiked
2. Fully processed data with correct time stamping and aperture correction

For set 1: Need to grid the data to 1 s intervals. Once the gridding is done data will be saved
in netCDF4 format, but without any attributes or explanations!

"""

from typing import Tuple
import datetime
import os.path
import glob
import numpy
import netCDF4
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.dates as md
import cmocean
import scipy.interpolate as si

plt.rcParams["image.cmap"] = cmocean.cm.thermal
plt.rcParams["interactive"] = True
plt.rcParams["font.size"] = 18

T0 = datetime.datetime(2019,12,13,14,20, tzinfo=datetime.timezone.utc)
T1 = datetime.datetime(2019,12,13,14,50, tzinfo=datetime.timezone.utc)
T_EPOCH = datetime.datetime(1970,1,1,0, tzinfo=datetime.timezone.utc)

BASEPATH = "/home/thomas/devl/tc2013/Data/tia_evolve"
PATH_SET_RAW = "NC"
PATH_SET_PROC = "20220627_delayedQC_ClockRosIntCal"

VARS = {
    "Time": 0,
    "Latitude": 1,
    "Longitude": 2,
    "Pressure": 3,
    "TemperaturePT100": 4,
    "SalinityPT100": 5,
    "SigmaPT100": 6
    }

nvars = len(VARS.keys())

def load_raw(path_to_set: str) -> numpy.ndarray:
    all_files = glob.glob(os.path.join(path_to_set, "*.nc"))
    n_prbs = len(all_files)
    n_samples = 0
    for f in all_files:
        with netCDF4.Dataset(f, "r") as ds:
            dim_t_size = ds.dimensions["time"].size
        if dim_t_size > n_samples:
            n_samples = dim_t_size
    data = numpy.zeros((n_prbs, nvars, n_samples))
    for i, f in enumerate(all_files):
        with netCDF4.Dataset(f, "r") as ds:
            dim_t_size = ds.dimensions["time"].size
            for var, idx in VARS.items():
                data[i, idx, 0: dim_t_size] = ds.variables[var][:].data
    return data

def harmonize_raw(data: numpy.ndarray) -> numpy.ndarray:
    # create a pressure lookup table
    n_prbs = data.shape[0]
    ps = []
    for i in range(n_prbs):
        press = numpy.ma.masked_equal(data[i, VARS["Pressure"]], 0)
        ps.append(int(press.mean() * 100))
    ps.sort()
    # get timerange of whole data set
    tmin = numpy.ma.masked_equal(data[:,0], 0).min()
    tmax = numpy.ma.masked_equal(data[:,0], 0).max()
    n_seconds = int((tmax - tmin) * 24 * 3600)
    h_data = numpy.zeros((n_prbs, nvars, n_seconds))
    h_data[:,0] = numpy.linspace(tmin, tmax, n_seconds)
    nt = h_data[0,0]
    # apply to all probes
    for prb_idx in range(n_prbs):
        print(f"\rProbe {prb_idx+1} of {n_prbs}", end="")
        zeros = numpy.where(data[prb_idx, 0] == 0)[0]
        if zeros.any():
            end_of_data_idx = zeros[0]
        else:
            end_of_data_idx = -1
        ot = data[prb_idx, 0, 0:end_of_data_idx]
        digitized = numpy.digitize(ot, nt)
        start_time_old = ot[digitized[0]]
        start_time_old_idx_in_nt = 0
        while nt[start_time_old_idx_in_nt] < start_time_old:
            start_time_old_idx_in_nt += 1

        prb_press_val = int(numpy.ma.masked_equal(data[prb_idx, VARS["Pressure"]], 0).mean() * 100)
        pidx = ps.index(prb_press_val)
        for j, uidx in enumerate(numpy.unique(digitized)):
            for var_j in list(VARS.values())[1:]:
                h_data[pidx, var_j, start_time_old_idx_in_nt + j - 1] = numpy.mean(data[prb_idx, var_j, numpy.where(digitized == uidx)[0]])
    print()
    return h_data

def ncdump_raw(harmonized_data: numpy.ndarray) -> None:
    dim_nprbs, _, dim_time_size = harmonized_data.shape
    with netCDF4.Dataset(os.path.join(BASEPATH, "harmonized_set1.nc"), "w") as ds:
        ds.createDimension("Time", dim_time_size)
        ds.createDimension("NProbes", dim_nprbs)
        for k, v in VARS.items():
            ds.createVariable(k, "f8", ("NProbes", "Time"))
            ds.variables[k][:,:] = harmonized_data[:,v]
    return 0

def ncload_raw(path: str) -> numpy.ndarray:
    with netCDF4.Dataset(path, "r") as ds:
        nprbs = ds.dimensions["NProbes"].size
        ntime = ds.dimensions["Time"].size
        nvars = len(ds.variables.keys())
        data = numpy.zeros((nprbs, nvars, ntime), dtype="f8")
        for i, (k, v) in enumerate(ds.variables.items()):
            data[:,i] = ds.variables[k][:,:].data
    return data

def ncload_fullchain(path_to_nc_dir: str) -> numpy.ndarray:
    files = glob.glob(os.path.join(path_to_nc_dir, "*.nc"))
    depths = []
    nprbs = len(files)
    fidx = list(range(nprbs))
    dim_time = 0
    for f in files:
        with netCDF4.Dataset(f, "r") as ds:
            if not dim_time:
                dim_time = ds.dimensions["time"].size 
            p = int(numpy.nanmean(ds.variables["Pressure"][:].data) * 100)
            depths.append(p)
    fidx = [i for _, i in sorted(zip(depths, fidx))]
    data = numpy.zeros((nprbs, nvars, dim_time), dtype="f8")
    for arr_idx, fi in enumerate(fidx):
        with netCDF4.Dataset(files[fi], "r") as ds:
            for k, v in VARS.items():
                data[arr_idx, v] = ds.variables[k][:]
    data[:,3][numpy.isnan(data[:,3])] = 0
    return data

def get_timerange_indeces(data: numpy.ndarray, t0: datetime.datetime, t1: datetime.datetime) -> Tuple[float, float]:
    td = t0 - T_EPOCH
    t0 = td.days + td.seconds / 86400 + td.microseconds / (86400 * 1E6)
    td = t1 - T_EPOCH
    t1 = td.days + td.seconds / 86400 + td.microseconds / (86400 * 1E6)
    time = data[0,0]
    i0 = i1 = 0
    while time[i0] < t0:
        i0 += 1
    i1 = i0
    while time[i1] < t1:
        i1 += 1
    return i0, i1

def interpolate_set(data: numpy.ndarray, t0: float, t1: float, ninterp: int = 10000) -> Tuple[numpy.ndarray, numpy.ndarray]:
    interp_ts = numpy.linspace(t0, t1, ninterp)
    nprbs = data.shape[0]
    data_interp = numpy.zeros((nprbs, nvars, ninterp), dtype="f8")
    for i in range(nprbs):
        for j in range(nvars):
            f_interp_data = si.interp1d(data[i,0], data[i, j])
            data_interp[i, j] = f_interp_data(interp_ts)
    return interp_ts, data_interp

if __name__ == "__main__":

    print(f"Using matplotlib backend: {matplotlib.get_backend()}")

    td = T0 - T_EPOCH
    t0 = td.days + td.seconds / 86400
    td = T1 - T_EPOCH
    t1 = td.days + td.seconds / 86400

    var = "TemperaturePT100"    

    path_to_ncraw = os.path.join(BASEPATH, "harmonized_set1.nc")
    if os.path.isfile(path_to_ncraw):
        print("Found parsed netCDF file for raw probe data set")
        raw = ncload_raw(path_to_ncraw)
    else:
        print("Loading raw data set from probe netCDFs, this may take a while")
        ds1 = load_raw(os.path.join(BASEPATH, PATH_SET_RAW))
        print("Harmonizing data set")
        raw = harmonize_raw(ds1)
        ncdump_raw(raw)

    proc = ncload_fullchain(os.path.join(BASEPATH, PATH_SET_PROC))
    sets = [raw, proc]
    lims = []
    for s in sets:
        lims.append(get_timerange_indeces(s, T0, T1))
    
    # compute interpolation in desired range for later comparison
    interps = []
    for s in sets:
        interp_ts, set_interp = interpolate_set(s, t0, t1)
        interps.append(set_interp)

    # differences between various interpolated data sets to obtain estimates for the effect
    # each postprocessing step has. This may go into the appendix...
    deltas = interps[0] - interps[1]
   
    # making the main plot, several options are hardcoded to control how the plot looks like
    # precisely
    yticks = [1,5,10,15]
    vmin = 16
    vmax = 23
    probe_idx = list(range(1,18,1)) 
    
    fig = plt.figure(dpi=100, figsize=(16,12))
    gs = matplotlib.gridspec.GridSpec(40, 39)
    
    # plots data for raw CTD data over time with probe index instead of depth. uses n+1 data
    # points for x and y to get an accurate pcolormesh
    i0, i1 = lims[0]
    ax_raw = fig.add_subplot(gs[0:12,:37])
    map_temp = ax_raw.pcolormesh(
        sets[0][0, 0, i0:i1+1] - sets[0][0, 0, i0], 
        probe_idx, sets[0][:, VARS[var], i0:i1], 
        vmin=vmin, vmax=vmax, shading="flat"
        )
    ax_raw.set_ylim([1,16])
    ax_raw.invert_yaxis()
    ax_raw.set_ylabel("Probe Idx [-]")
    ax_raw.set_title("Raw CTD data - no corrections applied", loc="center")    

    # plots the fully processed data set in the second panel
    i0, i1 = lims[-1]
    ax_proc = fig.add_subplot(gs[14:26,:37])
    map_temp = ax_proc.pcolormesh(
        sets[-1][0, 0, i0:i1+1] - sets[-1][0, 0, i0], 
        probe_idx, sets[-1][:, VARS[var], i0:i1], 
        vmin=vmin, vmax=vmax, shading="flat"
        )
    ax_proc.set_ylim([1,16])
    ax_proc.invert_yaxis()
    ax_proc.set_ylabel("Probe Idx [-]")
    ax_proc.set_title("Processed TIA data - all corrections applied", loc="center")   

    # adds a colorbar for the temperature plots
    cbar_temp_ax = fig.add_subplot(gs[7:19, 38])
    cbar_temp = fig.colorbar(
        map_temp, cax=cbar_temp_ax,
        label="Temperature [°C]"
        )

    # plots the differences between raw and fully processed data into the third panel
    ax_diff = fig.add_subplot(gs[28:,:37])
    rawx = interp_ts - interp_ts[0]
    drawx = numpy.mean(rawx[1:] - rawx[:-1])
    xdata = numpy.zeros((rawx.shape[0] + 1,))
    xdata[:-1] = rawx - drawx
    xdata[-1] = rawx[-1] + drawx
    map_diff = ax_diff.pcolormesh(
        xdata, 
        probe_idx, 
        deltas[:, VARS[var]],
        vmin=-5, vmax=5,
        shading="flat", cmap=cmocean.cm.diff
        )
    ax_diff.set_ylim([1,16])
    ax_diff.invert_yaxis()
    ax_diff.set_ylabel("Probe Idx [-]")
    ax_diff.set_title("Difference between raw and processed data sets", loc="center")

    # colorbar for difference plot
    cbar_ax_diff = fig.add_subplot(gs[28:, 38])
    cbar_diff = fig.colorbar(
        map_diff, cax=cbar_ax_diff, 
        label="Temperature Difference [°C]"
        )

    # plot appearance
    ax_diff.xaxis.set_major_locator(md.AutoDateLocator())
    ax_diff.xaxis.set_major_formatter(md.DateFormatter("%-M"))
    ax_diff.set_xlabel("Minutes")

    # this bit mimics sharex=true when using gridspec instead of .subplots
    ticklocs = ax_diff.xaxis.get_ticklocs()
    ax_raw.xaxis.set_ticks(ticklocs)    
    ax_raw.xaxis.set_ticklabels([])
    ax_proc.xaxis.set_ticks(ticklocs)
    ax_proc.xaxis.set_ticklabels([])

    ax_raw.set_yticks(yticks)
    ax_proc.set_yticks(yticks)
    ax_diff.set_yticks(yticks)

    plt.subplots_adjust(left=0.05, right=0.94, top=0.96, bottom=0.06)
    # arrangement: left 0.05, bottom 0.06 right 0.94 top 0.96
    plt.savefig(os.path.join(BASEPATH, f"postpro_evolution.png"))
    
