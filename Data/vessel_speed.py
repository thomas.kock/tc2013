import datetime
import json
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import utm
import numpy

# file = "/home/thomas/devl/tc2013/Data/subex16/2306_NMEA.txt"
# file = "/home/thomas/devl/tc2013/Data/he517/HE517_Trimble_20180824_063202.log"
file = "/home/thomas/devl/tc2013/Data/he517/HE517_Trimble_20180829_124405.log"

with open(file, "r") as fid:
    lines = fid.readlines()

print(len(lines))

# times
# subex2016 2306
# t0 = datetime.datetime(2016,6,23,9,30)
# t1 = datetime.datetime(2016,6,23,12,30)

# he517 2408
# t0=datetime.datetime(2018,8,24,16,30)
# t1=datetime.datetime(2018,8,24,18,15)

#he517 2908
t0=datetime.datetime(2018,8,29,15,30)
t1=datetime.datetime(2018,8,29,16)

t = []
lat = []
lon = []

for line in lines:
    if "$GPRMC" in line.split(',')[0]:
        c = line.split(',')
        ts = datetime.datetime.strptime(f"{c[9]}T{c[1]}", "%d%m%yT%H%M%S.%f")
        if t0 < ts < t1:
            t.append(ts)
            lat.append(float(c[3][:2]) + float(c[3][2:])/60)
            lon.append(float(c[5][:3]) + float(c[5][3:])/60)
        else:
            continue
    else:
        continue

datenum = []
for ts in t:
    td = ts - datetime.datetime(1970,1,1,0)
    datenum.append(td.days + td.seconds/86400)

datenum = numpy.array(datenum)
e, n = utm.from_latlon(numpy.array(lat), numpy.array(lon))[:2]
ds = numpy.hypot(e[1:] - e[:-1], n[1:] - n[:-1])
spd = ds / ((datenum[1:] - datenum[:-1])*86400)

spd = numpy.concatenate((numpy.array([spd[0]]), spd))

speed_data = {}
speed_data["t0"] = t0.isoformat()
speed_data["t1"] = t1.isoformat()
speed_data["mean"] = numpy.nanmean(spd)
speed_data["variance"] = numpy.nanvar(spd)
speed_data["stddev"] = numpy.nanstd(spd)

with open(f"{file.split('.')[0]}_spd.json", "w") as fid:
    json.dump(speed_data, fid)

f, a = plt.subplots(1,2)
a[0].scatter(lon, lat, c=spd)
a[1].scatter(datenum, spd)
a[1].xaxis.set_major_formatter(mdates.DateFormatter("%m-%d\n%H:%M"))
plt.show()

