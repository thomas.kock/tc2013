"""
Single purpose module to compute the TIA shapes obtained in cruise HE517 in 2018

KT 2022-03
"""

import dataclasses
import datetime
import lib_tia_arrangement as lta
import utm
import netCDF4
import glob
import os
import json
import numpy
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import scipy.interpolate as si
import scipy.signal as ss

DATA_BASEPATH = "/home/thomas/devl/tc2013/Data/"

@dataclasses.dataclass
class TIA_Deployment():

    def __init__(
        self, tia_arrangement: lta.TIA_ARRANGEMENT, 
        datapath: str,
        autoload: bool = True,
        control_plots: bool = True
        ):
        self.tiaa = tia_arrangement()
        self.datapath = datapath
        self.ctm_data: numpy.ndarray
        if autoload: self.load_tia_data()
        if control_plots: self.control_plots()
        
    def load_tia_data(self, press: bool = True, georef: bool = True, filter_window: int = 101):
        """Load data from nc files and return data dict with pressure data"""
        self.ndims = 1
        if press: self.ndims += 1
        if georef: self.ndims += 2
        ncfiles = glob.glob(os.path.join(self.datapath, "*.nc"))
        ctm_data = {}
        for ncfile in ncfiles:
            filename = ncfile.split(os.path.sep)[-1]
            print(f"Working on file {filename}")
            id_idx = filename.find("CTM") + 3
            ctm = filename[id_idx:id_idx+4]
            if ctm[-1] == '_': ctm = ctm[:-1]
            ctm = int(ctm)
            with netCDF4.Dataset(ncfile, "r") as ds:
                if "Pressure" in ds.variables.keys():
                    p = "Pressure"
                elif "P" in ds.variables.keys():
                    p = "P"
                else:
                    p = ""
                    press = False
                    print("Could not find pressure data in default keys.")
                    print(f"Keys are: {ds.variables.keys()}")
                ctm_data[ctm] = numpy.zeros((ds.dimensions["time"].size, self.ndims))
                ctm_data[ctm][:,0] = ds.variables["Time"][:]
                if press: ctm_data[ctm][:,1] = ds.variables[p][:]
                if georef:
                    ctm_data[ctm][:,2] = ds.variables["Latitude"][:]
                    ctm_data[ctm][:,3] = ds.variables["Longitude"][:]
                    if not hasattr(self, "spd") and not numpy.isnan(ctm_data[ctm][2:]).any():
                        e, n = utm.from_latlon(ctm_data[ctm][:,2], ctm_data[ctm][:,3])[:2]
                        spd = numpy.hypot(e[1:] - e[:-1], n[1:] - n[:-1]) / ((ctm_data[ctm][1:,0] - ctm_data[ctm][:-1, 0]) * 24 * 3600)
                        self.spd = ss.savgol_filter(spd, filter_window, 3)
        self.ctm_data = ctm_data

    def control_plots(self, size: int = 2):
        if self.ndims == 2:
            self.cp, a = plt.subplots(1, 1, figsize=(16,10), dpi=100)
            for probe, vals in self.ctm_data.items():
                a.scatter(vals[:,0], vals[:,1], s=size, label=f"CTM{probe}")
        elif self.ndims == 4:
            self.cp, a = plt.subplots(1, 3, figsize=(16,10), dpi=100)
            for probe, vals in self.ctm_data.items():
                a[0].scatter(vals[:,0], vals[:,1], s=size, label=f"CTM{probe}")
            a[0].xaxis.set_major_formatter(mdates.DateFormatter("%m-%d\n%H:%M"))
            a[1].scatter(vals[:,3], vals[:,2], s=size, c=vals[:,0])
            if numpy.isnan(vals[:,2:]).any():
                print("Cannot compute vessel speed, missing data")
                a[1].text(0.5, 0.5, "Missing data", color="k", fontsize=32, verticalalignment="center", horizontalalignment="center", transform=a[1].transAxes)
                a[2].text(0.5, 0.5, "Missing data", color="k", fontsize=32, verticalalignment="center", horizontalalignment="center", transform=a[2].transAxes)
            else:
                a[2].scatter(vals[1:, 0], self.spd, s=size)
                a[2].xaxis.set_major_formatter(mdates.DateFormatter("%m-%d\n%H:%M"))
        else:
            raise ValueError(f"ndims is not 2 or 4, please check")
        plt.show()

    def show_time_limits(self):
        for k, v in self.ctm_data.items():
            print(f"Limits for probe CTM{k}:")
            print(f"{datetime.datetime(1970,1,1) + datetime.timedelta(v[0,0])}")
            print(f"{datetime.datetime(1970,1,1) + datetime.timedelta(v[-1,0])}")
        
    def compute_pv_means(self, t0: datetime.datetime = None, t1: datetime.datetime = None) -> dict:
        """Dumb mean function to compute average pressure from pressure dict"""
        self.t0 = t0
        self.t1 = t1
        t1970 = datetime.datetime(1970, 1, 1)
        if t0:
            td = t0 - t1970
            st0 = td.days + td.seconds/86400
        if t1:
            td = t1 - t1970
            st1 = td.days + td.seconds/86400
        p = {}
        p_stddev = {}
        velocity = []
        velocity_stddev = []
        sc = {}
        for k, v in self.ctm_data.items():
            i0 = 0
            i1 = -1
            if t0:
                while v[i0, 0] < st0:
                    i0 += 1
            if t1:
                while v[i1, 0] > st1:
                    i1 -= 1
            sc[k] = v.shape[0] + i1
            print(f"CTM{k}: i0={i0}, i1={i1} ({sc[k]})")
            p[k] = numpy.nanmean(v[i0:i1,1])
            p_stddev[k] = numpy.nanstd(v[i0:i1,1])
            if hasattr(self, "spd"):
                velocity.append(numpy.nanmean(self.spd[i0:i1]))
                velocity_stddev.append(numpy.nanstd(self.spd[i0:i1]))
        if all(sc[k] == v for v in sc.values()):
            self.p_sample_count = v.shape[0]
        else:
            self.p_sample_count = -1
        if velocity and velocity_stddev and hasattr(self, "spd"):
            if all(velocity[0] == _ for _ in velocity):
                self.velocity = velocity[0]
            if all(velocity_stddev[0] == _ for _ in velocity_stddev):
                self.velocity_stddev = velocity_stddev[0]
        else:
            self.velocity = numpy.nan
            self.velocity_stddev = numpy.nan
        self.mean_pressures = {k: v for v, k in sorted(zip(p.values(), p.keys()))}
        self.mean_pressur_stddev = {k: v for v, k in sorted(zip(p_stddev.values(), p_stddev.keys()))}
    
    def show_mean_pressures(self):
        print("Mean pressures of CTMs in selected time range")
        for k, v in self.mean_pressures.items():
            print(f"{k:4} -> {v:.2f}")

    def show_mean_velocity(self):
        print(f"Mean ship velocity during transect: {self.velocity:.2f} +/- {self.velocity_stddev:.2f} m/s")
    
    def show_sensor_distances(self):
        print("Along cable distances of CTMs")
        for k, v in self.tiaa.sensor_distances.items():
            print(f"CTM{k:4}: {v:.1f} m")

    def compute_sensor_displacement(self, l0: float = 0, p0: float = 0):
        dist = self.tiaa.sensor_distances
        l = [l0]
        p = [p0]
        for k, v in dist.items():
            l.append(v)
            p.append(self.mean_pressures[k])
        l = numpy.array(l)
        p = numpy.array(p)
        dl = l[1:] - l[:-1]
        dp = p[1:] - p[:-1]
        dx = numpy.zeros_like(p)
        dx[1:] = numpy.sqrt(dl**2 - dp**2)

        xstd = numpy.zeros_like(dx)
        mps = list(self.mean_pressur_stddev.values())
        for i in range(dp.shape[0]):
            xstd[i+1] = abs(numpy.sqrt((dl[i] + i * self.tiaa.l_error)**2 - (dp[i] + mps[i])**2) - dx[i+1])
        self.shape_data = numpy.stack((numpy.cumsum(dx), p, numpy.concatenate(([0], mps)), xstd))

    def interpolate_chain_shape(self, kind: str = "linear"):
        self.interp_kind = kind
        f = si.interp1d(
            self.shape_data[0], self.shape_data[1],
            kind=kind, assume_sorted=True, fill_value="extrapolate"
            )
        x0 = self.shape_data[0,0]
        x1 = self.shape_data[0,-1]
        xnew = numpy.linspace(x0, x1, int(x1 - x0) + 1)
        ynew = f(xnew)
        self.interp_shape_data = numpy.stack((xnew, ynew))
    
    def plot_chain_shape(self, interp: bool = True, saveplot: bool = True, error_bars: bool = True):
        f, a = plt.subplots(1, 1, figsize=(16, 10),  dpi=100)
        a.scatter(self.shape_data[0], self.shape_data[1], c='k', label="CTMs", s=5)
        a.set_ylabel("Depth [m]")
        a.set_xlabel("Distance from tow point [m]")
        a.grid(which="major", linestyle='dotted')
        a.set_title(
            f"{self.tiaa.cruise} on {self.tiaa.date}, t0={self.t0}, "
            f"t1={self.t1}, p_samples={self.p_sample_count}, "
            f"vessel_speed={self.velocity:.1f}+-{self.velocity_stddev:.1f} m/s")
        if interp:
            if not hasattr(self, "interp_shape_data"):
                self.interp_shape_data()
            a.plot(
                self.interp_shape_data[0], self.interp_shape_data[1], 
                linestyle="dashed", c="gray", label=f"chain shape {self.interp_kind}"
                )
        if error_bars:
            a.errorbar(
                self.shape_data[0], self.shape_data[1], yerr=self.shape_data[2], xerr=self.shape_data[3],
                fmt='.', ms=5, mfc='k', mec='k', ecolor='r', capsize=3, label=r"1$\sigma$ error"
                )
        a.invert_yaxis()
        f.legend()
        if saveplot:
            plt.savefig(os.path.join(self.datapath, f"Shape_{self.tiaa.cruise}_{self.tiaa.date}.png"))
        plt.show()
    
    def shape2json(self):
        sd_dims = ["x_displacement", "mean_press", "mean_press_stddev", "x_displacement_stddev"]
        json_dict = {}
        for i, d in enumerate(sd_dims):
            json_dict[d] = list(self.shape_data[i])
        with open(os.path.join(self.datapath, "shape_data.json"), "w") as fid:
            json.dump(json_dict, fid)
        print("Shape data saved to json file")
    
    def execute_defaults(self, t0=None, t1=None):
        self.show_time_limits()
        self.compute_pv_means(t0=t0, t1=t1)
        self.show_mean_pressures()
        self.show_mean_velocity()
        self.show_sensor_distances()
        self.compute_sensor_displacement()
        self.interpolate_chain_shape()
        self.plot_chain_shape()
        self.shape2json()


if __name__ == "__main__":
    
    m160 = TIA_Deployment(lta.M160_1312, "/home/thomas/devl/tc2013/Data/m160")
    m160.execute_defaults(t0=datetime.datetime(2019,12,13,14), t1=datetime.datetime(2019,12,13,16))

    se23 = TIA_Deployment(lta.SUBEX16_2306, "/home/thomas/devl/tc2013/Data/subex16/20160623")
    se23.execute_defaults(t0=datetime.datetime(2016,6,23,10,40), t1=datetime.datetime(2016,6,23,12,10))

    se27 = TIA_Deployment(lta.SUBEX16_2706, "/home/thomas/devl/tc2013/Data/subex16/20160627")
    se27.execute_defaults(t0=datetime.datetime(2016,6,27,10,33), t1=datetime.datetime(2016,6,27,12,20))

    he517_24 = TIA_Deployment(lta.HE517_2408, "/home/thomas/devl/tc2013/Data/he517/20180824")
    he517_24.execute_defaults(t0=datetime.datetime(2018,8,24,16,30), t1=datetime.datetime(2018,8,24,18,15))

    he517_29 = TIA_Deployment(lta.HE517_2908, "/home/thomas/devl/tc2013/Data/he517/20180829")
    he517_29.execute_defaults(t0=datetime.datetime(2018,8,29,15,30), t1=datetime.datetime(2018,8,29,16))
    
    tara = TIA_Deployment(lta.TARA_3008, "/home/thomas/devl/tc2013/Data/tara")
    tara.execute_defaults(t0=datetime.datetime(2021,8,31,5), t1=datetime.datetime(2021,8,31,16))

    
 