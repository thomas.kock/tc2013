"""
A collection of TIA arrangement file contents used for calculating the tow chain shape

2022-03-09, Kock
"""
import abc
import dataclasses
import datetime

@dataclasses.dataclass
class TIA_ARRANGEMENT(abc.ABC):
    """Abstract base class for any other TIA arangement"""
    
    def __init__(self):
        self.cruise: str
        self.date: datetime.date
        self.diameter: str
        self.cable_type: str
        self.depressor: str
        self.hooks: dict
        self.used_hooks: dict
        self.total_length: float
        self.flipped: bool
        self.l_error: float
    
    def _compute_cumul_dist(self):
        self.cumul_dist = {}
        for hook, dist in self.hooks.items():
            if hook == 1:
                self.cumul_dist[hook] = dist
            else:
                self.cumul_dist[hook] = dist + self.cumul_dist[hook - 1]
    
    def _compute_sensor_distances(self) -> dict:
        sdist = {}
        for sensor, hook in self.used_hooks.items():
            sdist[sensor] = self.cumul_dist[hook]
        shift = max(sdist.values()) - self.total_length
        if self.flipped:
            for k in sdist:
                sdist[k] = self.total_length - sdist[k] 
        else:
            shift = max(sdist.values()) - self.total_length
            for k in sdist:
                sdist[k] = sdist[k] - shift
        self.sensor_distances = {k: v for v, k in sorted(zip(sdist.values(), sdist.keys()))}
    
    def show_sensor_distances(self) -> None:
        print(f"{self.cruise}: Sensor distances along cable")
        if hasattr(self, "sensor_distances"):
            for k, v in self.sensor_distances.items():
                print(f"{k:4}: {v:.1f}")
        else:
            AttributeError("Sensor distances not yet computed")


class HE517_2408(TIA_ARRANGEMENT):

    def __init__(self):
        self.cruise = "HE517"
        self.date = datetime.date(2018, 8, 24)
        self.diameter = "6 mm"
        self.cable_type = "Dyneema"
        self.depressor = "Endeco"
        self.total_length = 409
        self.flipped = False
        self.l_error = 0.1
        self.hooks = {
            1: 15,
            2: 15,
            3: 15,
            4: 15,
            5: 15,
            6: 15,
            7: 15,
            8: 15,
            9: 15,
            10: 15,
            11: 15,
            12: 15,
            13: 15,
            14: 15,
            15: 15,
            16: 15,
            17: 15,
            18: 15,
            19: 15,
            20: 15,
            21: 15,
            22: 15,
            23: 15,
            24: 15,
            25: 15,
            26: 15,
            27: 15,
            28: 4
        }
        self.used_hooks = {
            985: 28,
            987: 20,
            988: 14,
            989: 10,
            993: 12,
            1112: 25,
            1113: 17,
            1114:  8,
            1116:  6,
            1118:  4
        }

        self._compute_cumul_dist()
        self._compute_sensor_distances()

class HE517_2908(TIA_ARRANGEMENT):

    def __init__(self):
        self.cruise = "HE517"
        self.date = datetime.date(2018, 8, 29)
        self.diameter = "6 mm"
        self.cable_type = "Dyneema"
        self.depressor = "Endeco"
        self.total_length = 160
        self.flipped = False
        self.l_error = 0.1
        self.hooks = {
            1: 15,
            2: 15,
            3: 15,
            4: 15,
            5: 15,
            6: 15,
            7: 15,
            8: 15,
            9: 15,
            10: 15,
            11: 15,
            12: 15,
            13: 15,
            14: 15,
            15: 15,
            16: 15,
            17: 15,
            18: 15,
            19: 15,
            20: 15,
            21: 15,
            22: 15,
            23: 15,
            24: 15,
            25: 15,
            26: 15,
            27: 15,
            28: 4
        }
        self.used_hooks = {
            985: 28,	
            987: 26,
            988: 24,
            989: 22,
            993: 23,
            1112: 27,
            1113: 25,
            1114: 21,
            1116: 20,
            1118: 19
        }
        self._compute_cumul_dist()
        self._compute_sensor_distances()

class SUBEX16_2306(TIA_ARRANGEMENT):

    def __init__(self):
        self.cruise = "Subex2016"
        self.date = datetime.date(2016, 6, 23)
        self.diameter = "5 mm"
        self.cable_type = "Dyneema"
        self.depressor = "BP orig"
        self.total_length = 125
        self.flipped = False
        self.l_error = 0.15
        self.hooks =  {
            1: 2.39,
            2: 4.43,
            3: 4.23,
            4: 4.29,
            5: 4.30,
            6: 4.20,
            7: 1.1,
            8: 4.36,
            9: 4.24,
            10: 4.30,
            11: 4.26,
            12: 2.37,
            13: 10.47,
            14: 10.30,
            15: 10.37,
            16: 10.32,
            17: 10.42,
            18: 10.55,
            19: 10.60,
            20: 6.73,
        }
        self.used_hooks = {
            993: 6,
            991: 20,
            990: 14,
            986: 17,
            1120: 13,
            1115: 9,
            1114: 11,
            1113: 15
        }
        self._compute_cumul_dist()
        self._compute_sensor_distances()

class SUBEX16_2706(TIA_ARRANGEMENT):

    def __init__(self):
        self.cruise = "Subex2016"
        self.date = datetime.date(2016, 6, 27)
        self.diameter = "5 mm"
        self.cable_type = "Dyneema"
        self.depressor = "BP orig"
        self.total_length = 125
        self.flipped = False
        self.l_error = 0.15
        self.hooks =  {
            1: 2.39,
            2: 4.43,
            3: 4.23,
            4: 4.29,
            5: 4.30,
            6: 4.20,
            7: 1.1,
            8: 4.36,
            9: 4.24,
            10: 4.30,
            11: 4.26,
            12: 2.37,
            13: 10.47,
            14: 10.30,
            15: 10.37,
            16: 10.32,
            17: 10.42,
            18: 10.55,
            19: 10.60,
            20: 6.73,
        }
        self.used_hooks = {
            993: 6,
            991: 20,
            990: 14,
            986: 17,
            1120: 13,
            1115: 9,
            1114: 11,
            1113: 15,
        }
        self._compute_cumul_dist()
        self._compute_sensor_distances()

class M160_1312(TIA_ARRANGEMENT):

    def __init__(self):
        self.cruise = "M160"
        self.date = datetime.date(2019, 12, 13)
        self.diameter = "7.6 mm"
        self.cable_type = "TIA Cable"
        self.depressor = "Hydrobios 70"
        self.total_length = 470
        self.flipped = True
        self.l_error = 0.3 #m
        self.hooks = {
            # acc. TKs campaign diary, matlab config files produce complex numbers, i.e. something is not correct
            1: 0,
            2: 80, #80,
            3: 20, #100,
            4: 30, #130,
            5: 10, #140,
            6: 20, #160,
            7: 20, #180,
            8: 10, #190,
            9: 10, #200,
            10: 10, #210,
            11: 10, #220,
            12: 10, #230,
            13: 10, #240,
            14: 20, #260,
            15: 10, #270,
            16: 20, #290,
            17: 10, #300,
            18: 20, #320,
            19: 30, #350,
            20: 20, #370,
            21: 20 #390
        }
        self.used_hooks = {
            984: 20,
            985: 1,
            986: 3,
            989: 13,
            993: 8,
            1112: 4,
            1114: 10,
            1115: 6,
            1117: 9,
            1120: 2,
            1573: 16,  
            1574: 14,
            1580: 7,
            1581: 11,
            1582: 18,
            1584: 5
        }

        self._compute_cumul_dist()
        self._compute_sensor_distances()

class TARA_3008(TIA_ARRANGEMENT):

    def __init__(self):
        self.cruise = "TARA Microbiomes"
        self.date = datetime.date(2021, 8, 30)
        self.diameter = "6 mm"
        self.cable_type = "Dyneema"
        self.depressor = "BP Klon"
        self.total_length = 125
        self.flipped = False
        self.l_error = 0.03 # Estimation M. Heineke
        self.hooks = {
            1: 15.49,
            2: 12.24,
            3: 10.47,
            4: 9.31,
            5: 8.46,
            6: 7.83,
            7: 7.3,
            8: 6.48,
            9: 5.48,
            10: 1.03,
        }
        self.used_hooks = {
            987: 9,
            988: 7,
            989: 5,
            993: 8,
            1115: 6,
            1116: 4,
            1119: 2,
            1580: 3,
            1584: 1
        }
        self._compute_cumul_dist()
        self._compute_sensor_distances() 

if __name__ == "__main__":
    
    he517_2408 = HE517_2408()
    he517_2408.show_sensor_distances()

    he517_2908 = HE517_2908()
    he517_2908.show_sensor_distances()

    subex_2306 = SUBEX16_2306()
    subex_2306.show_sensor_distances()

    subex_2706 = SUBEX16_2706()
    subex_2706.show_sensor_distances()

    m160_1312 = M160_1312()
    m160_1312.show_sensor_distances()

    tara_3008 = TARA_3008()
    tara_3008.show_sensor_distances()