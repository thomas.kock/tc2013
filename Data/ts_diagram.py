"""
A simple and mostly hardcoded script to produce a temperature-salinity diagram for Tara
Mission Microbiomes data. Uses CTD Program processed netcdf data sets as input.

Author: TK
EMail: thomas.kock@hereon.de
Date: 2022-07-01
"""

import os.path
import glob
import netCDF4
import numpy
import cmocean
import seawater
import datetime
import matplotlib.pyplot as plt

# set up and constants
plt.rcParams["font.size"] = 22
plt.rcParams["backend"] = "GTK4Agg"
plt.rcParams["interactive"] = 0
plt.rcParams["image.cmap"] = cmocean.cm.deep

T0 = datetime.datetime(2021,8,31,8, tzinfo=datetime.timezone.utc)
T1 = datetime.datetime(2021,8,31,18, tzinfo=datetime.timezone.utc)
T_EPOCH = datetime.datetime(1970,1,1,0, tzinfo=datetime.timezone.utc)

dt = T0 - T_EPOCH
t0 = dt.days + dt.seconds/86400
dt = T1 - T_EPOCH
t1 = dt.days + dt.seconds/86400

i0, i1 = 0, 0

basepath = "/home/thomas/devl/tc2013/Data/tara"
nc_glob = "*INTCAL.nc"

# get list of all files and load data
all_files = glob.glob(os.path.join(basepath, nc_glob))
data = {}
variables = []
for f in all_files:
    sstr_idx = f.find("CTM")
    psn_str = f[sstr_idx+3:sstr_idx+7]
    if psn_str[-1] == '_':
        psn = int(psn_str[:-1])
    else:
        psn = int(psn_str)
    print(f"Loading file for probe {psn}")
    data[psn] = {}
    with netCDF4.Dataset(f, "r") as ds:
        data[psn]["nsamples"] = ds.dimensions["time"].size
        for k,v in ds.variables.items():
            if "Quality" in k:
                continue
            if k not in variables:
                variables.append(k)
            data[psn][k] = numpy.asarray(v[:].data)
print("Found variables:")
print('\n'.join(variables))

# find the desired time window, see above for T0 and T1 datetime objects
ts = data[psn]["Time"]
while ts[i0] < t0 and i1 < data[psn]["nsamples"]:
    i0 += 1
i1 = i0
while ts[i1] < t1 and i1 < data[psn]["nsamples"]:
    i1 += 1

# find some minima and maxima used for plotting later
pmin = 999
pmax = -999
tmin = 999
tmax = -999
smin = 999
smax = -999
for k, kv in data.items():
    k_min = numpy.nanmin(kv["Pressure"][i0:i1])
    k_max = numpy.nanmax(kv["Pressure"][i0:i1])
    if k_min < pmin: pmin = k_min
    if k_max > pmax: pmax = k_max

    k_min = numpy.nanmin(kv["TemperaturePT100"][i0:i1])
    k_max = numpy.nanmax(kv["TemperaturePT100"][i0:i1])
    if k_min < tmin: tmin = k_min
    if k_max > tmax: tmax = k_max

    k_min = numpy.nanmin(kv["SalinityPT100"][i0:i1])
    k_max = numpy.nanmax(kv["SalinityPT100"][i0:i1])
    if k_min < smin: smin = k_min
    if k_max > smax: smax = k_max

fraction = 0.01
smax = smax * (1 + fraction)
smin = smin * (1 - fraction)
tmax = tmax * (1 + fraction)
tmin = tmin * (1 - fraction)

# generate data for the density contours
s, t = numpy.meshgrid(
    numpy.linspace(smin, smax, 100),
    numpy.linspace(tmin, tmax, 100),
)
dens = seawater.dens(s, t, 25) - 1000

# sort probes by median depths, useful for later colormapping.
psns = []
mean_pressures = []
for k, kv in data.items():
    psns.append(k)
    mean_pressures.append(int(numpy.nanmedian(kv["Pressure"]) * 100))
psns = [psn for _, psn in sorted(zip(mean_pressures, psns))]

# make the TS diagram
fig, ax = plt.subplots(1,1,figsize=(14,12), dpi=100)
qcs = ax.contour(s, t, dens, colors="darkgray", levels=[21,22,23])
ax.clabel(qcs, inline=1, manual=[(33.3,28.7), (34.43,28.25), (35.5,27.8)])
for psn in reversed(psns):
    print(f"Plotting probe {psn}")
    pcoll = ax.scatter(
        data[psn]["SalinityPT100"][i0:i1], 
        data[psn]["TemperaturePT100"][i0:i1], 
        c=data[psn]["Pressure"][i0:i1], 
        vmin=pmin, vmax=pmax, s=25
        )
ax.set_xlabel("Salinity [PSU]")
ax.set_ylabel("Potential Temperature [°C]")
fig.colorbar(pcoll, ax=ax, fraction=0.05, aspect=60, label="Pressure [dbar]")
plt.tight_layout()
fig_path = os.path.join(basepath, "TS_Diagram_depth.png")
plt.savefig(fig_path)
print(f"Figure saved: {fig_path}")
# plt.show()
