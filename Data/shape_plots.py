import os
import json
import numpy
import matplotlib.pyplot as plt
from lib_tia_arrangement import *

plt.rcParams["font.size"] = 18
plt.rcParams["lines.linewidth"] = 2
basepath = "/home/thomas/devl/tc2013/Data"

paths = [
    "subex16/20160623",
    "m160",
    "tara",
    "he517/20180824",
    "he517/20180829"
]

keys = [
    "s16",
    "m160",
    "tara",
    "he517_24",
    "he517_29"
]

s16 = SUBEX16_2306()
m160 = M160_1312()
tara = TARA_3008()
he517_24 = HE517_2408()
he517_29 = HE517_2908()

# styles - ropes and cables
dyneema5 = ':'
dyneema6 = '-.'
cable = '--'
#styles - ctm probes
ctm = '+'
icc = 's'
# styles - depressors
hydrobios = "D"
bp = '<'
bp_klon = '>'
endeco = 'p'


shape_data = {}

for path, key in zip(paths, keys):
    with open(os.path.join(basepath, path, "shape_data.json"), "r") as fid:
        shape_data[key] = json.load(fid)


# manual override of tow point height
shape_data["m160"]["mean_press"][0] = -2.6
shape_data["s16"]["mean_press"][0] = -1.6
shape_data["tara"]["mean_press"][0] = -2
shape_data["he517_24"]["mean_press"][0] = -2.4
shape_data["he517_29"]["mean_press"][0] = -2.4


f, a = plt.subplots(1, 1, figsize=(16,9), dpi=100)
# meteor
a.errorbar(
    shape_data["m160"]["x_displacement"],
    shape_data["m160"]["mean_press"],
    yerr=shape_data["m160"]["mean_press_stddev"],
    xerr=shape_data["m160"]["x_displacement_stddev"],
    fmt=f"{cable}{icc}",
    # lw=1,
    c='gray',
    ms=5,
    mfc="seagreen",
    mec="seagreen",
    # capsize=3,
    ecolor="lightgray",
    elinewidth=5,
    label=f"{m160.cruise}: {m160.total_length} m {m160.cable_type}, d={m160.diameter}, v=2.5 m/s",
    zorder=2.3
    )
# m160 depressor
a.scatter(
    shape_data["m160"]["x_displacement"][-1],
    shape_data["m160"]["mean_press"][-1],
    marker=hydrobios,
    s=200,
    c="seagreen",
    zorder=2.4
)
# subex
a.errorbar(
    shape_data["s16"]["x_displacement"],
    shape_data["s16"]["mean_press"],
    yerr=shape_data["s16"]["mean_press_stddev"],
    xerr=shape_data["s16"]["x_displacement_stddev"],
    fmt=f"{dyneema5}{ctm}",
    # lw=1,
    c='gray',
    ms=8,
    mfc="k",
    mec="k",
    # capsize=3,
    ecolor="lightgray",
    elinewidth=5,
    label=f"{s16.cruise}: {s16.total_length} m {s16.cable_type}, d={s16.diameter}, v=3.3 m/s"
    )
# subex depressor
a.scatter(
    shape_data["s16"]["x_displacement"][-1],
    shape_data["s16"]["mean_press"][-1],
    marker=bp,
    s=200,
    c="k",
    zorder=2.2
)

# tara
a.errorbar(
    shape_data["tara"]["x_displacement"],
    shape_data["tara"]["mean_press"],
    yerr=shape_data["tara"]["mean_press_stddev"],
    xerr=shape_data["tara"]["x_displacement_stddev"],
    fmt=f"{dyneema6}{ctm}",
    # lw=1,
    c='gray',
    ms=8,
    mfc="darkorange",
    mec="darkorange",
    # capsize=3,
    elinewidth=5,
    ecolor="lightgray",
    label=f"{tara.cruise}: {tara.total_length} m {tara.cable_type}, d={tara.diameter}, v=3.0 m/s"
    )
# tara depressor
a.scatter(
    shape_data["tara"]["x_displacement"][-1],
    shape_data["tara"]["mean_press"][-1],
    marker=bp_klon,
    s=200,
    c="darkorange",
    zorder=2.2
)

# he517_29
a.errorbar(
    shape_data["he517_29"]["x_displacement"],
    shape_data["he517_29"]["mean_press"],
    yerr=shape_data["he517_29"]["mean_press_stddev"],
    xerr=shape_data["he517_29"]["x_displacement_stddev"],
    fmt=f"{dyneema6}{ctm}",
    # lw=1,
    c='gray',
    ms=8,
    mfc="navy",
    mec="navy",
    # capsize=3,
    elinewidth=5,
    ecolor="lightgray",
    label=f"{he517_29.cruise}: {he517_29.total_length} m {he517_29.cable_type}, d={he517_29.diameter}, v=3.4 m/s"
    )
# he517_29 depressor
a.scatter(
    shape_data["he517_29"]["x_displacement"][-1],
    shape_data["he517_29"]["mean_press"][-1],
    marker=endeco,
    s=200,
    c="navy",
    zorder=2.2
)

# he517_29
a.errorbar(
    shape_data["he517_24"]["x_displacement"],
    shape_data["he517_24"]["mean_press"],
    yerr=shape_data["he517_24"]["mean_press_stddev"],
    xerr=shape_data["he517_24"]["x_displacement_stddev"],
    fmt=f"{dyneema6}{ctm}",
    # lw=1,
    c='gray',
    ms=8,
    mfc="navy",
    mec="navy",
    # capsize=3,
    elinewidth=5,
    ecolor="lightgray",
    label=f"{he517_24.cruise}: {he517_24.total_length} m, {he517_24.cable_type}, d={he517_29.diameter}, v=3.3 m/s"
    )
# he517_24 depressor
a.scatter(
    shape_data["he517_24"]["x_displacement"][-1],
    shape_data["he517_24"]["mean_press"][-1],
    marker=endeco,
    s=200,
    c="navy",
    zorder=2.2
)

# waterlevel
wlx = numpy.linspace(0,500,5000)
wly = 0.4* numpy.sin(wlx * 0.1)
a.plot(wlx, wly, c="royalblue", zorder=0.9)

# plot appearance
a.set_xlim([0, 500])
a.set_ylim([-10, 120])
a.grid(which="major", ls="dotted", color="lightgray")
a.set_xlabel("Distance from tow point [m]")
a.set_ylabel("Depth from surface [m]")
a.invert_yaxis()
a.set_aspect(1)
f.legend(loc='lower center', ncol=2, bbox_to_anchor=(0, 0, 1, 0.25))
plt.tight_layout()
plt.savefig(os.path.join(basepath, "Chain_Shapes4pub.png"))
plt.show()

