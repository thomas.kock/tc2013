""" 
Load force meter data and evaluate forces an 13th of December 2019 (M160)

KT - 2022-03-15
"""

import datetime
import glob
import os.path
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import netCDF4
import numpy
import utm
import scipy.signal as ss
import scipy.interpolate as si

# plot params
plt.rcParams["font.size"] = 18

# savatzki golay filter params
window = 101
polyorder = 3

# paths and files
filepath = "/home/thomas/devl/tc2013/Data/m160/force"
files = glob.glob(os.path.join(filepath, "force_13122019*.txt"))
write = True
# ensure sorting of input files
sk = [int(_.split('_')[-1].split('.')[0]) for _ in files]
files = [f for _, f in sorted(zip(sk, files))]
ctm_filepath = "/home/thomas/devl/tc2013/Data/m160/TIA_MT_20191213_Part1_CTM985_PostProcessing_INTCAL.nc"

# time constraints and epoch
t_epoch = datetime.datetime(1970,1,1)
t0 = datetime.datetime(2019,12,13,14)
t0_dn = (t0 - t_epoch).days + (t0 - t_epoch).seconds/86400
t1 = datetime.datetime(2019,12,13,16)
t1_dn = (t1 - t_epoch).days + (t1 - t_epoch).seconds/86400

# empty containers
t = []
force = []

# load force data
for file in files:
    i = 0
    with open(file, "r", encoding="cp1252") as fid:
        for line in fid:
            if i < 3:
                i += 1
            else:
                l = line.split(', ')
                ct = datetime.datetime.strptime(f"{l[0]}T{l[1]}", "%d.%m.%YT%H:%M:%S.%f")
                if t0 <= ct <= t1:
                    td = ct - t_epoch
                    t.append(td.days + td.seconds/86400 + td.microseconds/(1E6*86400))
                    force.append(float(l[2]) * 100) #daN
                i += 1
t = numpy.array(t) # in days
force = numpy.array(force)
delta_t_mean = numpy.nanmedian((t[1:] - t[:-1])*86400)

# load ctm data
with netCDF4.Dataset(ctm_filepath, "r") as ds:
    ctm = numpy.zeros((4, ds.dimensions["time"].size))  
    ctm[0] = ds.variables["Time"][:].data
    ctm[1] = ds.variables["Latitude"][:].data
    ctm[2] = ds.variables["Longitude"][:].data
    ctm[3] = ds.variables["Pressure"][:].data

# compute vessel speed
e, n = utm.from_latlon(ctm[1], ctm[2])[:2]
spd = numpy.concatenate(
    (
        numpy.array([0]),
        (numpy.hypot(e[1:] - e[:-1], n[1:] - n[:-1])) /
        ((ctm[0,1:] - ctm[0,:-1]) * 86400)
    )
)
spd[0] = spd[1]

# find time window indeces
i0 = 0
i1 = 0
while ctm[0, i0] < t0_dn:
    i0 += 1
while ctm[0, i1] < t1_dn:
    i1 += 1

# apply savatzki golay fitler to data
filtered_force = ss.savgol_filter(force, window, polyorder)
filtered_spd = ss.savgol_filter(spd, window, polyorder)

# compute correlations with respect to speed
f_interp_spd = si.interp1d(ctm[0], filtered_spd, kind="cubic")
interp_spd = f_interp_spd(t)
spd_force_corr = numpy.corrcoef(interp_spd, filtered_force)
ctm[3][numpy.isnan(ctm[3])] = 0
f_interp_press = si.interp1d(ctm[0], ctm[3], kind="cubic")
interp_press = f_interp_press(t)
press_force_corr = numpy.corrcoef(interp_spd, interp_press)
# correlation shifting, speed
force_coeffs = []
force_corr_max = -1
force_corr_max_idx = -1001
for i in range(-1000,1000, 1):
    cc = numpy.corrcoef(interp_spd[1000:20000], filtered_force[1000-i:20000-i])[0][1]
    force_coeffs.append(cc)
    if cc > force_corr_max: 
        force_corr_max = cc
        force_corr_max_idx = i
force_coeffs = numpy.array(force_coeffs)
# correlation shifting, pressure
press_coeffs = []
press_corr_min = 1
press_corr_min_idx = 1000
for i in range(-1000,1000, 1):
    cp = numpy.corrcoef(interp_spd[1000:20000], interp_press[1000-i:20000-i])[0][1]
    press_coeffs.append(cp)
    if cp < press_corr_min: 
        press_corr_min = cp
        press_corr_min_idx = i
press_coeffs = numpy.array(press_coeffs)
del (cc, cp)
print(f"Best speed correlation: {force_corr_max:.3f} at idx={force_corr_max_idx}")
print(f"Best press correlation: {press_corr_min:.3f} at idx={press_corr_min_idx}")
print("This translates into the time shifts:")
print(f"For speed dt={delta_t_mean * force_corr_max_idx}")
print(f"For pressue dt={delta_t_mean * press_corr_min_idx}")

zt_force = ((filtered_force - numpy.nanmin(filtered_force)) / (numpy.nanmax(filtered_force) - numpy.nanmin(filtered_force)) - 0.5 ) * 2
zt_spd = ((filtered_spd - numpy.nanmin(filtered_spd)) / (numpy.nanmax(filtered_spd) - numpy.nanmin(filtered_spd)) -0.5) * 2
zt_press = ((ctm[3, i0:i1] - numpy.nanmin(ctm[3, i0:i1])) / (numpy.nanmax(ctm[3, i0:i1]) - numpy.nanmin(ctm[3, i0:i1])) - 0.5) *2

# plot
tticks = [t0_dn + i * (20/(60*24)) for i in range(7)]
lw = 2
f, a = plt.subplots(1,1,figsize=(16,6), dpi=100, sharex=True)
# second time savgol filter only for displaying!
a.plot(ctm[0,i0:i1]-(3.7/86400), ss.savgol_filter(zt_spd, window, polyorder)[i0:i1], label="Vessel Speed", lw=lw, c="navy")
a.plot(t, zt_force, label="Force", c="darkorange", lw=lw, ls="-.")
a.plot(ctm[0,i0:i1]+(99/86400), zt_press, label="Pressure", lw=lw, c="seagreen", ls="--")
a.plot([ctm[0, i0], ctm[0, i1]], [0,0], ls='--', color="k", zorder=0.1)
a.set_xlim([t0_dn, t1_dn])
a.set_xticks(tticks)
a.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
a.set_xlabel("Time [hh:mm]")
a.set_ylabel("normalized Values")
a.set_yticks([-1,-0.5,0,0.5,1])
a.grid(which="major", ls="dotted", color="lightgray")
a.legend(loc="lower center", ncol=3, bbox_to_anchor=(0.5, 0))
f.tight_layout()
plt.savefig(os.path.join(filepath, "fpv_correlation.png"))
plt.show()

# write time and force into netcdf file
if write:
    with netCDF4.Dataset(os.path.join(filepath, "force_M160_TIA_Force_20191213_depl09.nc"), "w") as ds:
        ds.Conventions = "CF-1.8"
        ds.institution = "Helmholtz-Zentrum Hereon, Institute for Carbon Cycles, Germany"
        ds.title = "Force gauge data for Towed Instrument Array collected during Meteor cruise M160"
        ds.source = "in situ"
        ds.creation_date = datetime.datetime.now(tz=datetime.timezone.utc).isoformat()
        ds.originator = "Thomas Kock"
        ds.contact = "thomas.kock@hereon.de"
        ds.crs = "None"
        ds.comment = "Force data measured between Towed Intrument Array (TIA) and RV Meteor during cruise M160"
        ds.campaign = "Meteor 160, M160: TIA deployment 9"
        ds.carrier = "RV Meteor"
        ds.platform = "RV Meteor with experimental RT Towed Instrument Array"
        ds.profileType = "Timeseries"
        ds.processingSteps = "None"
        ds.StartTime = t0.isoformat()
        ds.EndTime = t1.isoformat()
        ds.history = "python3 force_data.py, self written script for loading and saving data"
        ds.geographicName = "Northern Central Atlantic, SE of Cape Verde"
        ds.castID = "TIA deployment 9"
        ds.PI = "Prof. Dr. Arne Koertzinger (Geomar)"
        ds.operator = "Thomas Kock"
        ds.contributor = "Dr. Paulo Calil, Daniel Blandfort"
        ds.imo_platform_code = "8411279"
        ds.license = "MIT"
        ds.institution_ID = "ROR: 03qjp1d79"
        
        ds.createDimension("Time", None)
        ds.createVariable("time", "f8", ("Time",))
        ds.createVariable("time_qf", "i1", ("Time",))
        ds.createVariable("force", "f8", ("Time",))
        ds.createVariable("force_qf", "i1", ("Time",))

        ds.variables["time"][:] = t
        ds.variables["time"].units = "Days since 1970-01-01 00:00"
        ds.variables["time"].standard_name = "time"
        ds.variables["time_qf"][:] = numpy.zeros_like(t, dtype="i1")
        ds.variables["time_qf"].units = "none"
        ds.variables["time_qf"].flag_values = "0, 1, 2, 3, 4, 5, 7, 8, 9"
        ds.variables["time_qf"].flag_meanings = "no_qc_performed good_data probably_good_data bad_data_that_are_potentially_correctable bad_data value_changed not_used nominal_value interpolated_value missing_value"
        ds.variables["time_qf"].standard_name = "quality_flag time"        
        ds.variables["force"][:] = force
        ds.variables["force"].units = "daN"
        ds.variables["force"].standard_name = "force"
        ds.variables["force_qf"][:] = numpy.zeros_like(force, dtype="i1")
        ds.variables["force_qf"].units = "none"
        ds.variables["force_qf"].standard_name = "quality_flag force"   
        ds.variables["force_qf"].flag_values = "0, 1, 2, 3, 4, 5, 7, 8, 9"
        ds.variables["force_qf"].flag_meanings = "no_qc_performed good_data probably_good_data bad_data_that_are_potentially_correctable bad_data value_changed not_used nominal_value interpolated_value missing_value"

