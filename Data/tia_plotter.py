""" 
Load TIA data sets (post processed) and make nice plots interactively

Written and maintained by TK@hereon

This module provides the tools for interactively producing plots of TIA data. Using ipython is
required. 

Because I used it a lot for producing the plots for a TIA publication the if __name__ block may
be cluttered with code snippets. Remove as necessary.

2022-03-21
    TODO
    ytype: pressure, probeidx -> diagnostics
    pcolormesh: think about cell corners again -> pydcp?
    handle NaN for oxy / chloro plots (contourf, pcolormesh)
    add at least some documentation
"""
DEBUG = True
if not DEBUG:
    try:
        from IPython import get_ipython
    except ModuleNotFoundError:
        raise ModuleNotFoundError("This plotting tool needs to run in IPython, please install first")

    if get_ipython().__class__.__name__ != "TerminalInteractiveShell":
        raise RuntimeError("Please use IPython to run.")

from copy import deepcopy
from typing import Union
import dataclasses
import glob
import os.path
import datetime
import gsw
import netCDF4
import numpy
import matplotlib
import matplotlib.pyplot as plt
import utm
import cmocean
import scipy.interpolate as si
import scipy.signal as ss
import sys

plt.rcParams["font.size"] = 18
plt.rcParams["grid.linestyle"] = ':'
plt.rcParams["grid.color"] = 'lightgray'
plt.rcParams["grid.linewidth"] = 1

plt.ion()

CMCMAPS = {
    4: cmocean.cm.thermal,
    5: cmocean.cm.haline,
    6: cmocean.cm.dense,
    7: cmocean.cm.ice,
    8: cmocean.cm.algae,
    9: cmocean.cm.matter
}

VAR_KEYS = {
    "Time": ["Time"],
    "Latitude": ["Latitude"],
    "Longitude": ["Longitude"],
    "Pressure": ["P", "Pressure"],
    "Temperature": ["T", "TemperaturePT100"],
    "Salinity": ["S", "SalinityPT100"],
    "Density": ["RHO", "SigmaPT100"],
    "O2": ["O2", "OxygenSatuSS"],
    "Fluorescence": ["Chla", "FluoCycl"],
    "Conductivity": ["Conductivity"]
}

VAR_IDX = {
    "Time": 0,
    "Latitude": 1,
    "Longitude": 2,
    "Pressure": 3,
    "Temperature": 4,
    "Salinity": 5,
    "Density": 6,
    "O2": 7,
    "Fluorescence": 8,
    "Conductivity": 9
}

@dataclasses.dataclass
class TIA_Data():
    """
    Dataclass representing a Tow Instrument Array data set.
    """

    cruise: str
    path2files: str
    dmg_conv_factor: float

    def __post_init__(self):
        """Runs follwing methods after init to bring the instance in the desired shape"""
        self.get_list_of_files()
        self.load_data_from_files()
        self.compute_dmg()

    def get_list_of_files(self) -> list:
        """
        Helper to get a list of all netCDF files associated with the cruise
        
        Beause for a TIA Probe order matters (pressure) the files are directly sorted
        by probe number with respect to increasing pressure
        """
        print(f"Getting list of files for cruise {self.cruise}")
        files = glob.glob(os.path.join(self.path2files, "*.nc"))
        probes = []
        mean_pressures = []
        for f in files:
            sn_idx = f.find("CTM") + 3
            sn = int(f[sn_idx:sn_idx+4].strip('_'))
            probes.append(sn)
            with netCDF4.Dataset(f, "r") as ds:
                nc_vars = ds.variables.keys()
                if "P" in nc_vars:
                    mean_pressures.append(
                        numpy.nanmean(ds.variables["P"][:].data)
                    )
                    print(f"{sn:4}: Found pressure data with key 'P'. Mean={mean_pressures[-1]:6.2f}")
                elif "Pressure" in nc_vars:
                    mean_pressures.append(
                        numpy.nanmean(ds.variables["Pressure"][:].data)
                    )
                    print(f"{sn:4}: Found pressure data with key 'Pressure'. Mean={mean_pressures[-1]:6.2f}")
                else:
                    print(f"{sn:4}: Could not find Pressure data in Dataset")
                    print("Keys:")
                    print('\n'.join(nc_vars))
                    raise KeyError("No Pressure data found")

        self.probe_order = [p for _,p in sorted(zip(mean_pressures, probes))]
        print("\nFound probe order from top to bottom:")
        self.tia_files = []
        for i, sn in enumerate(self.probe_order):
            print(f"{i+1:2}: {sn:4}")
            for fn in files:
                if str(sn) in fn:
                    self.tia_files.append(fn)
                    break
        self.n_probes = len(self.tia_files)
        print()
    
    def load_data_from_files(self):
        """Loads TIA data from TIA netCDF files"""
        print(f"Loading data files")
        self.data = None
        self.variables = []
        self.variables_units = []
        for i, f in enumerate(self.tia_files):
            print(f"Loading file: {f.split('/')[-1]}")
            with netCDF4.Dataset(f, "r") as ds:
                ds_keys = list(ds.variables.keys())
                keys = []
                units = []
                for vkeys in VAR_KEYS.values():
                    for v in vkeys:
                        if v in ds_keys:
                            keys.append(v)
                            units.append(ds.variables[v].units)
                            break
                ndata_types = len(keys)
                if not isinstance(self.data, numpy.ndarray):
                    self.data = numpy.zeros((ndata_types, self.n_probes, ds.dimensions["time"].size))
                for j, vn in enumerate(keys):
                    self.data[j,i,:] = ds.variables[vn][:].data
                    if vn not in ["Time", "Latitude", "Longitude", "Pressure"]:
                        self.data[j,i,:][ds.variables[f"{vn} Quality Flag"][:].data > 2] = numpy.nan
            self.variables.append(keys)
            self.variables_units.append(units)
        
        if all([_ == self.variables[0] for _ in self.variables]):
            self.variables = self.variables[0]
        else:
            raise ValueError("Not all netCDF files have the same Variable Names!")

        if all([_ == self.variables_units[0] for _ in self.variables_units]):
            self.variables_units = self.variables_units[0]
        else:
            raise ValueError("Not all netCDF files have the same Variable Units!")

        self.data[3][numpy.isnan(self.data[3])] = 0
        pmax = numpy.nanmax(self.data[3])
        pmax_rounded = round(pmax, -1)
        if pmax_rounded < pmax: pmax_rounded += 10
        self.press_lim = (0, pmax_rounded)
        print(f"Data set press limits: min={self.press_lim[0]}, max={self.press_lim[1]}\n")
    
    def compute_dmg(self, cf: float = 0) -> numpy.ndarray:
        """Helper to compute distance made good for the loaded TIA data set"""     
        if not cf:
            cf = self.dmg_conv_factor
        print(f"Computing distance made good using conversion factor {(1/cf):.3f} 1/m\n")
        e, n = utm.from_latlon(self.data[1,0], self.data[2,0])[:2]
        dd = numpy.zeros_like(self.data[1,0])
        dd[1:] = numpy.hypot(e[1:] - e[:-1], n[1:] - n[:-1])
        dd = numpy.cumsum(dd)
        self.dmg2d = numpy.tile(dd, self.n_probes).reshape(self.n_probes, -1) / cf

class TIA_plotter():
    """
    Plotter class for plotting TIA data and provide some interactive functionality beyond 
    zooming and panning
    """

    def __init__(self,
        tia: TIA_Data,
        var_idx: int,
        dt0: datetime.datetime,
        dt1: datetime.datetime,
        dt_epoch = datetime.datetime(1970,1,1),
        edge_offset: float = 2,
        marker_color: str = "k"
        ):
        
        self.tia = tia
        self.var_idx = var_idx
        self.dt0 = dt0
        self.dt1 = dt1
        self.dt_epoch = dt_epoch
        self.ax = {"shiptrack": None, "data": None, "cbar": None}
        self.kind = ""
        self.pressure_lines_drawn = False
        self.probe_labels_drawn = False
        self.draw_register = {}
        self.offset_idx = int(edge_offset / 100 * self.tia.data.shape[-1])
        self.depth_idx = 0
        self.mk = marker_color
        self.set_time_limits()
        self.set_variable()

    def __repr__(self):
        return f"TIA_plotter instance at {hex(id(self))}"

    def set_time_limits(self, dt0: Union[str, datetime.datetime, float] = "", dt1: Union[str, datetime.datetime, float] = ""):
        """Helper to determine the time limits"""
        if not dt0:
            dt0 = self.dt0
        if not dt1:
            dt1 = self.dt1
        if isinstance(dt0, str):
            dt0 = datetime.datetime.fromisoformat(dt0)
        if isinstance(dt1, str):
            dt1 = datetime.datetime.fromisoformat(dt1) 
        if isinstance(dt0, float):
            self.t0 = dt0
        else:
            self.t0 = (dt0 - self.dt_epoch).days + (dt0 - self.dt_epoch).seconds/86400
        if isinstance(dt1, float):
            self.t1 = dt1
        else:
            self.t1 = (dt1 - self.dt_epoch).days + (dt1 - self.dt_epoch).seconds/86400
        
        self._get_time_indeces()
        self._update_plots()
    
    def _get_time_indeces(self):
        """Helper to find the array indeces belonging to the time limits"""
        i0 = 0
        i1 = 0
        imax = self.tia.data.shape[2]
        while self.tia.data[0, 0, i0] < self.t0:
            i0 += 1
        while self.tia.data[0, 0, i1] < self.t1 and (i1+1) < imax:
            i1 += 1
        self.i0 = i0
        self.i1 = i1

    def set_variable(self, var_idx=-1, cmap=None):
        """Helper to set the variable to plot"""
        if isinstance(var_idx, str):
            try:
                var_idx = VAR_IDX[var_idx]
            except KeyError:
                print(f"Key must be in {', '.join(VAR_IDX.keys())}")
                raise KeyError(f"{var_idx} is not supported")
        if 0 <= var_idx <= 8:
            self.var_idx = var_idx
        if cmap:
            self.cmap = cmap
        else:
            self.cmap = CMCMAPS[self.var_idx]
        self.cut_off_cmap = self.cmap.copy()
        self.cut_off_cmap.set_over(color="lightgray")
        self.cut_off_cmap.set_under(color="lightgray")
        self.set_variable_minmax()
            
    def set_variable_minmax(self, vmin: float = 0, vmax: float = 0, update_plots: bool = True):
        """Set data value min and max either by hand or from data automatically"""
        if not vmin:
            self.vmin = numpy.nanmin(self.tia.data[self.var_idx,:,self.i0:self.i1])
        else:
            self.vmin = vmin
        if not vmax:
            self.vmax = numpy.nanmax(self.tia.data[self.var_idx,:,self.i0:self.i1])
        else:
            self.vmax = vmax
        print(f"Variable {list(VAR_IDX.keys())[self.var_idx]}: min={self.vmin:.2f}, max={self.vmax:.2f}\n")
        if update_plots:
            self._update_plots()

    def create_figure_with_gridspec(self, figsize: tuple = (21,7), dpi: int = 100):
        """Creates the figure instance and adds gridspec data"""
        self.f = plt.figure(constrained_layout=True, figsize=figsize, dpi=dpi)
        self.gs = matplotlib.gridspec.GridSpec(1, 100, figure=self.f)

    def add_shiptrack_to_plot(self, depth_idx: int = 0, start_marker: str = 'o', end_marker: str = 'X', marker_size: int = 150):
        """Plot a shiptrack with the surface data colored"""
        if isinstance(self.ax["shiptrack"], plt.Axes):
            self.ax["shiptrack"].cla()
        else:
            self.ax["shiptrack"] = self.f.add_subplot(self.gs[:,:27])
        if depth_idx:
            self.depth_idx = depth_idx
        surf_data = deepcopy(self.tia.data[self.var_idx, self.depth_idx])
        surf_data[:self.i0] = -1
        surf_data[self.i1:] = -1
        self.lat = self.tia.data[1,self.depth_idx]
        self.lon = self.tia.data[2,self.depth_idx]
        self.shiptrack = self.ax["shiptrack"].scatter(self.lon, self.lat, c=surf_data, vmin=self.vmin, vmax=self.vmax, cmap=self.cut_off_cmap, picker=True)
        self.shiptrack.set_pickradius(1)
        self.ax["shiptrack"].scatter(self.lon[self.i0], self.lat[self.i0], c=self.mk, marker=start_marker, s=marker_size)
        self.ax["shiptrack"].scatter(self.lon[self.i1], self.lat[self.i1], c=self.mk, marker=end_marker, s=marker_size)
        self.ax["shiptrack"].axis("equal")
        self.ax["shiptrack"].xaxis.set_major_formatter(matplotlib.ticker.FormatStrFormatter("%4.2f"))
        self.ax["shiptrack"].yaxis.set_major_formatter(matplotlib.ticker.FormatStrFormatter("%5.2f"))
        self.ax["shiptrack"].set_ylabel("Latitude")
        self.ax["shiptrack"].set_xlabel("Longitude")
        self.ax["shiptrack"].grid(which="both")
        self.ax["shiptrack"].text(
            0.05 * (96-28)/100, 0.03, "a", 
            color="k", verticalalignment="bottom", horizontalalignment="left", fontsize=32,
            transform=self.ax["shiptrack"].transAxes, 
            fontweight="bold", bbox=dict(facecolor='w')
            )

        def onpick(event):
            """
            Helper method to enable interactive start and end selction of transect data.

            Args:
                event (matplotlib.MouseEvent): See matplotlib docs for further details
            """
            mx, my = event.mouseevent.xdata, event.mouseevent.ydata
            dd = numpy.array(numpy.hypot(self.lon - mx, self.lat - my) * 1E6, dtype="i4")
            min_dd = numpy.nanmin(dd)
            idx = list(dd).index(min_dd)
            self.f.canvas.mpl_disconnect(self.cid)
            nt = self.tia.data[0,0,idx]
            ndt = self.dt_epoch + datetime.timedelta(days=nt)
            if idx < self.i0:
                self.set_time_limits(dt0=nt, dt1=self.tia.data[0,0,self.i1])
                print(f"New start time: {ndt.strftime('%Y-%m-%d T %H:%M')}")
            elif idx > self.i1:
                self.set_time_limits(dt0 = self.tia.data[0,0,self.i0], dt1=nt)
                print(f"New end time: {ndt.strftime('%Y-%m-%d T %H:%M')}")
            elif (idx - self.i0) < (self.i1 - idx):
                self.set_time_limits(dt0=nt, dt1=self.tia.data[0,0,self.i1])
                print(f"New start time: {ndt.strftime('%Y-%m-%d T %H:%M')}")
            else:
                self.set_time_limits(dt0 = self.tia.data[0,0,self.i0], dt1=nt)
                print(f"New end time: {ndt.strftime('%Y-%m-%d T %H:%M')}")
            plt.pause(0.001)

        self.cid = self.f.canvas.mpl_connect('pick_event', onpick)
    
    def scatter(
        self, 
        xdata: str = "dmg", 
        scatter_marker: str = '|', 
        scatter_size: int = 100
        ):
        """
        Adds a scatter plot for TIA data to the canvas.

        Args:
            xdata (str, optional): Determines the type of xdata, allowed values are 'dmg' and 
            'time'. Defaults to "dmg".
            offset_idx (int, optional): Set the number of samples before an after the desired
            data range. This will make the plots a little more pretty by creating some context
            with the whole data set. Defaults to 500.
            scatter_marker (str, optional): Type of marker. Defaults to '|'.
            scatter_size (int, optional): Size of one scatter marker. Defaults to 100.
        """
        self.kind = "Scatter"
        if isinstance(self.ax["data"], plt.Axes):
            self.ax["data"].cla()
        else:
            self.ax["data"] = self.f.add_subplot(self.gs[:,28:96])
        self.cfi0 = self.i0 - self.offset_idx
        self.cfi1 = self.i1 + self.offset_idx
        self.xdata = xdata

        if xdata == "dmg":
            self.x = self.tia.dmg2d[:, self.cfi0:self.cfi1] - self.tia.dmg2d[0, self.i0]
        elif xdata == "time":
            self.x = self.tia.data[0, :, self.cfi0:self.cfi1]
        else:
            raise ValueError(f"xtype {xdata} is not supported.")

        y = self.tia.data[3,:,self.cfi0:self.cfi1]
        c = self.tia.data[self.var_idx, :, self.cfi0:self.cfi1]

        self.datacurtain = []
        for i in range(self.tia.n_probes):
            print(f"Plotting probe {self.tia.probe_order[i]}, {i+1} of {self.tia.n_probes}")
            qcs = self.ax["data"].scatter(
                self.x[i], 
                y[i],
                c = c[i], 
                vmin = self.vmin, vmax = self.vmax, 
                marker = scatter_marker, 
                s = scatter_size, 
                cmap = self.cmap
                )
            self.datacurtain.append(qcs)

    def pcolormesh(
        self,  
        xdata: str = "dmg", 
        shading="nearest"
        ):
        """
        Adds a pcolormesh plot to the canvas.

        Args:
            xdata (str, optional): Sets type of xaxix data, either 'dmg' odr 'time'. 
            Defaults to "dmg".
            offset_idx (int, optional): Set the number of samples before an after the desired
            data range. This will make the plots a little more pretty by creating some context
            with the whole data set. Defaults to 500.
            shading (str, optional): Shading type of pcolormesh corners. Defaults to "nearest".
        """
        self.kind = "PCM"
        if isinstance(self.ax["data"], plt.Axes):
            self.ax["data"].cla()
        else:
            self.ax["data"] = self.f.add_subplot(self.gs[:,28:96])
        self.cfi0 = self.i0 - self.offset_idx
        self.cfi1 = self.i1 + self.offset_idx
        self.xdata = xdata
        if xdata == "dmg":
            self.x = self.tia.dmg2d[:, self.cfi0:self.cfi1] - self.tia.dmg2d[0, self.i0]
        elif xdata == "time":
            self.x = self.tia.data[0, :, self.cfi0:self.cfi1]
        
        y = self.tia.data[3,:,self.cfi0:self.cfi1]
        c = self.tia.data[self.var_idx, :, self.cfi0:self.cfi1]

        self.datacurtain = self.ax["data"].pcolormesh(
            self.x,
            y,
            c,
            vmin = self.vmin,
            vmax = self.vmax,
            cmap=self.cmap,
            shading=shading
        )

    def contourf(
        self,
        xdata: str = "dmg", 
        nlevels: int = 41
        ):
        """
        Adds a filled contour plot of TIA data to canvas

        Args:
            xdata (str, optional): Sets type of xaxix data, either 'dmg' odr 'time'. 
            Defaults to "dmg".
            offset_idx (int, optional): Set the number of samples before an after the desired
            data range. This will make the plots a little more pretty by creating some context
            with the whole data set. Defaults to 500.
            nlevels (int, optional): Number of contour level to use. Defaults to 41.
        """
        self.kind = "Contourf"
        if isinstance(self.ax["data"], plt.Axes):
            self.ax["data"].cla()
        else:
            self.ax["data"] = self.f.add_subplot(self.gs[:,28:96])
        self.cfi0 = self.i0 - self.offset_idx
        self.cfi1 = self.i1 + self.offset_idx
        self.xdata = xdata
        if xdata == "dmg":
            self.x = self.tia.dmg2d[:, self.cfi0:self.cfi1] - self.tia.dmg2d[0, self.i0]
        elif xdata == "time":
            self.x = self.tia.data[0, :, self.cfi0:self.cfi1]
        
        y = self.tia.data[3,:,self.cfi0:self.cfi1]
        c = self.tia.data[self.var_idx, :, self.cfi0:self.cfi1]
        levels = numpy.linspace(self.vmin, self.vmax, nlevels)

        self.datacurtain = self.ax["data"].contourf(
            self.x,
            y,
            c,
            cmap=self.cmap,
            levels = levels
        )
    
    def post_process_plot(
        self, 
        start_marker: str = 'o', 
        end_marker: str = 'X',  
        marker_size: int = 150, 
        interval: int = 15, 
        tfmt: str = "%H:%M"
        ):
        """
        Convenience method used to apply all eye candy and annotations to plots after changing
        them. This method should not be called directly but within other methods changing the
        plots.

        Args:
            start_marker (str, optional): Transect start marker type. Defaults to 'o'.
            end_marker (str, optional): Transect end marker type. Defaults to 'X'.
            marker_color (str, optional): Transect marker color. Defaults to 'k'.
            marker_size (int, optional): _Transect marker size. Defaults to 150.
            interval (int, optional): Interval used with xdata 'time' in minutes, not used with 
            xdata 'dmg'. Defaults to 15.
            tfmt (_type_, optional): Tick label format for xdata 'time'. Defaults to "%H:%M".
        """
        offset = self.i0 - self.cfi0
        self.ax["data"].scatter(self.x[0, offset], self.tia.data[3, self.depth_idx, self.i0], marker=start_marker, c=self.mk, s=marker_size, zorder=3.1)
        self.ax["data"].scatter(self.x[0, -offset], self.tia.data[3, self.depth_idx, self.i1], marker=end_marker, c=self.mk, s=marker_size, zorder=3.1)

        self.ax["data"].axvspan(self.x[0, 0], self.x[0, offset], color="white", alpha=0.75)
        self.ax["data"].axvspan(self.x[0, -offset], self.x[0, -1], color="white", alpha=0.75)

        self.ax["data"].grid(which="major", axis="x")
        self.ax["data"].set_ylim(self.tia.press_lim)
        self.ax["data"].invert_yaxis()
        self.ax["data"].set_ylabel(f"{self.tia.variables[3]} [{self.tia.variables_units[3]}]")
        self.ax["data"].set_xlim([self.x[0,0], self.x[0, -1]])
        if self.xdata == "dmg":
            self.ax["data"].set_xlabel("Along track distance [km]")
        elif self.xdata == "time":
            self.ax["data"].xaxis.set_ticks([self.t0 + i * interval/(24*60) for i in range(int((self.t1 - self.t0)*24*60 / interval) + 1)])
            self.ax["data"].xaxis.set_major_formatter(matplotlib.dates.DateFormatter(tfmt))
            self.ax["data"].set_xlabel(f"Date: {self.dt0.strftime('%Y-%m-%d')} - UTC {self.tia.variables[0]} [{tfmt.replace('%','')}]")
        self.ax["data"].text(
            0.05 * 27/100, 0.03, 
            "b", color="k", verticalalignment="bottom", horizontalalignment="left", fontsize=32, 
            transform=self.ax["data"].transAxes, 
            fontweight="bold", bbox=dict(facecolor='w')
            )
        if "pressure_lines" in self.draw_register and self.draw_register["pressure_lines"]:
            self.draw_pressure_lines()
        if "probe_labels" in self.draw_register and self.draw_register["probe_labels"]:
            self.draw_probe_labels()
        if "kline" in self.draw_register:
            print("Klines are not automatically redrawn, please do manually")
        if "isoline" in self.draw_register:
            print("Isolines are not automatically redrawn, please do manually")
    
    def add_cbar(self):
        """
        Convenience method for adding and decorating the colorbar
        """
        if isinstance(self.ax["cbar"], plt.Axes):
            self.ax["cbar"].cla()
        else:
            self.ax["cbar"] = self.f.add_subplot(self.gs[:,98:])
        if isinstance(self.datacurtain, list):
            m = self.datacurtain[-1]
        else:
            m = self.datacurtain
        self.cbar = self.f.colorbar(
            m, 
            cax=self.ax["cbar"], 
            format="%3.1f",
            cmap=self.cmap,
            label=f"{self.tia.variables[self.var_idx]} [{self.tia.variables_units[self.var_idx]}]")

    def set_cmap(self, cmap: cmocean.cm):
        """
        Convenience method to change the colormap of the plots

        Args:
            cmap (cmocean.cm): colormap of cmocean package
        """
        self.set_variable(cmap = cmap)
        if isinstance(self.datacurtain, list):
            for _ in self.datacurtain:
                _.set_cmap(self.cmap)
        elif isinstance(self.datacurtain, matplotlib.collections.QuadMesh):
            self.datacurtain.set_cmap(self.cmap)
        elif isinstance(self.datacurtain, matplotlib.contour.QuadContourSet):
            self.datacurtain.set_cmap(self.cmap)
        self.shiptrack.set_cmap(self.cut_off_cmap)
        if self.ax["cbar"]:
            self.add_cbar()

    def save_figure(self, path: str = ""):
        """
        Helper for plot figure saving

        Args:
            path (str, optional): If given, save figure to this path. Defaults to "".
        """
        varname = list(VAR_IDX.keys())[self.var_idx]
        plotname = f"STDAT_TIA_{self.tia.cruise.upper()}_{varname}_{self.kind}_{self.dt0.strftime('%Y%m%dT%H%M')}.png"
        if not path:
            path = os.path.join(self.tia.path2files, plotname)
        else:
            path = os.path.join(path, plotname)
        self.f.savefig(path)
        print(f"Figure saved as {path}")

    def pp(self):
        """Alias for self.post_process_plot method"""
        self.post_process_plot()
    
    def pcm(self):
        """Alias for self.pcolormesh method"""
        self.pcolormesh()

    def _update_plots(self):
        """
        Helper method to update existing plots after user change, for instance after handling
        mouse events
        """
        if isinstance(self.ax["shiptrack"], plt.Axes):
            self.add_shiptrack_to_plot()
        
        if isinstance(self.ax["data"], plt.Axes):
            if isinstance(self.datacurtain, list):
                self.scatter()
            elif isinstance(self.datacurtain, matplotlib.collections.QuadMesh):
                self.pcolormesh()
            elif isinstance(self.datacurtain, matplotlib.contour.QuadContourSet):
                self.contourf()
            self.post_process_plot()
        
        if isinstance(self.ax["cbar"], plt.Axes):
            self.add_cbar()
    
    def _update_data_plot(self):
        """Helper method to update only the data plot, i.e. data curtain"""
        if isinstance(self.ax["data"], plt.Axes):
            self.ax["data"].cla()
            if isinstance(self.datacurtain, list):
                self.scatter()
            elif isinstance(self.datacurtain, matplotlib.collections.QuadMesh):
                self.pcolormesh()
            elif isinstance(self.datacurtain, matplotlib.contour.QuadContourSet):
                self.contourf()
            self.post_process_plot()
    
    def draw_pressure_lines(self, lw: float = 0.3, color: str = 'w'):
        """
        Adds pressure line for each probe to existing data plot

        Args:
            lw (float, optional): Linewidth of pressure lines. Defaults to 0.3.
            color (str, optional): Color of pressure lines. Defaults to 'w'.
        """
        if isinstance(self.ax["data"], plt.Axes):
            print("Plotting pressure lines for all probes")
            for i in range(self.tia.n_probes):
                self.ax["data"].plot(
                    self.x[i], 
                    self.tia.data[3,i,self.cfi0:self.cfi1],
                    c=color, lw=lw
                    )
            self.draw_register["pressure_lines"] = True
        else:
            print("No data axes found, use methods scatter, pcolormesh or contourf first")

    def remove_pressure_lines(self):
        """
        Helper to remove all pressure lines from data plot
        """
        if (
            isinstance(self.ax["data"], plt.Axes) and
            self.draw_register["pressure_lines"] and
            self.kind
            ):
            self.draw_register["pressure_lines"] = False
            self._update_data_plot()
        else:
            print("No pressure lines were drawn or there is no data axes present")
    
    def draw_probe_labels(self, nlabels: int = 2, color: str = "r", fontsize: int = 10):
        """
        Adds probelabels to data plot. It is more of a debug feature to see which probe was at
        which depth. For final plots TIA data should be agnostoc of probe serial number

        Args:
            nlabels (int, optional): How many labels to draw across the data range 
            in x-direction. Defaults to 2.
            color (str, optional): Color of the probe label text. Defaults to "r".
            fontsize (int, optional): Fontsize of probe label text. Defaults to 10.
        """
        if isinstance(self.ax["data"], plt.Axes):
            print("Adding probe labels to data plot")
            offset = self.i0 - self.cfi0
            span = int((self.i1 - self.i0)/(nlabels + 1))
            for i in range(self.tia.n_probes):
                for j in range(1, nlabels + 1):
                    self.ax["data"].text(
                        self.x[i, offset + j * span], 
                        self.tia.data[3, i, self.i0 + j * span], 
                        str(self.tia.probe_order[i]), 
                        va = "center", ha = "center", 
                        color = color, 
                        fontweight = "bold", 
                        fontsize = fontsize
                        )
            self.draw_register["probe_labels"] = True
        else:
            print("No data axes found, use methods scatter, pcolormesh or contourf first")

    def remove_probe_labels(self):
        """
        Helper function to remove all probe labels and restore the previous state
        """
        if (
            isinstance(self.ax["data"], plt.Axes) and
            self.draw_register["probe_labels"] and
            self.kind
            ):
            self.ax["data"].cla()
            if self.kind == "Scatter":
                self.scatter()
            elif self.kind == "PCM":
                self.pcolormesh()
            elif self.kind == "Contourf":
                self.contourf()
            self.draw_register["probe_labels"] = False
            self.pp()
        else:
            print("No probe labels were drawn or there is no data axes present")

    def draw_kline(
        self, 
        var_idx: Union[int, str], 
        filterwindow: int = 151, 
        filterorder: int = 3
        ):
        """
        Add a xkline to data plot, where x is a variable, using the variable index for
        temperature will draw the thermokline, density the pycnokline, etc.

        The data for the kline will be smoothed using scipy.signal.savgol filter.

        Args:
            var_idx (Union[int, str]): Either an int as array index for the desired variable
            or its name as string
            filterwindow (int, optional): Window size of data filter. Defaults to 151.
            filterorder (int, optional): Polyorder of filter function. Defaults to 3.
        """
        if isinstance(var_idx, str):
            var_idx = VAR_IDX[var_idx]
        if isinstance(self.ax["data"], plt.Axes):
            print(f"Plotting the main {self.tia.variables[var_idx]}-kline")
            pyc_x = self.x[0]
            pyc_y = numpy.zeros_like(pyc_x)
            for i in range(pyc_x.shape[0]):
                dens = self.tia.data[6,:,self.cfi0+i]
                pres = self.tia.data[3,:,self.cfi0+i]
                gradients = numpy.array(numpy.gradient(dens, pres) * 1E6, dtype="i4")
                max_gradient_idx = numpy.where(gradients == gradients.max())[0][0]
                pyc_y[i] = pres[max_gradient_idx]
            pyc_y = ss.savgol_filter(pyc_y, filterwindow, filterorder)
            self.ax["data"].plot(pyc_x, pyc_y, c="k")
            self.draw_register["kline"] = True 
        else:
            print("No data axes found, use methods scatter, pcolormesh or contourf first")

    def remove_klines(self):
        """
        Helper method to remove all klines and restore the before data plot
        """
        if (
            isinstance(self.ax["data"], plt.Axes) and
            self.draw_register["kline"] and
            self.kind
            ):
            self.ax["data"].cla()
            if self.kind == "Scatter":
                self.scatter()
            elif self.kind == "PCM":
                self.pcolormesh()
            elif self.kind == "Contourf":
                self.contourf()
            self.draw_register["kline"] = False
            self.pp()
        else:
            print("No kline(s) were drawn or there is no data axes present")

    def draw_isoline(
        self,
        value: float, 
        var_idx: Union[int, str], 
        c: str = "w", 
        ls: str = "--", 
        label: str = "", 
        filterwindow: int = 151, 
        filterorder: int = 3
        ):
        """
        Adds an isoline to data plot for variable with var_idx with value. Make sure to choose 
        a value inside the data range for variable.

        Data for the isoline will be filtered prior to drawing using scipy.signal.savgol filter.

        Args:
            value (float): Value of the isoline
            var_idx (Union[int, str]): Array idx or name variable for isoline as string
            c (str, optional): Color of isoline. Defaults to "w".
            ls (str, optional): Linestyle of isoline. Defaults to "--".
            label (str, optional): Label of isoline for legend. Defaults to "".
            filterwindow (int, optional): Filterwindow for savgol filter. Defaults to 151.
            filterorder (int, optional): Filter polyorder for savgol filter. Defaults to 3.
        """
        if isinstance(var_idx, str):
            var_idx = VAR_IDX[var_idx]
        if isinstance(self.ax["data"], plt.Axes):
            print(f"Plotting isoline for {self.tia.variables[var_idx]} at {value:.2f} {self.tia.variables_units[var_idx]}")
            pyc_x = self.x[0]
            pyc_y = numpy.zeros_like(pyc_x)
            for i in range(pyc_x.shape[0]):
                valu = self.tia.data[var_idx,:,self.cfi0+i]
                pres = self.tia.data[3,:,self.cfi0+i]
                f_interp = si.interp1d(valu, pres, bounds_error=False, fill_value=numpy.nan)
                pyc_y[i] = f_interp(value)
            pyc_y = ss.savgol_filter(pyc_y, filterwindow, filterorder)
            if label:
                self.ax["data"].plot(pyc_x, pyc_y, c=c, ls=ls, label=label)
                self.ax["data"].legend(loc="lower right")
            else:
                self.ax["data"].plot(pyc_x, pyc_y, c=c, ls=ls)
            self.draw_register["isoline"] = True

        else:
            print("No data axes found, use methods scatter, pcolormesh or contourf first")
    
    def remove_isoline(self):
        """
        Removes all previously drawn isolines. Unspecific method, removes all isolines
        """
        if (
            isinstance(self.ax["data"], plt.Axes) and
            self.draw_register["isoline"] and
            self.kind
            ):
            self.ax["data"].cla()
            if self.kind == "Scatter":
                self.scatter()
            elif self.kind == "PCM":
                self.pcolormesh()
            elif self.kind == "Contourf":
                self.contourf()
            self.draw_register["isoline"] = False
            self.pp()
        else:
            print("No isoline(s) were drawn or there is no data axes present")

if __name__ == "__main__":

    PATHS = {
        "m160v0": "/home/thomas/devl/tc2013/Data/m160/2022-03-29",
        "m160v1" : "/home/thomas/devl/tc2013/Data/m160/Proc_20211109",
        "m160" : "/home/thomas/devl/tc2013/Data/m160/Proc_20220607",
        "tara": "/home/thomas/devl/tc2013/Data/tara",
        "dori": "/home/thomas/devl/tc2013/Data/dori"
    }
    TIMES = {
        "m160v0": (datetime.datetime(2019,12,13,14,30), datetime.datetime(2019,12,13,16)),
        "m160v1": (datetime.datetime(2019,12,13,14,30), datetime.datetime(2019,12,13,16)),
        "m160": (datetime.datetime(2019,12,13,14,30), datetime.datetime(2019,12,13,16)),
        "tara": (datetime.datetime(2021,8,31,8), datetime.datetime(2021,8,31,18)),
        "dori": (datetime.datetime(2022,8,11,11,4), datetime.datetime(2022,8,11,13,50))
    }

    if len(sys.argv) != 5:
        raise ValueError("Number arguments is not supported! Call with argument <int var> <str cruise> <str xdata> <str plttype>")
    else:
        VAR = int(sys.argv[1])
        cruise = sys.argv[2]
        xdata = sys.argv[3]
        plttype = sys.argv[4]

    if cruise == "dori":
        oxy_molmass = 31.9988 # g/mol
        tia = TIA_Data(cruise, PATHS[cruise], 1000)
        # compute expected o2 solubility
        o2sol_eos80 = gsw.O2sol_SP_pt(tia.data[5], tia.data[4])
        # convert to o2 concentration umol/kg and further to umol/L
        tia.data[7] = o2sol_eos80 * (tia.data[7] / 100) * ((tia.data[6] + 1000) / 1000)
        tp = TIA_plotter(tia, VAR, *TIMES[cruise], marker_color="firebrick")
        # need to change some variable names and units to suit o2 concentration
        tp.tia.variables[tp.var_idx] = "Oxygen contentration"
        tp.tia.variables_units[tp.var_idx] = "µmol/L"
        tp.create_figure_with_gridspec()
        tp.add_shiptrack_to_plot(depth_idx=7)
        if plttype.lower() in ["contourf", "contour", "c"]:
            tp.contourf(xdata=xdata, nlevels=31)
        elif plttype.lower() in ["pcm", "pcolormesh", "p"]:
            tp.pcolormesh(xdata=xdata)
        elif plttype.lower() in ["scatter", "s"]:
            tp.scatter(xdata=xdata)
        tp.post_process_plot()
        tp.add_cbar()
        tp.ax["cbar"].yaxis.set_major_formatter(matplotlib.ticker.StrMethodFormatter("{x:.0f}"))
        tp.ax["data"].set_xticks([0,2,4,6,8,10,12,14,16])
        tp.save_figure()

    elif cruise == "tara":
        tia = TIA_Data(cruise, PATHS[cruise], 1000)
        tp = TIA_plotter(tia, VAR, *TIMES[cruise], edge_offset=1)
        if tp.var_idx == 5:
            tp.tia.variables_units[tp.var_idx] = "PSU"
        tp.create_figure_with_gridspec()
        tp.add_shiptrack_to_plot(depth_idx=1)
        if plttype.lower() in ["contourf", "contour", "c"]:
            tp.contourf(xdata=xdata, nlevels=31)
        elif plttype.lower() in ["pcm", "pcolormesh", "p"]:
            tp.pcolormesh(xdata=xdata)
        elif plttype.lower() in ["scatter", "s"]:
            tp.scatter(xdata=xdata)
        tp.post_process_plot()
        tp.add_cbar()
        ticks = list(range(34, 37, 1))
        tp.ax["cbar"].set_yticks(ticks)
        tp.ax["cbar"].set_yticklabels(ticks)
        tp.save_figure()

    elif cruise == "m160":
        tia = TIA_Data(cruise, PATHS[cruise], 1000)
        tp = TIA_plotter(tia, VAR, *TIMES[cruise], edge_offset=0.5)
        if tp.var_idx == 5:
            tp.tia.variables_units[tp.var_idx] = "PSU"
        tp.create_figure_with_gridspec()
        tp.add_shiptrack_to_plot(depth_idx=0)
        if plttype.lower() in ["contourf", "contour", "c"]:
            tp.contourf(xdata=xdata, nlevels=31)
        elif plttype.lower() in ["pcm", "pcolormesh", "p"]:
            tp.pcolormesh(xdata=xdata)
        elif plttype.lower() in ["scatter", "s"]:
            tp.scatter(xdata=xdata)
        tp.post_process_plot()
        tp.add_cbar()
        tp.draw_isoline(26.2, 6, c="k", label="26.2 kg/m³ isopycnal", filterwindow=501)
        ticks = list(range(14, 24, 2))
        tp.ax["cbar"].set_yticks(ticks)
        tp.ax["cbar"].set_yticklabels(ticks)
        tp.ax["data"].set_xticks(list(range(0,13,2)))
        tp.save_figure()

    else:
        print(f"Cruise {cruise} is not part of the paper, so there is no code here for it. Sorry")