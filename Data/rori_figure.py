import json
import scipy.io as sio
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rcParams["font.size"] = 18
matplotlib.rcParams["lines.markersize"] = 12

plt.ion()

path_to_fig_file = "/home/thomas/devl/tc2013/Data/ppro/M160_SigmaVertProf.fig"

fc = sio.loadmat(path_to_fig_file, squeeze_me=True, struct_as_record=False)

# no_intercal
x1 = fc["hgS_070000"].children[0].children[0].properties.XData
y1 = fc["hgS_070000"].children[0].children[0].properties.YData

# intercal
x2 = fc["hgS_070000"].children[0].children[1].properties.XData
y2 = fc["hgS_070000"].children[0].children[1].properties.YData

# for json dump...
data = {
    "no_intcal_x": list(x1),
    "no_intcal_y": list(y1),
    "with_intcal_x": list(x2),
    "with_intcal_y": list(y2),
}
with open("/home/thomas/devl/tc2013/Data/ppro/M160_SigmaVertProf.json", "w", encoding="utf-8") as fj:
    json.dump(data, fj)

# ellipses
e1 = matplotlib.patches.Ellipse((25.43, 15.6), 0.10, 16.0, fill=False, color="darkgray", linewidth=2)
e2 = matplotlib.patches.Ellipse((25.78, 28.0), 0.14, 08.0, fill=False, color="darkgray", linewidth=2)
e3 = matplotlib.patches.Ellipse((25.93, 32.5), 0.12, 08.0, fill=False, color="darkgray", linewidth=2)
e4 = matplotlib.patches.Ellipse((26.41, 60.1), 0.12, 18.0, fill=False, color="darkgray", linewidth=2)

# make the plot 
f, a = plt.subplots(1,1, figsize=(10,10), dpi=100)
a.scatter(x1, y1, c="navy", marker="s", label="without intercal.")
a.scatter(x2, y2, c="darkorange", label="with intercal.")
a.add_patch(e1)
a.add_patch(e2)
a.add_patch(e3)
a.add_patch(e4)
a.grid(which="major", axis="both", linestyle=":", c="gray")
a.set_ylabel("Pressure [dbar]")
a.set_xlabel(r"$\rho - 1000$ [kg m$^{-3}$]")
a.set_ylim([0, 120])
a.set_xlim([25,27])
a.xaxis.set_ticks([25, 25.5, 26, 26.5, 27])
a.invert_yaxis()
a.legend(loc="upper right")
plt.tight_layout()
plt.savefig("/home/thomas/devl/tc2013/Data/ppro/M160_SigmaVertProf.png")
