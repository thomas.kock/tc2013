""" 
Load TIA data sets (post processed) and make nice plots
"""

from copy import deepcopy
import glob
import os.path
import datetime
import netCDF4
import numpy
import matplotlib
import matplotlib.pyplot as plt
import scipy.signal as ss
import scipy.interpolate as si
import utm

# symbolic constants
P = 3
O2 = 4
CHL = 5

# switches
cruise = "Tara" # one of M160 Tara SubEx
x_data = "Time" # one of Time or Distance
filter_data = False
probe_depths = True
probe_labels = True
nlabels = 3
label_color = "k"
sm = 'o'
em = 'X'
msize = 100
mc = 'k'
ylim = [0,50]
plt.rcParams["font.size"] = 18
plt.rcParams["grid.linestyle"] = ':'
plt.rcParams["grid.color"] = 'lightgray'
plt.rcParams["grid.linewidth"] = 1

# order lists are ordered deep to shallow, but we need shallow to deep
m160_order = [985,1120,986,1112,1584,1115,1580,993,1117,1114,1581,989,1574,1573,1582,984][::-1]
tara_order = [987,993,988,1115,989,1116,1580,1119,1584][::-1]
subex_order = [993,1115,1120,990,1113,986,991][::-1]

# vars of interest
var0 = "OxygenSatuSS"
var1 = "FluoCycl"
var_units = {
    "SigmaPT100": "kg/m³",
    "TemperaturePT100": "°C",
    "SalinityPT100": "PSU",
    "OxygenSatuSS": "%",
    "FluoCycl": "??"
}

# colormap modifications
# if var == "SigmaPT100":
#     cmap = "viridis_r"
# else:
cmap = "viridis"
cut_off_cmap = matplotlib.cm.get_cmap(cmap).copy()
cut_off_cmap.set_over(color="lightgray")
cut_off_cmap.set_under(color="lightgray")

# time constraints and epoch
if cruise == "M160":
    t_epoch = datetime.datetime(1970,1,1)
    t0 = datetime.datetime(2019,12,13,14,30)
    t0_dn = (t0 - t_epoch).days + (t0 - t_epoch).seconds/86400
    t1 = datetime.datetime(2019,12,13,16)
    t1_dn = (t1 - t_epoch).days + (t1 - t_epoch).seconds/86400
elif cruise == "Tara":
    t_epoch = datetime.datetime(1970,1,1)
    t0 = datetime.datetime(2021,8,31,2,45)
    t0_dn = (t0 - t_epoch).days + (t0 - t_epoch).seconds/86400
    t1 = datetime.datetime(2021,8,31,6)
    t1_dn = (t1 - t_epoch).days + (t1 - t_epoch).seconds/86400

# load data
tia = None
if cruise == "M160":
    datapath = "/home/thomas/devl/tc2013/Data/m160"
    files = glob.glob(os.path.join(datapath, "*MT*.nc"))
    tia_files = []
    for sn in m160_order:
        for fn in files:
            if str(sn) in fn:
                tia_files.append(fn)
                break
    del(files)
elif cruise == "Tara":
    datapath = "/home/thomas/devl/tc2013/Data/tara"
    files = glob.glob(os.path.join(datapath, "*TA*.nc"))
    tia_files = []
    for sn in tara_order:
        for fn in files:
            if str(sn) in fn:
                tia_files.append(fn)
                break
    del(files)

n_probes = len(tia_files)
for i, f in enumerate(tia_files):
    print(f"Loading file: {f.split('/')[-1]}")
    with netCDF4.Dataset(f, "r") as ds:
        if not isinstance(tia, numpy.ndarray):
            tia = numpy.zeros((6, n_probes, ds.dimensions["time"].size))
        tia[0,i,:] = ds.variables["Time"][:].data
        tia[1,i,:] = ds.variables["Latitude"][:].data
        tia[2,i,:] = ds.variables["Longitude"][:].data
        tia[3,i,:] = ds.variables["Pressure"][:].data
        tia[4,i,:] = ds.variables[var0][:].data
        tia[5,i,:] = ds.variables[var1][:].data

# find time window indeces
i0 = 0
i1 = 0
while tia[0, 0, i0] < t0_dn:
    i0 += 1
while tia[0, 0, i1] < t1_dn:
    i1 += 1

print("Interpolating O2 and Chl Data")
max_idx = numpy.where(numpy.isnan(tia[P,-1]))[0].min()
if i1 > max_idx: i1 = max_idx - 1

for i in range(i0, i1, 1):
    print(f"\r{i-i0+1:6} of {i1 - i0}", end="")
    p = tia[P,:,i]
    o2 = numpy.ma.masked_array(tia[O2,:,i], mask=numpy.isnan(tia[O2,:,i]))
    chl = numpy.ma.masked_array(tia[CHL,:,i], mask=numpy.isnan(tia[CHL,:,i]))
    
    o2min = o2[o2.mask==False][0]
    o2max = o2[o2.mask==False][-1]

    chlmin = chl[chl.mask==False][0]
    chlmax = chl[chl.mask==False][-1]

    fo2 = si.interp1d(p[o2.mask==False], o2[o2.mask==False], "cubic", bounds_error=False, fill_value=(o2min, o2max))#"extrapolate")
    fchl = si.interp1d(p[chl.mask==False], chl[chl.mask==False], "cubic", bounds_error=False, fill_value=numpy.nan)#"extrapolate")
    o2full = fo2(p)
    chlfull = fchl(p)

    tia[O2,:,i] = o2full
    tia[CHL,:,i] = chlfull
print()

f, a = plt.subplots(3,1, figsize=(32,32), dpi=120, sharex=True)
vmin = 0
vmax = 1
nticks = 4
ticks = [i * (vmax-vmin)/4 for i in range(nticks +1)]
qcs = a[0].contourf(
    tia[0,:,i0:i1],
    tia[O2,:,i0:i1],
    tia[CHL,:,i0:i1],
    levels = numpy.linspace(vmin, vmax, 51),
    extend="max"
    )
a[0].set_ylabel(f"{var0} [{var_units[var0]}]")
f.colorbar(qcs, ax=a[0], label=f"{var1} [{var_units[var1]}]", ticks=ticks)
# chl vs press
qcs = a[2].contourf(
    tia[0,:,i0:i1],
    tia[P,:,i0:i1],
    tia[CHL,:,i0:i1],
    levels = numpy.linspace(vmin, vmax, 51),
    extend="max"
    )
a[2].set_ylabel("Pressure [dbar]")
f.colorbar(qcs, ax=a[2], label=f"{var1} [{var_units[var1]}]", ticks=ticks)
# oxy vs press
vmin = numpy.nanmin(tia[O2,:,i0:i1])
vmax = numpy.nanmax(tia[O2,:,i0:i1])
qcs = a[1].contourf(
    tia[0,:,i0:i1],
    tia[P,:,i0:i1],
    tia[O2,:,i0:i1],
    levels = numpy.linspace(vmin, vmax, 51),
    extend="max"
    )
a[1].set_ylabel("Pressure [dbar]")
f.colorbar(qcs, ax=a[1], label=f"{var0} [{var_units[var0]}]")
a[2].set_xlabel("Time UTC [HH:MM]")
a[2].xaxis.set_major_formatter(matplotlib.dates.DateFormatter("%H:%M"))
a[1].invert_yaxis()
a[2].invert_yaxis()
plt.tight_layout()
plt.savefig(os.path.join(datapath, "Experiment_TimeO2_cmapChl.png"))
plt.show()
# if filter_data:
#     for i in range(n_probes):
#         tia[4,i] = ss.savgol_filter(tia[4,i], 101, 3)

# e, n = utm.from_latlon(tia[1,0], tia[2,0])[:2]
# dd = numpy.zeros_like(tia[1,0])
# dd[1:] = numpy.hypot(e[1:] - e[:-1], n[1:] - n[:-1])
# dd = numpy.cumsum(dd)
# distance = numpy.tile(dd, n_probes).reshape(n_probes, -1)/1000
# del(dd, e, n)

# # find time window indeces
# i0 = 0
# i1 = 0
# while tia[0, 0, i0] < t0_dn:
#     i0 += 1
# while tia[0, 0, i1] < t1_dn:
#     i1 += 1

# vmin = numpy.nanmin(tia[4,:,i0:i1])
# vmax = numpy.nanmax(tia[4,:,i0:i1])

# # plot using gridspec for subplots spacing
# f = plt.figure(constrained_layout=True, figsize=(21,7), dpi=100)
# gs = matplotlib.gridspec.GridSpec(1, 100, figure=f)
# # set pressure nans to 0 for plotting
# tia[3][numpy.isnan(tia[3])] = 0
# # grey out values outside the interesting range
# surf_data = deepcopy(tia[4,0])
# surf_data[:i0] = -1
# surf_data[i1:] = -1
# # axes instance for ship track (lat lon -> ll)
# ax_ll = f.add_subplot(gs[:,:27])
# ax_ll.scatter(tia[2,0], tia[1,0], c=surf_data, vmin=vmin, vmax=vmax, cmap=cut_off_cmap)
# ax_ll.scatter(tia[2,0,i0], tia[1,0,i0], c=mc, marker=sm, s=msize)
# ax_ll.scatter(tia[2,0,i1], tia[1,0,i1], c=mc, marker=em, s=msize)
# ax_ll.axis("equal")
# ax_ll.xaxis.set_major_formatter(matplotlib.ticker.FormatStrFormatter("%4.2f"))
# ax_ll.yaxis.set_major_formatter(matplotlib.ticker.FormatStrFormatter("%5.2f"))
# ax_ll.set_ylabel("Latitude")
# ax_ll.set_xlabel("Longitude")
# ax_ll.grid(which="both")
# ax_ll.text(
#     0.05 * (96-28)/100, 0.03, "a", 
#     color="k", verticalalignment="bottom", horizontalalignment="left", fontsize=32,
#     transform=ax_ll.transAxes, 
#     fontweight="bold", bbox=dict(facecolor='w')
#     )
# # axes instance for contourf plot
# ax_cf = f.add_subplot(gs[:,28:96])
# if x_data == "Distance":
#     offs = 500
#     cfi0 = i0-offs
#     cfi1 = i1+offs
#     nd = distance[:,cfi0:cfi1] - distance[0,cfi0+offs]
#     qcs = ax_cf.contourf(
#         nd,
#         tia[3,:,cfi0:cfi1],
#         tia[4,:,cfi0:cfi1],
#         levels=numpy.linspace(round(vmin, 1)-0.1, round(vmax, 1) + 0.1, 41),
#         cmap=cmap,
#         # extend="both" # this is the desired setting, it is however buggy in matplotlib 3.5.1 and should be fixed in 3.5.2, but is not released yet
#     )
#     if probe_depths:
#         for i in range(n_probes):
#             ax_cf.plot(nd[i], tia[3,i,cfi0:cfi1], c="w", lw=0.3)
#     if probe_labels:
#         span = int((cfi1 - cfi0)/(nlabels +1))
#         for i in range(n_probes):
#             for j in range(1, nlabels+1):
#                 ax_cf.text(nd[i, j * span], tia[3,i,cfi0 + j * span], str(m160_order[i]), 
#                 va="center", ha="center", color=label_color, fontweight="bold", fontsize=8)
#     ax_cf.scatter(nd[0,  offs], tia[3,0,i0], marker=sm, c=mc, s=msize, zorder=3)
#     ax_cf.scatter(nd[0, -offs], tia[3,0,i1], marker=em, c=mc, s=msize, zorder=3)
#     ax_cf.set_xlabel("Along track distance [km]")
# elif x_data == "Time":
#     qcs = ax_cf.contourf(
#         tia[0,:,i0:i1],
#         tia[3,:,i0:i1],
#         tia[4,:,i0:i1],
#         levels=numpy.linspace(round(vmin, 1)-0.1, round(vmax, 1) + 0.1, 41),
#         cmap=cmap,
#         # extend="both" # this is the desired setting, it is however buggy in matplotlib 3.5.1 and should be fixed in 3.5.2, but is not released yet
#     )
#     if probe_depths:
#         for i in range(n_probes):
#             ax_cf.plot(tia[0,i,i0:i1], tia[3,i,i0:i1], c="w", lw=0.3)
#     if probe_labels:
#         span = int((i1 - i0)/(nlabels +1))
#         for i in range(n_probes):
#             for j in range(1, nlabels+1):
#                 ax_cf.text(tia[0, i, i0 + j * span], tia[3,i,i0 + j * span], str(m160_order[i]), 
#                 va="center", ha="center", color=label_color, fontweight="bold", fontsize=8)
#     ax_cf.scatter(tia[0, 0, i0], tia[3,0,i0], marker=sm, c=mc, s=msize, zorder=3)
#     ax_cf.scatter(tia[0, 0, i1], tia[3,0,i1], marker=em, c=mc, s=msize, zorder=3)
#     ax_cf.set_xlabel("UTC time [HH:MM]")
#     ax_cf.xaxis.set_major_formatter(matplotlib.dates.DateFormatter("%H:%M"))
# ax_cf.grid(which="major", axis="x")
# ax_cf.set_ylim(ylim)
# ax_cf.invert_yaxis()
# ax_cf.set_ylabel("Pressure [dbar]")
# ax_cf.text(
#     0.05 * 27/100, 0.03, 
#     "b", color="k", verticalalignment="bottom", horizontalalignment="left", fontsize=32, 
#     transform=ax_cf.transAxes, 
#     fontweight="bold", bbox=dict(facecolor='w')
#     )

# ax_cbar = f.add_subplot(gs[:,98:])
# cbar = f.colorbar(qcs, cax=ax_cbar, format="%3.1f", label=f"{var} [{var_units[var]}]")
# if var == "SigmaPT100": cbar.ax.invert_yaxis()
# plt.savefig(os.path.join(datapath, f"{cruise}_contourf_{var}.png"))
# plt.show()
