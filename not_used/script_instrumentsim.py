# %% [markdown]
# # About
# A jupyter notebook used to model and evaluate different towed instruments used for sampling the 
# submesoscale.

# %%
"""Import and setup cell, does not do any logic work. Run first"""
import libmodeldata
import libinstruments

import cmocean
import numpy
import matplotlib.pyplot as plt

CMAP = cmocean.cm.thermal_r
CMAP_DIFF = cmocean.cm.balance

DEBUG = False
SAVEFIGS = False
XLIM = (1000, 5000)
FRONT = True

plt.close("all")
plt.rcParams["font.size"] = 22

print("Modules and constant loaded successfully")

# %% [markdown]
# # Modelling the transect data
# A data model is computed based on a measured temperature profile. Simplifying assumptions are 
# that the vertical tempretature profile is constant of time/distance and perturbations in the 
# vertical do not change the profile itself, but move it up or down. The temperature profile data
# file is provided in the Data directory of this repo.

# %%
"""Model setup cell. After running this cell the model data is in memory"""
figures = {}
data_filepath = "/home/thomas/0_working_local/tc2013/20110414/Vessel/TIA_20110414_part1.mat"
if FRONT:
    model = libmodeldata.Model_SingleFront(domain_length=6000, max_depth=100, x_step=5,
    base_temp=25, tc_drop=-0.8, tc_start=50, tc_size=15)
    # model.create_var_array_from_profile_file(data_filepath)
    model.create_var_array_from_function()
    model.add_frontal_feature(3500, 100, 1000)
else:
    model = libmodeldata.Model_InternalWave(domain_length=12000, max_depth=100, x_step=10)
    model.create_var_array_from_profile_file(data_filepath)
    # model.create_var_array_from_linear()
    # model.add_noise_to_var_array()
    model.compute_internal_wave(wave_center = model.DOMAIN_LENGTH/2, wave_height=12)
    # model.compute_internal_wave(wave_center = 400, wave_height=-3, wave_length=200)
    # model.compute_internal_wave(wave_center = 1700, wave_height=4, wave_length=290)

if DEBUG:
    figures["debug_model_data"] = model.plot_model_data()
    figures["debug_model_profile"] = model.plot_model_profile()

# %% [markdown]
# # Modelling the instruments
# Three types of instruments are set up for the intercomparison of the different sampling
# approchaes used in the submesoscale (Levy 2012, own experiences):
# 
# 1: Horizontally towed CTDs attached to a string (towed CTD chains, for instance Sellschopp 1997)
# 2: Undulating devices following sawtooth pattern (for instance SeaSoar, Pollard 1986)
# 3: Repeatedly vertical profiling instruments used underway (underway CTD, Rudnick 1997)
# 
# # The towed CTD chain
# A towed CTD chain is simulated using a set of parameters used by our own working group during
# the M160 cruise on German research vessel Meteor in late 2019. About 15 CTD (+ 1 additional sensor) 
# were used sampling at a rate of about 6 Hz. The targeted depth was about 100 m, actual depths was 
# between 120 and 90 m. The Meteor steamed at about 6 kn, which is approx. 3 m/s.

# %%
"""Setup Towed Chain instrument and simulate data collection"""
tia = libinstruments.TIA(model, sensor_depths=(2,100,15),sample_rate=6, boat_speed=3, cmap=CMAP)
tia.simulate_data_collection()
tia.interpolate_to_model_grid()
figures["tia"] = tia.plot_data(xlim=XLIM)

# %% [markdown]
# # The undulating instrument
# As pointed out before the SeaSoar instrument (Pollard 1986, Pidcock 2010) serves as a well
# established instrument for the group of undulating instruments. The operating parameters are
# taken from Pidcock et al. 2010, section 2b. They used the SeaSoar at 8.5 kn (about 4.25 m/s)
# vessel speed, a diving rate of 1.3 m/s and a sampling rate of 1 Hz.

# %%
"""Setup Undulatin instrument and simulate data collection"""
undu = libinstruments.Undulating(model, sample_rate=1, offset=0, 
    min_depth=2, boat_speed=4.25, dive_rate=1.3, cmap=CMAP)
undu.simulate_data_collection()
undu.interpolate_to_model_grid()
figures["undu"] = undu.plot_data(xlim=XLIM)

# %% [markdown]
# # The vertical profiling instrument
# The underway CTD described in Rudnick & Klinke 2007 serves a represetative for the group of 
# vertically profiling instruments. Operating parameters are taken from this paper, which is also
# the default for the modelled instrument. The drop rate is about 4 m/s, the recover rate about 
# 0.89 m/s. The instruments samples at 10 Hz. No time for tail spooling or instrument handling
# is taken into account, meaning the vertical profiles are as close together as possible.
# 
# Note: We do not consider the use of XBTs as a contender for this group of instruments because of
# their single use nature.

# %%
"""Setup and simulate vertical profiling instrument"""
uctd = libinstruments.UCTD(model, cmap=CMAP)
uctd.simulate_data_collection()
uctd.interpolate_to_model_grid()
figures["uctd"] = uctd.plot_data(xlim=XLIM)

# %% [markdown]
# # Comparison
# The different sampling approaches seem to make different interpolation strategies necessary. 
# But all sampled instrument data is simply linearly interpolated back to the original grid and
# plotted as such

# %%
fc, ac = plt.subplots(
    4, 3, sharex=True, sharey=True, figsize=(20,16), dpi=128, num="Comparison",
    facecolor='w'
    )
tmin = model.min_temp
tmax = model.max_temp
err_min = -1
err_max = 1
ylim = (0, model.MAX_DEPTH)
if not XLIM:
    xlim = (0, model.DOMAIN_LENGTH)
else:
    xlim = XLIM
scatsize=3
source = ["Model", "Tow Chain", "SeaSoar", "uCTD"]

# model data
xx, yy = numpy.meshgrid(model.distances, model.z)
mod_scat = ac[0,0].scatter(xx.flatten(), yy.flatten(), c=model.temp_array.T.flatten(), cmap=CMAP, vmin=tmin, vmax=tmax, s=scatsize)
ac[0,1].pcolormesh(model.distances, model.z, model.temp_array.T, cmap=CMAP, vmin=tmin, vmax=tmax, shading="auto")
ac[0,2].pcolormesh(model.distances, model.z, model.temp_array.T - model.temp_array.T, cmap=CMAP_DIFF, vmin=err_min, vmax=err_max, shading="auto")
# tia data
for i in range(tia.nz):
    ac[1,0].scatter(tia.x_samples, numpy.ones_like(tia.x_samples) * tia.SENSOR_DEPTHS[i], c=tia.t_samples[i], cmap=CMAP, vmin=tmin, vmax=tmax, s=scatsize)
ac[1,1].pcolormesh(tia.model.distances, tia.model.z, tia.temp_array, cmap=CMAP, vmin=tmin, vmax=tmax, shading="auto")
ac[1,2].pcolormesh(tia.model.distances, tia.model.z, model.temp_array.T - tia.temp_array, cmap=CMAP_DIFF, vmin=err_min, vmax=err_max, shading="auto")
# scanfish data
ac[2,0].scatter(undu.x_samples, undu.z_samples, c=undu.t_samples, cmap=CMAP, vmin=tmin, vmax=tmax, s=scatsize)
ac[2,1].pcolormesh(undu.model.distances, undu.model.z, undu.temp_array, cmap=CMAP, vmin=tmin, vmax=tmax, shading="auto")
ac[2,2].pcolormesh(undu.model.distances, undu.model.z, model.temp_array.T - undu.temp_array, cmap=CMAP_DIFF, vmin=err_min, vmax=err_max, shading="auto")
# uctd data
for i in range(uctd.nx):
    ac[3,0].scatter(numpy.ones_like(uctd.z_samples) * uctd.x_samples[i], uctd.z_samples, c=uctd.t_samples[i], cmap=CMAP, vmin=tmin, vmax=tmax, s=scatsize)
ac[3,1].pcolormesh(uctd.model.distances, uctd.model.z, uctd.temp_array, cmap=CMAP, vmin=tmin, vmax=tmax, shading="auto")
ac[3,2].pcolormesh(uctd.model.distances, uctd.model.z, model.temp_array.T - uctd.temp_array, cmap=CMAP_DIFF, vmin=err_min, vmax=err_max, shading="auto")
c = ord('a')
    
for i in range(4):
    ac[i,0].plot([3000, 3000], [0, 100], lw=2, c='gray')
    ac[i,1].plot([3000, 3000], [0, 100], lw=2, c='gray')
    ac[i,2].plot([3000, 3000], [0, 100], lw=2, c='gray')
    ac[i, 0].set_ylim(ylim)
    ac[i, 0].set_xlim(xlim)
    ac[i, 0].set_ylabel("Depth [m]")
    ac[i, 0].set_title(chr(c) + ") " + source[i] + " - Sampled data", loc="left")
    c += 1
    ac[i, 1].set_title(chr(c) + ") " + source[i] + " - Interpolated data", loc="left")
    c += 1
    ac[i, 2].set_title(chr(c) + ") " + source[0] + " - " + source[i], loc="left")
    c += 1
ac[0,0].invert_yaxis()
for i in range(3):
    ac[3,i].set_xlabel("Distance [m]")
    if xlim:
        current_ticks = ac[3,i].get_xticks()
        print(current_ticks)
        ac[3,i].set_xticks(current_ticks, labels=(current_ticks - model.FEATURE_CENTER).astype('i4'))


fc.subplots_adjust(left=0.05, right=0.97, top=0.95, bottom=0.15)
cbar_ax = fc.add_axes([0.3, 0.05, 0.4, 0.02])
cbar = fc.colorbar(mod_scat, cax=cbar_ax, orientation="horizontal")
cbar.set_label("Density [kg/m³]", loc="center")
# fc.tight_layout()

# plt.show()

# fc.savefig("/home/thomas/0_working_local/tc2013/comparison.png")

print(f"TIA stddev overall: {numpy.nanstd(tia.temp_array):4f}")
print(f"SF stddev overall: {numpy.nanstd(undu.temp_array):4f}")
print(f"UCTD stddev overall: {numpy.nanstd(uctd.temp_array):4f}")

i0 = int((model.FEATURE_CENTER - model.FEATURE_SIZE/2)/model.X_STEP)
i1 = int((model.FEATURE_CENTER + model.FEATURE_SIZE/2)/model.X_STEP)
print(f"TIA stddev feature: {numpy.nanstd(tia.temp_array[:, i0:i1]):4f}")
print(f"SF stddev feature: {numpy.nanstd(undu.temp_array[:, i0:i1]):4f}")
print(f"UCTD stddev feature: {numpy.nanstd(uctd.temp_array[:, i0:i1]):4f}")

# %%
# compute delta rho over delta x
vmin = -0.02
vmax = 0.02
tia_drho_dx = libinstruments.compute_dval_dx(tia.temp_array, dx=tia.model.X_STEP)
undu_drho_dx = libinstruments.compute_dval_dx(undu.temp_array, dx=undu.model.X_STEP)
uctd_drho_dx = libinstruments.compute_dval_dx(uctd.temp_array, dx=uctd.model.X_STEP)
f, a = plt.subplots(1,3, figsize=(30,10), dpi=100)
m = a[0].pcolormesh(tia.model.distances, tia.model.z, tia_drho_dx, vmin=vmin, vmax=vmax, cmap=CMAP_DIFF)
a[1].pcolormesh(undu.model.distances, undu.model.z, undu_drho_dx, vmin=vmin, vmax=vmax, cmap=CMAP_DIFF)
a[2].pcolormesh(uctd.model.distances, uctd.model.z, uctd_drho_dx, vmin=vmin, vmax=vmax, cmap=CMAP_DIFF)
for i in range(3):
    a[i].invert_yaxis()
    a[i].set_title(f"drho/dx: {source[i+1]}")
f.colorbar(m, ax=a[2])
f.show()

# %% check that all models are the same
print((undu.model.temp_array == tia.model.temp_array).all())
print((undu.model.temp_array == uctd.model.temp_array).all())

# %% compute drho/dx from sampled data
