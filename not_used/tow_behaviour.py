# product should be plots:
#   1. Shape of 50 m line
#   2. Shape of 100 m line
#   3. Constants of pressure reading. Maybe variance?

import json
import glob, os
import math
import scipy.io as sio
import matplotlib.pyplot as plt
import numpy

FIT = False
SPAN = True
size=50

# swith on interactive matplotlib plots
plt.ion()
plt.rcParams["font.size"] = 22


# load hooks file for sensor positions along the rope
f = open("hook.json", "r")
chain_config = json.load(f)
f.close()
hooks = chain_config["2011-04-14a"]
# get the paths of data files
basepath = "/home/thomas/0_working_local/tc2013/20110414/Vessel"
paths = glob.glob(os.path.join(basepath, "*part1_transect*.mat"))
f, a = plt.subplots(1,1,num="pressure", figsize=(30,16), dpi=100)
fsh, ash = plt.subplots(1,1,num="shape")

x_all = []
y_all = []

pf_x = numpy.zeros((len(paths), len(hooks)), dtype="f4")
pf_y = numpy.zeros((len(paths), len(hooks)), dtype="f4")

label = True

for ip, path in enumerate(paths):
    # get the number of the transect as it is used by the data dict keys...
    i_substr = path.find("transect")
    i_transe = int(path[i_substr+8:i_substr+10])
    data = sio.loadmat(path)
    t = data["t" + str(i_transe)][0] * 3600 * 24
    for p in data["P" + str(i_transe)][1:]:
        a.plot(t, p, c="k")
    average_depths = numpy.nanmean(data["P" + str(i_transe)], axis=1)
    average_depths[0] = 0.4
    dx = [0]
    for i in range(1, len(hooks)):
        dx.append(
            math.sqrt((hooks[i] - hooks[i-1])**2 - (average_depths[i] - average_depths[i-1])**2)
        )
    x = numpy.cumsum(dx)
    
    if label:
        ash.scatter(x, average_depths, c="k", s=size, label="100 m TIA")
        label = False
    else:
        ash.scatter(x, average_depths, s=size, c="k")
    
    pf_x[ip] = x
    pf_y[ip] = average_depths

    x_all.extend(x)
    y_all.extend(average_depths)

if FIT:
    p = numpy.polynomial.polynomial.Polynomial.fit(x_all, y_all, 2, domain=[0, 100])
    ash.plot(*p.linspace(), ls="--", c="k")

if SPAN:
    x_min = numpy.zeros((len(hooks),), dtype="f4")
    x_max = numpy.zeros((len(hooks),), dtype="f4")
    p_min = numpy.zeros((len(hooks),), dtype="f4")
    p_max = numpy.zeros((len(hooks),), dtype="f4")
    for i in range(len(hooks)):
        x_min[i] = numpy.min(pf_x[:,i])
        x_max[i] = numpy.max(pf_x[:,i])
        p_min[i] = numpy.min(pf_y[:,i])
        p_max[i] = numpy.max(pf_y[:,i])
    xs = numpy.concatenate((x_min, x_max))
    ps = numpy.concatenate((p_min, p_max))
    ash.plot(x_min, p_min)
    ash.plot(x_max, p_max)


#### part2 ####

hooks = chain_config["2011-04-14b"]
paths = glob.glob(os.path.join(basepath, "*part2_transect*.mat"))

x_all = []
y_all = []

label=True

for path in paths:
    # get the number of the transect as it is used by the data dict keys...
    i_substr = path.find("transect")
    i_transe = int(path[i_substr+8:i_substr+10])
    data = sio.loadmat(path)
    t = data["t" + str(i_transe)][0] * 3600 * 24
    for p in data["P" + str(i_transe)][1:]:
        a.plot(t, p, c="gray")
    average_depths = numpy.nanmean(data["P" + str(i_transe)], axis=1)
    average_depths[0] = 0.4
    dx = [0]
    for i in range(1, len(hooks)):
        dx.append(
            math.sqrt((hooks[i] - hooks[i-1])**2 - (average_depths[i] - average_depths[i-1])**2)
        )
    x = numpy.cumsum(dx)
    
    if label:
        ash.scatter(x, average_depths, c="gray", s=size, label="50 m TIA")
        label = False
    else:
        ash.scatter(x, average_depths, c="gray", s=size)
    x_all.extend(x)
    y_all.extend(average_depths)

if FIT:
    p = numpy.polynomial.polynomial.Polynomial.fit(x_all, y_all, 2, domain=[0, 60])
    ash.plot(*p.linspace(), ls="--", c="gray")

ash.set_ylim([0, 40])
ash.set_xlim([0,100])
ash.set_ylabel("Depth [dbar]")
ash.set_xlabel("Distance [m]")
ash.invert_yaxis()
ash.grid(True)
fsh.legend()


# e, n = utm.from_latlon(data["lat7"][0], data["lon7"][0])[:2]
# speed = ssi.savgol_filter(numpy.hypot((e[1:] - e[:-1]), (n[1:] - n[:-1])) / (t[1:] - t[:-1]), 51, 1)

# 
    
# medians = []
    

# fsp, asp = plt.subplots(1,1, num="Speed")
# asp.plot(t[1:], speed)

# # average depths of probes during transect
# 


# x = numpy.cumsum(dx)
# 
# xp, yp = p.linspace(

# )
# 
# plt.plot(xp, yp)


