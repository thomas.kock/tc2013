import glob
import os.path
import netCDF4
import numpy
import matplotlib.pyplot as plt

bad_ctms = ["CTM1581", "CTM1573", "CTM984"]

files = glob.glob("/home/thomas/devl/tc2013/Data/dori/*.nc")

fig, a = plt.subplots(1,1)
for f in files:
    with netCDF4.Dataset(f, "r") as x:
        ctm = f.split(os.path.sep)[-1].split('_')[5]
        if ctm not in bad_ctms:
            continue
        i = list(range(x.dimensions["time"].size))
        t = x.variables["TemperaturePT100"][:]
        qf = x.variables["TemperaturePT100 Quality Flag"][:]
        tm = numpy.ma.masked_where(qf > 2, t)
        a.plot(tm, label=ctm)
fig.legend()
plt.show()