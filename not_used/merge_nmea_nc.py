""" 
Takes NMEA logs (raw telegrams) and merges them with tia netcdf files.

This script does not check for time accuracy.

KT 2022-03-17
"""

import datetime
import glob
import os.path
import numpy
import matplotlib.pyplot as plt
import netCDF4
import scipy.interpolate as si


basepath = "/home/thomas/devl/tc2013/Data/subex16"
date2process = "20160627"
t_epoch = datetime.datetime(1970,1,1)
lat_mod = {'N': 1, 'S': -1}
lon_mod = {'E': 1, 'W': -1}

t = []
lat = []
lon = []

with open(os.path.join(basepath, "2706_NMEA.txt"), "r", encoding="cp1252") as fid:
    fc = fid.readlines()

for line in fc:
    s = line.split(',')
    if s[0] == "$GPGGA":
        td = datetime.datetime.strptime(f"{date2process}T{s[1]}", "%Y%m%dT%H%M%S.%f") - t_epoch
        t.append(td.days + td.seconds/86400 + td.microseconds/(86400*1E6))
        lat.append(lat_mod[s[3]] * (float(s[2][:2]) + float(s[2][2:])/60))
        lon.append(lon_mod[s[5]] * (float(s[4][:3]) + float(s[4][3:])/60))

ndata = len(t)
nmea = numpy.zeros((3, ndata))
nmea[0] = t
nmea[1] = lat
nmea[2] = lon

all_tia_files = glob.glob(os.path.join(basepath, date2process, "*.nc"))
for file in all_tia_files:
    with netCDF4.Dataset(file, "r+") as ds:
        print(ds.dimensions["time"].size)
        tia_time = ds.variables["Time"][:].data

        f_lat = si.interp1d(nmea[0], nmea[1])
        f_lon = si.interp1d(nmea[0], nmea[2])

        tia_lat = f_lat(tia_time)
        tia_lon = f_lon(tia_time)

        ds.variables["Latitude"][:] = tia_lat
        ds.variables["Longitude"][:] = tia_lon


with netCDF4.Dataset(all_tia_files[3]) as ds:
    ntime = ds.variables["Time"][:].data
    nlat = ds.variables["Latitude"][:].data
    nlon = ds.variables["Longitude"][:].data

plt.scatter(nlon, nlat, c=ntime)
plt.show()