import matplotlib.pyplot as plt
import glob
import os.path
from scipy.io import loadmat
import cmocean
from datetime import datetime as dt
from matplotlib.dates import date2num
import numpy

plt.ion()
plt.rcParams["font.size"] = 22
cmap = cmocean.cm.thermal

path = "/home/thomas/0_working_local/tc2013/RBR_20130131"
pattern = "*.mat"
time_fmt = "%d/%m/%Y %H:%M:%S.%f %p"
TEMP = 1
TIME = 0
PRESS = 2

files = glob.glob(os.path.join(path, pattern))

pdata = {}
ptime = {}

for file in files:
    if "18628" in file:
        continue
    print(f"Loading data of file {file.split(os.path.sep)[-1]}")
    data = loadmat(file)
    quantities = data["RBR"]["data"][0,0]
    times = date2num([dt.strptime(_[0][0], time_fmt) for _ in data["RBR"]["sampletimes"][0,0]])
    times = times.reshape((times.shape[0], 1))
    pdata[data["RBR"]["name"][0,0][0].split()[-1]] = numpy.hstack((times, quantities))

tmax = 0
tmin = 100

print("Looking for Temperature min and max")
for k, v in pdata.items():
    print(f"Shape {k}: {v.shape}")
    _ = v[:,1].min()
    if _ < tmin:
        tmin = _
    _ = v[:,1].max()
    if _ > tmax:
        tmax = _

print("Found:")
print(f"Tmax: {tmax:.3f}")
print(f"Tmin: {tmin:.3f}")


f, a = plt.subplots(1,1, num="temp", figsize=(16,10), dpi=180)
for k, v in pdata.items():
    if v.shape[1] <= 2:
        continue
    a.scatter(v[:,TIME], v[:,PRESS], c=v[:,TEMP], vmin=tmin, vmax=tmax, cmap=cmap)

