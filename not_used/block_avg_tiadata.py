import glob, os.path
import netCDF4
import numpy
from typing import List

def compute_block(ntime: numpy.ndarray, orig_time: numpy.ndarray, values: numpy.ndarray) -> numpy.ndarray:
    new_vals = []
    j = 0
    for nt in ntime:
        block = []
        while orig_time[j] < nt:
            block.append(values[j])
            j += 1
        new_vals.append(numpy.nanmean(block))
        block = []
    return numpy.array(new_vals)

def find_slices(ntime: numpy.ndarray, orig_time: numpy.ndarray) -> List[slice]:
    slices = []
    i = 0
    j = 0
    for nt in ntime:
        while orig_time[j] < nt:
            j += 1
        slices.append(slice(i, j, 1))
        i = j
    return slices


interval = 5
basepath = "/home/thomas/devl/tc2013/Data/dori"
files = glob.glob(os.path.join(basepath, "*.nc"))
ctm_serials = [_.split(os.path.sep)[-1].split('_')[5] for _ in files]
keys_to_process = [
    "Latitude",
    "Longitude",
    "Pressure",
    "TemperaturePT100",
    "SalinityPT100",
    "SigmaPT100",
    "OxygenSatuSS",
    "FluoCycl",
    "Conductivity",
]
nvars = len(keys_to_process) + 2 # +time and nsamples

with netCDF4.Dataset(files[0], "r") as dataset:
    t0 = dataset.variables["Time"][0] * 86400
    t1 = dataset.variables["Time"][-1] * 86400
    nsamples = dataset.dimensions["time"].size

seconds = t1 - t0
print(f"Datasets contain {seconds} s and {nsamples} samples")
print(f"This translates to {int(seconds/interval)} intervals of {interval} s")
nnew = int(nsamples / seconds * interval)
print(f"Samples per interval: {nnew}")
ntime = numpy.arange(int(t0 + interval/2), int(t1 - interval/2), interval) / 86400

for sn, file in zip(ctm_serials, files):
    # grab data from fulls set and reduce
    print(f"{sn}, computing blocks ...")
    blocked = numpy.zeros((nvars, len(ntime)), dtype=numpy.float64)
    units = {}
    with netCDF4.Dataset(file, "r") as dataset:
        slices = find_slices(ntime, dataset.variables["Time"][:])
        blocked[0] = ntime
        for i, key in enumerate(keys_to_process):
            mem = []
            units[key] = dataset[key].units
            for s in slices:
                mem.append(numpy.nanmean(dataset.variables[key][s]))
            blocked[i+1] = mem
        blocked[i+2] = [s.stop - s.start for s in slices]
    print("Finished")
    # write the blocks to new nc file

    fp = os.path.join(basepath, "block", f"{sn}_blocked_{interval}s.nc")
    print(f"Writing {fp}")
    with netCDF4.Dataset(fp, "w") as nds:
        nds.createDimension("time", len(ntime))
        nds.createVariable("Time", "f8", ("time",))
        nds.createVariable("Samples per Block", "f8", ("time",))
        nds.variables["Time"][:] = blocked[0]
        nds.variables["Time"].units = "days since 1970-01-01"
        nds.variables["Samples per Block"][:] = blocked[-1][:]
        nds.variables["Samples per Block"].units = "none"
        for i, k in enumerate(keys_to_process):
            nds.createVariable(k, "f8", ("time",))
            nds.variables[k][:] = blocked[i+1][:]
            nds.variables[k].units = units[k]