"""
Quick and dirty script to generate a comparison plot between individual ctm timeseries without
time corrections and the corrected netcdf timeseires of the accepted probes.

Author: thomas.kock@hereon.de
Date: 2022-05-02
"""

import datetime
import netCDF4
import os.path
import glob 
import numpy
import cmocean
import matplotlib.gridspec as gridsp
import matplotlib.pyplot as plt
import matplotlib.dates as md

# global parameters for later plots
plt.rcParams["image.cmap"] = cmocean.cm.thermal
plt.rcParams["scatter.marker"] = '|'
plt.rcParams["lines.markersize"] = 8
plt.ion()

# you may change the paths to reflect local set up
# basepath of data dir
basepath ="/home/thomas/Documents/20191213_1_tia6"
# were to find the full chain netcdf data
fullchainpath = "FULL_CHAIN_NC/Part1"
# where to find the individual timeseries netcdfs with TimeOrig
ctmpath = "NC"

# set up of time constraints
dt_epoch = datetime.datetime(1970,1,1, tzinfo=datetime.timezone.utc)
dt0 = datetime.datetime(2019,12,13,14,38,tzinfo=datetime.timezone.utc)
dt1 = datetime.datetime(2019,12,13,14,48,tzinfo=datetime.timezone.utc)
time_delta = dt0 - dt_epoch
t0 = time_delta.days + time_delta.seconds/86400 + time_delta.microseconds / (86400 * 1E6)
time_delta = dt1 - dt_epoch
t1 = time_delta.days + time_delta.seconds/86400 + time_delta.microseconds / (86400 * 1E6)

# glob files of individual ctms timeseries
print("Working on individual CTM ncs ...")
files = glob.glob(os.path.join(basepath, ctmpath, "*.nc"))
# load timeseries data
times = []
press = []
values = []
vmin = 999
vmax = -999
i0s = []
i1s = []
sns = []
for f in files:
    snidx = f.find("CTM")
    sn = f[snidx:snidx+7].strip('_')
    print(f"{sn}")
    sns.append(sn)
    with netCDF4.Dataset(f, "r") as ds:
        # unfortunately Timeorig is in Matlab datenum format, so we need to convert it to 
        # somethingmore useful
        t = ds.variables["TimeOrig"][:].data
        conv = []
        for _ in t:
            ctd = (datetime.datetime.fromordinal(int(_) - 366) + datetime.timedelta(days=_%1)).replace(tzinfo=datetime.timezone.utc) - dt_epoch
            conv.append(ctd.days + ctd.seconds/86400 + ctd.microseconds / (86400 * 1E6))
        # the pressure and temperature
        times.append(conv)
        press.append(ds.variables["Pressure"][:].data)
        try:
            vals = ds.variables["TemperaturePT100"][:].data
        except KeyError:
            vals = ds.variables["TemperatureNTC"][:].data
            print(f"Note: Probe {sn} has not PT100 data, using NTC instead")
        # look for correct time indeces
        i = 0
        while conv[i] < t0 and i < len(conv) - 1:
            i += 1
        i0 = i
        i0s.append(i0)
        while conv[i] < t1 and i < len(conv) - 1:
            i += 1
        i1 = i
        i1s.append(i1)
        # check if there are new min max values
        try:
            lmin = numpy.nanmin(vals[i0:i1])
            lmax = numpy.nanmax(vals[i0:i1])
        except ValueError:
            pass
        if lmin < vmin: vmin = lmin
        if lmax > vmax: vmax = lmax
        values.append(vals)

# glob and load all tia netcdfs
print("Working on TIA ncs ...")
tia_files = glob.glob(os.path.join(basepath, fullchainpath, "*.nc"))
tia_sns = []
tia_i0s = []
tia_i1s = []
tia_data = {}
for f in tia_files:
    snidx = f.find("CTM")
    sn = f[snidx:snidx+7].strip('_')
    print(f"{sn}")
    tia_sns.append(sn)
    with netCDF4.Dataset(f, "r") as ds:
        if not sn in tia_data.keys():
            tia_data[sn] = numpy.zeros((3, ds.dimensions["time"].size))
        tia_data[sn][0] = ds.variables["Time"][:].data
        tia_data[sn][1] = ds.variables["Pressure"][:].data
        tia_data[sn][2] = ds.variables["TemperaturePT100"][:].data
    tia_data[sn][1][numpy.isnan(tia_data[sn][1])] = 0
    i = 0
    while tia_data[sn][0][i] < t0 and i < tia_data[sn].shape[1] - 1:
        i += 1
    i0 = i
    tia_i0s.append(i0)
    while tia_data[sn][0][i] < t1 and i < tia_data[sn].shape[1] - 1:
        i += 1
    i1 = i
    tia_i1s.append(i1)

f = plt.figure(constrained_layout=True, figsize=(12,5), dpi=100)
gs = gridsp.GridSpec(10, 50, figure=f)

# do the plot
# scatter tia, bottom panel
ax_tia = f.add_subplot(gs[5:10,:48])
for i0, i1, tiad in zip(tia_i0s, tia_i1s, tia_data.values()):
    tia_scat = ax_tia.scatter(tiad[0][i0:i1], tiad[1][i0:i1], c=tiad[2][i0:i1], vmin=vmin, vmax=vmax)
# scatter ctm timeseria, top panel
ax_ctms = f.add_subplot(gs[:5,:48])
for i0, i1, t, p, v in zip(i0s, i1s, times, press, values):
    scat = ax_ctms.scatter(t[i0:i1], p[i0:i1], c=v[i0:i1], vmin=vmin, vmax=vmax)
# plot manipulations
ax_ctms.set_ylim([0,70])
ax_ctms.invert_yaxis()
ax_ctms.grid(which="major", axis='x')
ax_ctms.set_ylabel("Pressure [dbar]")

ax_ctms.set_xlim([t0, t1])
ax_ctms.xaxis.set_major_formatter(md.DateFormatter(""))

ax_tia.set_ylim([0,70])
ax_tia.invert_yaxis()
ax_tia.grid(which="major", axis='x')
ax_tia.set_xlabel("Time [hh:mm]")
ax_tia.set_ylabel("Pressure [dbar]")

ax_tia.set_xlim([t0, t1])
ax_tia.xaxis.set_major_formatter(md.DateFormatter("%H:%M"))
cax = f.add_subplot(gs[:, 49:])
cbar = f.colorbar(tia_scat, cax=cax, label="Temperature [°C]")
plt.show()

f.savefig(os.path.join(basepath, "ctm-ts_vs_tia.png"))