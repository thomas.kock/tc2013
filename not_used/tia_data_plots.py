""" 
Load TIA data sets (post processed) and make nice plots
"""

from copy import deepcopy
import glob
import os.path
import datetime
import netCDF4
import numpy
import matplotlib
import matplotlib.pyplot as plt
import utm
import cmocean

#TODO 
# savefig
# autoscale ylim
# subex
# refactor towards API based?!

CMCMAPS = {
    4: cmocean.cm.thermal,
    5: cmocean.cm.haline,
    6: cmocean.cm.dense,
    7: cmocean.cm.ice,
    8: cmocean.cm.algae
}

# dat dir basepaths
paths = {
    "m160" : "/home/thomas/devl/tc2013/Data/m160",
    "subex" : "/home/thomas/devl/tc2013/Data/subex16/20160627",
    "tara": "/home/thomas/devl/tc2013/Data/tara"
    }

# order lists are ordered deep to shallow, but we need shallow to deep
ctm_orders = {
    "m160" : [985,1120,986,1112,1584,1115,1580,993,1117,1114,1581,989,1574,1573,1582,984][::-1],
    "tara" : [987,993,988,1115,989,1116,1580,1119,1584][::-1],
    "subex" : [993,1115,1120,990,1113,986,991],
    }

# time limits
time_lims = {
    "m160": (datetime.datetime(2019,12,13,14,30), datetime.datetime(2019,12,13,16)),
    "tara": (datetime.datetime(2021,8,31,2), datetime.datetime(2021,8,31,4,8)),
    "subex": (datetime.datetime(2016,6,27,10,12), datetime.datetime(2016,6,27,10,28))
}

var_names = {
    "subex": "Time,Latitude,Longitude,P,T,S,RHO".split(','),
    "m160": "Time,Latitude,Longitude,Pressure,TemperaturePT100,SalinityPT100,SigmaPT100".split(','),
    "tara": "Time,Latitude,Longitude,Pressure,TemperaturePT100,SalinityPT100,SigmaPT100,OxygenSatuSS,FluoCycl".split(',')
}
# vars of interest
var_units = [
    "[d since 1970-01-01]",
    "[deg]",
    "[deg]",
    "[dbar]",
    "[°C]",
    "[PSU]",
    "[kg/m³]",
    "[%]",
    "[???]"
]

def load_files(datapath: str, order: list) -> list:
    files = glob.glob(os.path.join(datapath, "*.nc"))
    tia_files = []
    for sn in order:
        for fn in files:
            if str(sn) in fn:
                tia_files.append(fn)
                break
    return tia_files

def compute_dmg(tia: numpy.ndarray, cf: float = 1000) -> numpy.ndarray:
    e, n = utm.from_latlon(tia[1,0], tia[2,0])[:2]
    dd = numpy.zeros_like(tia[1,0])
    dd[1:] = numpy.hypot(e[1:] - e[:-1], n[1:] - n[:-1])
    dd = numpy.cumsum(dd)
    return numpy.tile(dd, n_probes).reshape(n_probes, -1)/cf

def get_time_indeces(tia: numpy.ndarray, t0_dn: float, t1_dn: float) -> tuple:
    # find time window indeces
    i0 = 0
    i1 = 0
    imax = tia.shape[2]
    while tia[0, 0, i0] < t0_dn:
        i0 += 1
    while tia[0, 0, i1] < t1_dn and (i1+1) < imax:
        i1 += 1
    return i0, i1

def plot_oxy_chloro(tia: numpy.ndarray) -> tuple:
    vmin_oxy = numpy.nanmin(tia[7,:,i0:i1])
    vmax_oxy = numpy.nanmax(tia[7,:,i0:i1])
    vmin_chl = 0
    vmax_chl = 1
    f = plt.figure(constrained_layout=True, figsize=(21,7), dpi=100)
    gs = matplotlib.gridspec.GridSpec(13, 50, figure=f)
    ax = f.add_subplot(gs[:,:47])
    for i in range(n_probes):
        oxy = ax.scatter(nd[i], tia[3,i,cfi0:cfi1], c=tia[7,i,cfi0:cfi1], vmin=vmin_oxy, vmax=vmax_oxy, marker='|', s=150, cmap="BuPu")
        chl = ax.scatter(nd[i], tia[3,i,cfi0:cfi1], c=tia[8,i,cfi0:cfi1], vmin=vmin_chl, vmax=vmax_chl, marker='|', s=150, cmap="YlGn")
    cax_oxy = f.add_subplot(gs[:6, 47])
    cax_chl = f.add_subplot(gs[7:, 47])
    f.colorbar(oxy, cax=cax_oxy, label=f"{var_names[cruise][7]} [%]")
    f.colorbar(chl, cax=cax_chl, label=f"{var_names[cruise][8]} [??]")
    ax.set_xlabel("Distance made good [km]")
    ax.set_ylabel("Pressure [dbar]")
    ax.set_title("TIA Oxygen & Chl-A (Fluorescence)")
    return f

################################################################################################
################################################################################################
################################################################################################
################################################################################################

if __name__ == "__main__":

    # switches
    cruise = "m160" # m160, tara, subex
    x_data = "Distance" # Time, Distance
    plottype = "scatter" # scatter, contourf or pcolormesh
    var_idx = 6
    offs = 500
    probe_depths = False
    probe_labels = True
    saveplot = True
    showplot = True
    nlabels = 1
    label_color = "k"
    sm = 'o'
    em = 'X'
    msize = 100
    mc = 'k'
    minutes = 10
    ylim = [0,60]
    vmin = 0
    vmax = 0
    scatter_marker = 'o'
    scatter_size = 175
    plt.rcParams["font.size"] = 18
    plt.rcParams["grid.linestyle"] = ':'
    plt.rcParams["grid.color"] = 'lightgray'
    plt.rcParams["grid.linewidth"] = 1

    cmap = CMCMAPS[var_idx]
    cut_off_cmap = cmap.copy()
    cut_off_cmap.set_over(color="lightgray")
    cut_off_cmap.set_under(color="lightgray")

    # time constraints and epoch
    t_epoch = datetime.datetime(1970,1,1)
    t0, t1 = time_lims[cruise]
    t0_dn = (t0 - t_epoch).days + (t0 - t_epoch).seconds/86400
    t1_dn = (t1 - t_epoch).days + (t1 - t_epoch).seconds/86400

    # load data
    tia = None
    tia_files = load_files(paths[cruise], ctm_orders[cruise])
    n_probes = len(tia_files)
    if cruise == "subex": 
        press_name = "P"
    else:
        press_name = "Pressure"
    for i, f in enumerate(tia_files):
        print(f"Loading file: {f.split('/')[-1]}")
        with netCDF4.Dataset(f, "r") as ds:
            ndata_types = len(var_names[cruise])
            if not isinstance(tia, numpy.ndarray):
                tia = numpy.zeros((ndata_types, n_probes, ds.dimensions["time"].size))
            for j, vn in enumerate(var_names[cruise]):
                tia[j,i,:] = ds.variables[vn][:].data
    distance = compute_dmg(tia)
    i0, i1 = get_time_indeces(tia, t0_dn, t1_dn)
    cfi0 = i0-offs
    cfi1 = i1+offs
    nd = distance[:,cfi0:cfi1] - distance[0,cfi0+offs]
    tia[3][numpy.isnan(tia[3])] = 0

    if not vmin:
        vmin = numpy.nanmin(tia[var_idx,:,i0:i1])
    if not vmax:
        vmax = numpy.nanmax(tia[var_idx,:,i0:i1])

    # plot using gridspec for subplots spacing
    f = plt.figure(constrained_layout=True, figsize=(21,7), dpi=100)
    gs = matplotlib.gridspec.GridSpec(1, 100, figure=f)
    # set pressure nans to 0 for plotting
    tia[3][numpy.isnan(tia[3])] = 0
    # grey out values outside the interesting range
    surf_data = deepcopy(tia[var_idx,0])
    surf_data[:i0] = -1
    surf_data[i1:] = -1
    # axes instance for ship track (lat lon -> ll)
    ax_ll = f.add_subplot(gs[:,:27])
    ax_ll.scatter(tia[2,0], tia[1,0], c=surf_data, vmin=vmin, vmax=vmax, cmap=cut_off_cmap)
    ax_ll.scatter(tia[2,0,i0], tia[1,0,i0], c=mc, marker=sm, s=msize)
    ax_ll.scatter(tia[2,0,i1], tia[1,0,i1], c=mc, marker=em, s=msize)
    ax_ll.axis("equal")
    ax_ll.xaxis.set_major_formatter(matplotlib.ticker.FormatStrFormatter("%4.2f"))
    ax_ll.yaxis.set_major_formatter(matplotlib.ticker.FormatStrFormatter("%5.2f"))
    ax_ll.set_ylabel("Latitude")
    ax_ll.set_xlabel("Longitude")
    ax_ll.grid(which="both")
    ax_ll.text(
        0.05 * (96-28)/100, 0.03, "a", 
        color="k", verticalalignment="bottom", horizontalalignment="left", fontsize=32,
        transform=ax_ll.transAxes, 
        fontweight="bold", bbox=dict(facecolor='w')
        )
    # axes instance for contourf plot
    ax_cf = f.add_subplot(gs[:,28:96])
    if x_data == "Distance":
        if plottype == "scatter":
            for i in range(n_probes):
                print(f"Plotting probe {ctm_orders[cruise][i]}, {i+1} of {n_probes}")
                qcs = ax_cf.scatter(
                    nd[i], tia[3,i,cfi0:cfi1], c=tia[var_idx,i,cfi0:cfi1], 
                    vmin=vmin, vmax=vmax, 
                    marker=scatter_marker, s=scatter_size, cmap=cmap
                    )

        if plottype == "contourf":
            qcs = ax_cf.contourf(
                nd,
                tia[3,:,cfi0:cfi1],
                tia[var_idx,:,cfi0:cfi1],
                levels=numpy.linspace(round(vmin, 1)-0.1, round(vmax, 1) + 0.1, 41),
                cmap=cmap,
                # extend="both" # this is the desired setting, it is however buggy in matplotlib 3.5.1 and should be fixed in 3.5.2, but is not released yet
            )

        if plottype == "pcolormesh":
            qcs = ax_cf.pcolormesh(
                nd,
                tia[3,:,cfi0:cfi1],
                tia[var_idx,:,cfi0:cfi1],
                shading="auto",
                vmin=vmin,
                vmax=vmax,
                cmap=cmap
            )

        if probe_depths:
            for i in range(n_probes):
                ax_cf.plot(nd[i], tia[3,i,cfi0:cfi1], c="w", lw=0.3)

        if probe_labels:
            span = int((cfi1 - cfi0)/(nlabels +1))
            for i in range(n_probes):
                for j in range(1, nlabels+1):
                    ax_cf.text(nd[i, j * span], tia[3,i,cfi0 + j * span], str(ctm_orders[cruise][i]), 
                    va="center", ha="center", color=label_color, fontweight="bold", fontsize=8)

        ax_cf.scatter(nd[0,  offs], tia[3,0,i0], marker=sm, c=mc, s=msize, zorder=3)
        ax_cf.scatter(nd[0, -offs], tia[3,0,i1], marker=em, c=mc, s=msize, zorder=3)
        ax_cf.set_xlim([nd[0, 0], nd[0, -1]])
        ax_cf.set_xlabel("Along track distance [km]")
    
    elif x_data == "Time":
        if plottype == "scatter":
            for i in range(n_probes):
                print(f"Plotting probe {ctm_orders[cruise][i]}, {i+1} of {n_probes}")
                qcs = ax_cf.scatter(
                    tia[0,i,cfi0:cfi1], tia[3,i,cfi0:cfi1], c=tia[var_idx,i,cfi0:cfi1], 
                    vmin=vmin, vmax=vmax, 
                    marker=scatter_marker, s=scatter_size, cmap=cmap
                    )

        if plottype == "contourf":
            qcs = ax_cf.contourf(
                tia[0,:,cfi0:cfi1],
                tia[3,:,cfi0:cfi1],
                tia[var_idx,:,cfi0:cfi1],
                levels=numpy.linspace(round(vmin, 1)-0.1, round(vmax, 1) + 0.1, 41),
                cmap=cmap,
                # extend="both" # this is the desired setting, it is however buggy in matplotlib 3.5.1 and should be fixed in 3.5.2, but is not released yet
            )

        if plottype == "pcolormesh":
            qcs = ax_cf.pcolormesh(
                tia[0,:,cfi0:cfi1],
                tia[3,:,cfi0:cfi1],
                tia[var_idx,:,cfi0:cfi1],
                shading="auto",
                vmin=vmin,
                vmax=vmax,
                cmap=cmap
            )

        if probe_depths:
            for i in range(n_probes):
                ax_cf.plot(tia[0,i,i0:i1], tia[3,i,i0:i1], c="w", lw=0.3)
        if probe_labels:
            span = int((i1 - i0)/(nlabels +1))
            for i in range(n_probes):
                for j in range(1, nlabels+1):
                    ax_cf.text(tia[0, i, i0 + j * span], tia[3,i,i0 + j * span], str(ctm_orders[cruise][i]), 
                    va="center", ha="center", color=label_color, fontweight="bold", fontsize=8)
        ax_cf.scatter(tia[0, 0, i0], tia[3,0,i0], marker=sm, c=mc, s=msize, zorder=3)
        ax_cf.scatter(tia[0, 0, i1], tia[3,0,i1], marker=em, c=mc, s=msize, zorder=3)
        ax_cf.set_xlabel("UTC time [HH:MM]")
        ax_cf.xaxis.set_ticks([t0_dn + i * minutes/(24*60) for i in range(int((t1_dn - t0_dn)*24*60 / minutes) + 1)])
        ax_cf.xaxis.set_major_formatter(matplotlib.dates.DateFormatter("%H:%M"))
        ax_cf.set_xlim([tia[0,0,cfi0], tia[0,0,cfi1]])

    ax_cf.grid(which="major", axis="x")
    ax_cf.set_ylim(ylim)
    ax_cf.invert_yaxis()
    ax_cf.set_ylabel("Pressure [dbar]")
    ax_cf.text(
        0.05 * 27/100, 0.03, 
        "b", color="k", verticalalignment="bottom", horizontalalignment="left", fontsize=32, 
        transform=ax_cf.transAxes, 
        fontweight="bold", bbox=dict(facecolor='w')
        )
    ax_cbar = f.add_subplot(gs[:,98:])
    cbar = f.colorbar(qcs, cax=ax_cbar, format="%3.1f", label=f"{var_names[cruise][var_idx]} [{var_units[var_idx]}]")
    if saveplot: plt.savefig(os.path.join(paths[cruise], f"{cruise.upper()}_{plottype}_{var_names[cruise][var_idx]}_{x_data}.png"))
    if showplot: plt.show()
