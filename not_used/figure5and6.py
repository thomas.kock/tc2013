# creates a plot with 4 panels
# 0th panel: contour of temperature with probe tracks
# 1st panel: Top and bottem probe temperature horizotnal profiles
# 2nd panel: Abribratry salinity horizontal profile
# 3rd panel: oxygen horizontal profile

# imports needed in this script
from scipy.io import loadmat
import matplotlib.pyplot as plt
import utm
import gsw
import numpy
import cmocean

# set some switches and clean up workspace
plt.ion()
plt.close("all")
plt.rcParams["font.size"] = 18

vmin = 13
vmax = 16
cmap = cmocean.cm.thermal
full_ctds = [0, 4]
oxy = 4

# load data file for the plots
matfile = "/home/thomas/0_working_local/tc2013/20110414/Vessel/TIA_20110414_part2_transect07.mat"
data = loadmat(matfile)

# making the namespce flatter
lat = data["lat7"][0]
lon = data["lon7"][0]
T = data["T7"]
S = data["S7"]
O2 = data["O2_7"]
e, n = utm.from_latlon(lat, lon)[:2]
dx = numpy.zeros_like(e)
dx[1:] = numpy.hypot(e[1:] - e[:-1], n[1:] - n[:-1])
dx = numpy.cumsum(dx)
DX = numpy.tile(dx, 9).reshape(9, len(dx))
P = data["P7"]

# plotting
fig, ax = plt.subplots(4, 1, num="Figure5", sharex=True, gridspec_kw={"height_ratios": [2,1,1,1]}, figsize=(16,16), dpi=120)
# the contour plot
ctf = ax[0].contourf(DX, P, T, cmap=cmap, extend="both", levels=numpy.linspace(vmin, vmax, 100, endpoint=True))
for i in range(len(P)):
    if i in full_ctds:
        ax[0].plot(dx, P[i], c="black", lw=0.7)
    else:
        ax[0].plot(dx, P[i], c="lightgray", lw=0.7)

# temp, top and fifth probe

ax[1].plot(DX[0], T[0], c="r", label="Micro TSG", lw=0.7)
ax[1].plot(DX[0], T[4], c="b", label="CTD @ 7.7 m", lw=0.7)
ax[2].plot(DX[0], S[0], c="r", label="Micro TSG", lw=0.7)
ax[2].plot(DX[0], S[4], c="b", label="CTD @ 7.7 m", lw=0.7)
ax[3].plot(DX[0], O2[oxy], label="CTD @ 7.7 m", c="b", lw=0.7)

ax[0].set_ylim([0,30])
ax[0].set_ylabel("Pressure [dbar]")
ax[0].invert_yaxis()
ax[1].set_ylim([13,16])
ax[1].set_ylabel("Temperature [°C]")
ax[1].legend(loc="lower right")
ax[2].set_ylabel("Salinity [PSU]")
ax[2].set_ylim([28, 34])
ax[2].legend(loc="lower right")
ax[3].set_ylabel(r"O$_2$ [ml/L]")
ax[3].set_ylim([6.7, 7.4])
ax[3].set_xlabel("Distance [m]")
ax[3].legend(loc="lower right")
for i in range(1,4):
    ax[i].grid(True, ls="dotted")
cbar = fig.colorbar(ctf, ax=ax[0], location="bottom", fraction=0.1, shrink=0.33, pad=0.1)
cbar.set_ticks([13,14,15,16])
cbar.ax.set_xlabel("Temperature [°C]")
fig.tight_layout()
try:
    fig.savefig("/run/user/1000/gvfs/smb-share:server=a0mn1600001,share=share_a0mn1600001/figure5.png")
except FileNotFoundError:
    print("Could not save to share... trying 0_working_local")
    fig.savefig("/home/thomas/0_working_local/tc2013/figure5.png")


# figure 6, rho plots and stddev of rho
# gsw toolbox follows the TEOS 10 standards.

SA = gsw.SA_from_SP(S, P, lon, lat)
CT = gsw.CT_from_t(SA, T, P)
rho = gsw.rho(SA, CT, P)
sigma = gsw.sigma0(SA[4], CT[4])

# std dev of probe at index 4
std_err_SA = numpy.std(SA[4])
# SA minus and plus 1 std deviation
sigma_m1sal_std_err = gsw.sigma0(SA[4] - std_err_SA, CT[4])
sigma_p1sal_std_err = gsw.sigma0(SA[4] + std_err_SA, CT[4])
# SA minus and plus 2 std deviation
sigma_m2sal_std_err = gsw.sigma0(SA[4] - 2*std_err_SA, CT[4])
sigma_p2sal_std_err = gsw.sigma0(SA[4] + 2*std_err_SA, CT[4])
# std deviation of sigma 
std_err_sigma = numpy.std(sigma)

# sigma with indicated SA induced errors 
fr, ar = plt.subplots(1,1,num="rho", figsize=(16,8), dpi=150)
ar.fill_between(DX[4],sigma_m2sal_std_err, sigma_p2sal_std_err, color="lightgray", label="2 sal stddev", edgecolor=None)
ar.fill_between(DX[4],sigma_m1sal_std_err, sigma_p1sal_std_err, color="whitesmoke", label="1 sal stddev", edgecolor=None)
ar.plot(DX[4], sigma, label=r"$\sigma_0$", c="k", lw=0.7)
ar.set_ylabel(r"$\sigma_0$ [kg/m³]")
ar.set_xlabel("Distance [m]")
ar.grid(True, ls="dotted")
ar.legend()
fr.tight_layout()
try:
    fr.savefig("/run/user/1000/gvfs/smb-share:server=a0mn1600001,share=share_a0mn1600001/figure6.png")
except FileNotFoundError:
    print("Could not save to share... trying 0_working_local")
    fr.savefig("/home/thomas/0_working_local/tc2013/figure6.png")
