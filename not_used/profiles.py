import matplotlib.pyplot as plt
from scipy.io import loadmat
import numpy

plt.ion()
plt.close("all")
plt.rcParams["font.size"] = 20

def compute_t_error(data: numpy.ndarray, dT: float, dP: float, relative: bool=False) -> numpy.ndarray:
    dT_over_dP_sqared = (data[1:] - data[:-1]) ** 2
    if relative:
        return numpy.sqrt(dT ** 2 + dT_over_dP_sqared * dP ** 2) / data[1:]
    else:
        return numpy.sqrt(dT ** 2 + dT_over_dP_sqared * dP ** 2)

basepath = "/home/thomas/0_working_local/tc2013/20110414/Vessel/TIA_20110414_part1.mat"
data = loadmat(basepath)

ctd_params = ['T', 'S', "O2"]
ctd_param_labels = {
    'T': "[°C]",
    'S': "[PSU]",
    "O2": "[%]"
}

f, axs = plt.subplots(1,3,num="Profiles", sharey=True)
for i, ctd_param in enumerate(ctd_params):
    axs[i].plot(data[ctd_param + "_down"][0], data["P_down"][0], c="blue", label=ctd_param + "_down")
    axs[i].plot(data[ctd_param + "_up"][0], data["P_up"][0], c="red", label=ctd_param + "_up")
    axs[i].legend(loc="lower right")
    axs[i].set_xlabel(ctd_param + ctd_param_labels[ctd_param])

axs[0].set_ylabel("Pressure [dbar]")
axs[-1].set_ylim([0,110])
axs[-1].invert_yaxis()
plt.show()


f_err, ax_err = plt.subplots(1,3, num="Error", sharey=True, gridspec_kw={"width_ratios":[1,3,1]}, figsize=(16,10), dpi=120)
ax_err[0].plot(compute_t_error(data["T_down"][0], dT=0.002, dP=0.37, relative=False), data["P_down"][0][1:], c="blue", ls="solid", lw=1)
ax_err[0].plot(compute_t_error(data["T_down"][0], dT=0.002, dP=0.37, relative=True), data["P_down"][0][1:], c="blue", ls="dotted", lw=1)
ax_err[1].plot(data["T_down"][0], data["P_down"][0], c="blue", label="Downcast")
ax_err[1].fill_betweenx(data["P_down"][0][1:], data["T_down"][0][1:] - compute_t_error(data["T_down"][0], dT=0.002, dP=0.37, relative=False), data["T_down"][0][1:] + compute_t_error(data["T_down"][0], dT=0.002, dP=0.37, relative=False), color="k", alpha=0.5, label="Error")
ax_err[1].fill_betweenx(data["P_up"][0][1:], data["T_up"][0][1:] - compute_t_error(data["T_up"][0], dT=0.002, dP=0.37, relative=False), data["T_up"][0][1:] + compute_t_error(data["T_up"][0], dT=0.002, dP=0.37, relative=False), color="k", alpha=0.5)
ax_err[1].plot(data["T_up"][0], data["P_up"][0], c="red", label="Upcast")
ax_err[2].plot(compute_t_error(data["T_up"][0], dT=0.002, dP=0.37, relative=False), data["P_up"][0][1:], c="red", ls="solid", lw=1)
ax_err[2].plot(compute_t_error(data["T_up"][0], dT=0.002, dP=0.37, relative=True), data["P_up"][0][1:], c="red", ls="dotted", lw=1)
ax_err[0].set_ylim([0,110])
ax_err[0].set_ylabel("Depth [dbar]")
ax_err[0].invert_yaxis()
ax_err[0].set_xlabel(r"Downcast Error $\Delta$T [°C]")
ax_err[1].set_xlabel("Temperature [°C]")
ax_err[2].set_xlabel(r"Upcast Error $\Delta$T [°C]")
ax_err[0].set_xlim([0,0.1])
ax_err[1].set_xlim([8,16])
ax_err[2].set_xlim([0,0.1])
ax_err[1].legend(loc="lower center")
for ax in ax_err:
    ax.grid(True)
f_err.tight_layout()
f_err.savefig("/home/thomas/0_working_local/tc2013/error.png")