"""
A collection of classes to simulate several kinds of towed instruments for a comparison study

This is the base for the simulated instruments used in the analytical comparison study in the
adcvanced tow chain paper

Author: thomas.kock@hereon.de
"""
import abc

# dependencies, this module
import libmodeldata

# dependencies, external
import numpy
import matplotlib.pyplot as plt
import scipy.interpolate

def compute_dval_dx(val_array: numpy.ndarray, dx: float = 1) -> numpy.ndarray:
    """
    Quick and dirty function to compute the derivative of value over dx.
    Not used, only for experiments with data set.
     """
    derived_arr = numpy.zeros_like(val_array)
    derived_arr[:,1:] = (val_array[:,1:] - val_array[:,:-1]) / dx
    derived_arr[:,0] = derived_arr[:,1]
    return derived_arr

class Instrument(abc.ABC):

    def __init__(self, figsize: tuple = (16,10), dpi: int = 100, cmap: str = "viridis") -> None:
        super().__init__()
        self.instrument: str
        self.x_samples: numpy.ndarray
        self.z_samples: numpy.ndarray
        self.t_samples: numpy.ndarray
        self.model: libmodeldata.DataModel
        self.nx: int
        self.nz: int
        self.temp_array: numpy.ndarray

        self.FIGSIZE = figsize 
        self.DPI = dpi
        self.CMAP = cmap

    @abc.abstractmethod
    def simulate_data_collection(self):
        pass

    @abc.abstractmethod
    def interpolate_to_model_grid(self) -> numpy.ndarray:
        pass

    @abc.abstractmethod
    def plot_data(self) -> plt.figure:
        pass


class TIA(Instrument):
    """
    Class representing a towed CTD chain as a subclass of Instrument

    Args:
        Instrument (Instrument): abc.ABC Instrument of this module
    """

    def __init__(self, model: libmodeldata.DataModel, sensor_depths = (2, 30, 10), sample_rate: float=1, boat_speed: float=5, cmap: str = "viridis"):
        """
        Constructor method for towed instrument chain representation

        Args:
            model (DataModel): The DataModel Class this instance relies on for input data
            sensor_depths (tuple, optional): Either len=3 tuple for numpy.arange call or list of sensor depth. 
            Tuple format must be (First depth, last depth, num_probes). Defaults to (2, 30, 10).
            sample_rate (float, optional): Sample rate in Hz the simulated tow chain samples with. Defaults to 1.
            boat_speed (float, optional): Speed of the towing boat in m/s. Defaults to 5.

        Raises:
            TypeError: If sensor_depths is not either a tuple or a list
        """
        super().__init__()
        self.instrument = "TIA"
        self.model = model
        self.V = boat_speed
        
        print("\nSimulating a tow chain with")
        
        if type(sensor_depths) == tuple:
            self.SENSOR_DEPTHS = numpy.linspace(*sensor_depths)
        elif type(sensor_depths) == list:
            self.SENSOR_DEPTHS = numpy.array(sensor_depths)
        else:
            raise TypeError(f"Sensor_list must be either a tuple (Startdepth, Enddepth, Number of Sensor) or a list with depths")
        
        self.SAMPLING_RATE = sample_rate
        self.nz = self.sensor_count =  len(self.SENSOR_DEPTHS)
        self.nx = int(self.model.DOMAIN_LENGTH / self.V * self.SAMPLING_RATE)
        self.t_samples = numpy.zeros((self.sensor_count, self.nx))
        self.x_samples = numpy.arange(0, self.model.DOMAIN_LENGTH, step=self.V/self.SAMPLING_RATE)
        print(f"TIA number of sensors: {self.sensor_count}")
        print(f"TIA sensor spacing: {self.SENSOR_DEPTHS[1] - self.SENSOR_DEPTHS[0]:.2f} m")
        print("TIA sensor depths in m: ", end="")
        [print(f"{_:.2f}, ", end="") for _ in self.SENSOR_DEPTHS]
        print()
        print(f"TIA Sampling Rate: {self.SAMPLING_RATE} 1/s")
        print(f"TIA boat speed: {self.V} m/s")

    def simulate_data_collection(self):
        """
        Method to simulate which data the towed chain would sample.
        """
        for i in range(self.sensor_count):
            sensor_depth_idx = int(self.SENSOR_DEPTHS[i] / self.model.Z_STEP)
            if sensor_depth_idx == self.model.temp_array.shape[1]:
                sensor_depth_idx -= 1
            f_sensor = scipy.interpolate.interp1d(
                self.model.distances, self.model.temp_array[:, sensor_depth_idx], 
                kind="linear", fill_value="extrapolate"
                )
            self.t_samples[i] = f_sensor(self.x_samples)

    def interpolate_to_model_grid(self):
        """
        Interpolates the simulated tow chain samples back to the model grid
        """
        f_tia_interp2d = scipy.interpolate.interp2d(
            self.x_samples, self.SENSOR_DEPTHS, self.t_samples, 
            kind="linear", bounds_error=False, fill_value=numpy.nan
            )
        self.temp_array = f_tia_interp2d(self.model.distances, self.model.z)
    
    def plot_data(self, xlim: tuple = ()):
        """
        Convenience plotter method for tow chain data

        Args:
            figsize ([type], optional): [description]. Defaults to FIGSIZE.
            dpi ([type], optional): [description]. Defaults to DPI.
        """
        if not xlim:
            x0 = 0
            x1 = self.model.x_size * self.model.X_STEP
        else:
            x0, x1 = xlim

        depth_layers = numpy.zeros((self.nx, self.sensor_count))
        fig, ax = plt.subplots(
            3, 1, num=self.instrument, figsize=self.FIGSIZE, dpi=self.DPI, sharex=True,
            facecolor='w'
            )
        # plot of probe data as a scatter time series plot with color
        for i in range(self.sensor_count):
            depth_layers[:, i] = numpy.ones_like(self.x_samples) * self.SENSOR_DEPTHS[i]
            ax[0].scatter(self.x_samples, depth_layers[:, i], c=self.t_samples[i], cmap=self.CMAP, vmin = self.model.min_temp, vmax = self.model.max_temp)
        # plot of interpolated data
        ax[1].contourf(self.model.distances, self.model.z, self.temp_array, cmap=self.CMAP,
            vmin = self.model.min_temp, vmax = self.model.max_temp, extend = "both",
            levels=self.model.levels)
        for i in range(self.sensor_count):
            ax[1].scatter(self.x_samples, depth_layers[:, i], c="gray", s=2)
        # delta T plot of interpolated and real data
        ax[2].pcolormesh(self.model.distances, self.model.z, (self.model.temp_array.T - self.temp_array), cmap="seismic", vmin=-1, vmax=1, shading="auto")
        # plot manipulations
        titles = [
            f"{self.instrument}: Simulated collected data",
            f"{self.instrument}: Interpolated data",
            f"{self.instrument}: Modelled data - interpolated sampled data"
            ]
        for i, a in enumerate(ax):
            a.set_ylim([0, self.model.MAX_DEPTH])
            a.invert_yaxis()
            a.set_ylabel("Depth [m]")
            a.set_title(titles[i])
        ax[-1].set_xlim([x0, x1])
        ax[-1].set_xlabel("Distance [m]")
        return(fig)

class Undulating(Instrument):
    """
    Class representation of a Scanfish like instrument for use with a libmodeldata.DataModel 
    of this module

    Args:
        Instrument (Instrument): Instrument abc.ABC to derive from
    """

    def __init__(self, model: libmodeldata.DataModel, dive_rate: float=1, sample_rate: float=2, min_depth: float=2,
        offset: float=10, boat_speed: float=5, cmap: str = "viridis"):
        """
        Constructor method of scanfish class. 

        Args:
            model (DataModel): DataModel this class relies on for input data
            dive_rate (float, optional): How fast the instrument dives in m/s. Defaults to 1.
            sample_rate (float, optional): How fast the instrument samples in Hz. Defaults to 2.
            min_depth (float, optional): Minimum depth of instrument in m. 
            For practical reasons it is not possible to measure all the way to the surface with the scanfish. Defaults to 2.
            offset (float, optional): Starting depth of sampling in the model domain in m. 
            Setting a different value may change the outcome of the simulation drastically. Defaults to 10.
            boat_speed (float, optional): Speed of the boat in m/s. Defaults to 5.
        """
        super().__init__()
        print("\nSimulating a undulating device like EIVA Scanfish or SeaSoar")
        self.instrument = "Undulating"
        self.DIVE_RATE = dive_rate # m/s
        self.SAMPLE_RATE = sample_rate # Hz
        self.MIN_DEPTH = min_depth # M
        self.MAX_DEPTH = model.MAX_DEPTH
        self.START_OFFSET = offset # m
        self.V = boat_speed
        self.model = model

        print(f"SF Dive Rate: {self.DIVE_RATE} m/s")
        print(f"SF Sample Rate: {self.SAMPLE_RATE} 1/s")
        print(f"SF Boat Speed: {self.V} m/s")

        self.x_samples = numpy.arange(0, self.model.DOMAIN_LENGTH, step = self.V / self.SAMPLE_RATE, dtype="f2")
        self.z_samples = numpy.ones_like(self.x_samples) * self.MIN_DEPTH
        self.z_samples[0] = self.z_samples[0] + self.START_OFFSET
        self.nx = self.x_samples.shape[0]

    def simulate_data_collection(self):
        """
        Simulate the data collection of the scanfish like device
        """
        print("Simulate sampling with undulating device ... ", end="")
        
        dives = True
        for i in range(1, self.nx):
            if dives:
                self.z_samples[i] = self.z_samples[i-1] + (self.DIVE_RATE / self.SAMPLE_RATE)
                if self.z_samples[i] >= self.MAX_DEPTH:
                    dives = False
            else:
                self.z_samples[i] = self.z_samples[i-1] - (self.DIVE_RATE / self.SAMPLE_RATE)
                if self.z_samples[i] <= self.MIN_DEPTH:
                    dives = True

        # find closest possible temp values
        self.t_samples = numpy.zeros_like(self.x_samples)
        for i in range(self.nx):
            # find x idx in model_temp_array
            xidx = 0
            zidx = 0
            while self.model.distances[xidx] < self.x_samples[i] and xidx < self.model.x_size -1:
                xidx += 1
            while self.model.z[zidx] < self.z_samples[i] and zidx < self.model.z_size - 1:
                zidx += 1
            self.t_samples[i] = self.model.temp_array[xidx, zidx]
        print("done")

    def interpolate_to_model_grid(self):
        """
        Interpolate sampled data to model data grid using scipy griddata function
        """
        print("Interpolating undulating device data to model grid ... ", end="")
        self.temp_array = scipy.interpolate.griddata(
            numpy.stack((self.x_samples, self.z_samples), axis=1), # input coordinates, unstructured (n, 2d) grid
            self.t_samples, # input temperature samples
            tuple(numpy.meshgrid(self.model.distances, self.model.z)), # model grid points to evaluate data at
            rescale=True, method="linear")  # options
        print("done")

    def plot_data(self, xlim: tuple = (), size: int = 1) -> plt.figure:
        """
        Plots simulated scanfish data and differences to input model data

        Args:
            figsize (tuple, optional): Tuple of width and height in inch. Defaults to FIGSIZE.
            dpi (int, optional): DPI of figure. Defaults to DPI.
            size (int, optional): Size of scatter plot dots. Defaults to 1.

        Returns:
            matplotlib.pyplot.figure: Figure instance
        """
        if not xlim:
            x0 = 0
            x1 = self.model.x_size * self.model.X_STEP
        else:
            x0, x1 = xlim

        print("Plotting scanfish data ... ", end="")    
        fig, ax = plt.subplots(
            3, 1, num=self.instrument, sharex=True, figsize=self.FIGSIZE, dpi=self.DPI,
            facecolor='w'
            )
        ax[0].scatter(self.x_samples, self.z_samples, c=self.t_samples, cmap=self.CMAP,
            vmin=self.model.min_temp, vmax=self.model.max_temp)
        ax[1].contourf(self.model.distances, self.model.z, self.temp_array, cmap=self.CMAP,
            vmin=self.model.min_temp, vmax=self.model.max_temp, extend="both",
            levels=self.model.levels)
        ax[1].scatter(self.x_samples, self.z_samples, c="gray", s=size)
        ax[2].pcolormesh(self.model.distances, self.model.z, (self.model.temp_array.T - self.temp_array), vmin=-1, vmax=1, cmap="seismic", shading="auto")
        ax[0].set_xlim([0, self.model.DOMAIN_LENGTH])
        titles = [
            f"{self.instrument}: Simulated collected data",
            f"{self.instrument}: Interpolated data",
            f"{self.instrument}: Modelled data - interpolated sampled data"
            ]
        for i, a in enumerate(ax):
            a.set_ylim([0, self.model.MAX_DEPTH])
            a.invert_yaxis()
            a.set_ylabel("Depth [m]")
            a.set_title(titles[i])
        ax[-1].set_xlim([x0,x1])
        ax[-1].set_xlabel("Distance [m]")
        print("done")
        return fig

class UCTD(Instrument):
    """
    Class representing the simulation of an underway CTD system

    Args:
        Instrument (class Instrument): Instrument Base Class of this module
    """

    def __init__(self, model: libmodeldata.Model_InternalWave, drop_rate: float=4, recover_rate: float=0.89, sample_rate: float=10,
        spool_time: float=0, offset: float=100, boat_speed: float=5, cmap: str = "viridis"):
        """
        Constructor method of uCTD class. Initial values are based on [1]

        [1] Rudnick, D. L. and Klinke, J. (2007) The Underway Conductivity-Temperature-Depth Instrument, Journal of Atmospheric and Oceanic Technology, 24(11), pp. 1910-1923. doi: 10.1175/JTECH2100.1.

        Args:
            model (Model_InternalWave): Instance of underlying data model
            drop_rate (float, optional): Sinking speed of uCTD in m/s. Defaults to 4.
            recover_rate (float, optional): Recover speed of uCTD in m/s. Defaults to 0.89.
            sample_rate (float, optional): Sample rate of uCTD in Hz. Defaults to 10.
            spool_time (float, optional): Time it takes to do the tail spooling of the uCTD in s. Defaults to 0.
            offset (float, optional): Offset with respect to model domain start in m. May significantly change moddeling outcome. Defaults to 100.
            boat_speed (float, optional): Speed of the boat in m/s. Defaults to 5.
        """
        super().__init__()
        self.instrument = "UCTD"
        self.DROP_RATE = drop_rate # m/s
        self.RECOVER_RATE = recover_rate # m/s as calculated from 800 m / (15 min * 60 s/min)
        self.SAMPLE_RATE = sample_rate # hz
        self.SPOOL_TIME = spool_time # s
        self.OFFSET = offset # m
        self.model = model
        self.V = boat_speed

        print("\nSimulating an underway CTD as described in Rudnick and Klinke 2007")
        drop_time = self.model.MAX_DEPTH / self.DROP_RATE
        recover_time = self.model.MAX_DEPTH / self.RECOVER_RATE
        self.profile_distance = self.V * (drop_time + recover_time + self.SPOOL_TIME)
        self.nx = int((self.model.DOMAIN_LENGTH - self.OFFSET) / self.profile_distance) + 1
        self.z_samples = numpy.arange(0, self.model.MAX_DEPTH, step = self.DROP_RATE / self.SAMPLE_RATE)
        self.nz = self.z_samples.shape
        self.t_samples = numpy.zeros((self.nx, int(drop_time * self.SAMPLE_RATE)))
        self.x_samples = numpy.zeros((self.nx,))
        print(f"uCTD drop rate: {self.DROP_RATE} m/s")
        print(f"uCTD Recover Rate: {self.RECOVER_RATE} m/s")
        print(f"uCTD Sample Rate: {self.SAMPLE_RATE} 1/s")
        print(f"uCTD respool time: {self.SPOOL_TIME} s")

    def simulate_data_collection(self):
        """
        Simulation of data collection with the uCTD
        """
        print("Simulating UCTD sampling ... ", end="")
        x_idx = 0
        for i in range(self.nx):
            self.x_samples[i] = i * self.profile_distance + self.OFFSET# in m, translat to idx on model data
            x_idx = int(self.x_samples[i] / self.model.X_STEP)
            f_interp = scipy.interpolate.interp1d(
                self.model.z, self.model.temp_array[x_idx], 
                kind="linear"
                )
            self.t_samples[i] = f_interp(self.z_samples)
        print("done")
        
    def interpolate_to_model_grid(self):
        """
        Interpolate sampled data to model grid
        """
        print("Interpolating UCTD data to model grid ... ", end="")
        f_interp2d = scipy.interpolate.interp2d(
            self.x_samples, self.z_samples, self.t_samples.T, 
            kind="linear", bounds_error=False, fill_value=numpy.nan
            )
        self.temp_array = f_interp2d(self.model.distances, self.model.z)
        print("done")

    def plot_data(self, xlim: tuple = ()) -> plt.figure:
        """
        Convenience method to easily plot the produced data set

        Args:
            figsize (tuple, optional): length 2 tuple of width and height of the figure. Defaults to FIGSIZE.
            dpi (int, optional): DPI of the figure. Defaults to DPI.
        """
        if not xlim:
            x0 = 0
            x1 = self.model.x_size * self.model.X_STEP
        else:
            x0, x1 = xlim
        print("Plotting UCTD data ... ", end="")
        fig, ax = plt.subplots(
            3, 1, num=self.instrument, sharex=True, figsize=self.FIGSIZE, dpi=self.DPI,
            facecolor='w'
            )
        for i in range(self.nx):
            ax[0].scatter(numpy.ones_like(self.z_samples) * self.x_samples[i], 
                self.z_samples, c=self.t_samples[i], cmap=self.CMAP,
                vmin=self.model.min_temp, vmax=self.model.max_temp)

        ax[1].contourf(self.model.distances, self.model.z, self.temp_array, cmap=self.CMAP,
            vmin=self.model.min_temp, vmax=self.model.max_temp, levels=self.model.levels, extend="both")
        for d in self.x_samples:
            ax[1].scatter(numpy.ones_like(self.z_samples)*d, self.z_samples, c="gray", s=2)
        ax[2].pcolormesh(self.model.distances, self.model.z, (self.model.temp_array.T - self.temp_array), vmin=-1, vmax=1, cmap="seismic", shading="auto")
        titles = [
            f"{self.instrument}: Simulated collected data",
            f"{self.instrument}: Interpolated data",
            f"{self.instrument}: Modelled data - interpolated sampled data"
            ]
        for i, a in enumerate(ax):
            a.set_ylim([0, self.model.MAX_DEPTH])
            a.invert_yaxis()
            a.set_ylabel("Depth [m]")
            a.set_title(titles[i])
        ax[-1].set_xlim([x0, x1])
        ax[-1].set_xlabel("Distance [m]")
        print("done")
        return fig