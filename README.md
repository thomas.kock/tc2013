# Repository tc2013
tc2013 is a collection of scripts and sample files belonging to a publication about the Towed Instrument Array (TIA) developed at Helmholtz-Zentrum Geesthacht and its successor Helmholtz-Zentrum Hereon published in 2022. The scripts are used for data analysis and making of the plots in the paper. Below is a brief description of all scripts used in the final version of the paper.

The directory "not_used" contains scripts that are not used in the paper or are intermediate versions. These scripts are note fully documented and may not work. They are not deleted for the sake of completeness and transparency.

Author: Thomas Kock  
E-Mail: thomas.kock@hereon.de  
Updated: 2022-07-14  


# Base directory

## libinstruments.py
Model classes for the analytical comparison study of towed instrument for the submesoscale. Used
for Section 2 of the paper

## libmodeldata.py
Contains tools needed for creating model data sets for further analaysis.

## script_instrumentsim.ipynb
Jupyter notebook with the scripts for the instrument comparison presented in the paper

# Data directory

## compute_tia_shapes.py
A single purpose script to load, process and plot tia shape data. This script is used to produce .json files with tia shape data for several TIA deployments. The .json files are used later in shape_plots.py for a plot for the paper.

## force_data.py
Loads force gauge data for cruise M160 and also pressure data for the deepedt probe used during that campaign. Outcome of the script is a plot with normalized forces, pressures and SOG of vessel for paper.

## intercal_plots.py
Loads an intercalibration data set for TIA probes obtained during cruise M160 and makes several plots. This script was initially planned to be written in intercal, but I could not figure out the correct number PLEASE commands, so I switched back to python.

## lib_tia_arrangement.py
A library with information about TIA configuration used in various deployments. It is more of a database than anything else.

## postpro_eval.py
Loads, processes and plots data to create a figure for the paper showing the improvement of TIA data achieved by post processing with "CTD Program"

## shape_plots.py
Takes TIA shape data from .json files and plots the TIA shapes for several TIA deployments. Plot used in paper.

## tia_plotter.py
Final form of the plotting tools used for the paper. This tools might be useful later on, because it allows rudimentary interactive plotting of TIA data and can potentially be developed further into an even more useful TIA data plotting tool.

## ts_diagram.py
Takes TIA data sets and makes a TS diagram with pressure as cmap variable. Experimental and only used for appendix figures.

## vessel_speed.py
Loads NMEA data and calcutes vessel speed. Probably a one time script with little felxibility. Used for one figure in paper.

# Output
This dir is not synced with this repository but needed for some hardcoded savepaths. Create at your wish.

# Input data files
Several scripts need access to input in a variety of formats. Those files will be hosted elsewhere when the paper is published, but it is not yet clear where. Once known, it will be linked here where to download them.

