"""
A collection of tools provinding an interface to create model data sets. Model data sets refers
to 2D data sets representing a slice trough a water column, i.e. succesive vertical profile over
time and space

Author: thomas.kock@hereon.de
"""

import abc
import csv

# dependencies
import numpy
import matplotlib.pyplot as plt
import scipy.io
import scipy.interpolate

# symbolic constants defining mostly the appearance of plots
FIGSIZE = (16,10)
DPI = 100
CMAP = "inferno"

PI = numpy.pi

def make_vertical_profile(
    depth0: float = 0,
    depth1: float = 100,
    depth_step: float = 0.1,
    v_surface: float = 25,
    v_bottom: float = 10,
    vkline_top: float = 20,
    vkline_bottom: float = 60,
    linear_dec: float = 0.1
    ) -> tuple:
    """
    Helper function to create a simple vertical profile of values over depth. The value-kline 
    uses a cosine function for the modelling the values.

    Args:
        depth0 (float, optional): Starting depth in m. Defaults to 0.
        depth1 (float, optional): End depth in m. Defaults to 100.
        depth_step (float, optional): Stepsize for depth in m. Defaults to 0.1.
        v_surface (float, optional): Value at surface, arbitary. Defaults to 25.
        v_bottom (float, optional): Value at bottom of profile, arbitrary. Defaults to 10.
        vkline_top (float, optional): Top depth of value-kline in m. Defaults to 20.
        vkline_bottom (float, optional): Bottom depth of value-kline in m. Defaults to 60.

    Returns:
        tuple: Computed values, depths
    """
    v_drop = v_surface - v_bottom
    depths = numpy.linspace(depth0, depth1, int((depth1 - depth0)/depth_step))
    v = numpy.zeros_like(depths)
    for i, d in enumerate(depths):
        if d < vkline_top:
            v[i] = v_surface
        elif d > vkline_bottom:
            if v_bottom > v_surface:
                v[i] = v_bottom + linear_dec * (d - vkline_bottom)/(depth1 - vkline_bottom)
            else:
                v[i] = v_bottom - linear_dec * (d - vkline_bottom)/(depth1 - vkline_bottom)
        else:
            v[i] = v_drop/2 * numpy.cos((d - vkline_top) * PI / (vkline_bottom - vkline_top)) + v_drop/2 + v_bottom
    return v, depths


class DataModel(abc.ABC):
    """
    Abstract base class for data models, defines which attributes a data model must have
    """
    def __init__(self) -> None:
        """
        Constructor method of model abstract base class
        """
        super().__init__()
        self.X_STEP: float
        self.BASE_TEMP: float
        self.DOMAIN_LENGTH: float
        self.MAX_DEPTH: float
        self.Z_STEP: float
        self.TC_START: float
        self.TC_SIZE: float
        self.TC_DROP: float
        self.CONTROL_PLOT: bool
        self.USE_FILE_TEMPERATURE: bool

        self.z_size: int
        self.x_size: int
        self.x_step: float
        self.distances: numpy.ndarray
        self.z: numpy.ndarray

        self.levels: numpy.ndarray
        self.max_temp: float
        self.min_temp: float
        self.temp_array: numpy.ndarray
    
    @abc.abstractmethod
    def create_var_array_from_profile_file(self, fn: str):
        """
        Abstract method: Each data model must have a method to create a data array from input file

        Args:
            fn (str): input file path
        """
        pass

    @abc.abstractmethod
    def create_var_array_from_linear(self):
        """
        Creates a data field from user input based on a linear temperature drop
        """
        pass

    def save_csv(self, outpath: str) -> None:

        with open(outpath, "w") as fobj:
            cw = csv.writer(fobj)
            for n in range(self.x_size):
                cw.writerow(self.temp_array[n])
        print(f"Wrote csv output to {outpath}")

class Model_InternalWave(DataModel):
    """
    Model class for simulating an internal wave in sine form somewhere in the model domain.

    Args:
        DataModel (__main__.DataModel): Model class is derived from ABC DataModel of this module.
    """

    def __init__(self, 
        x_step: float=5, 
        z_step: float=0.1,
        base_temp: float=20,
        domain_length: float=2000,
        max_depth: float=30,
        tc_start: float=12,
        tc_size: float=15,
        tc_drop: float=5,
        ) -> None:
        """
        Constructor method

        Args:
            x_step (float, optional): Defines the resolution in horizontal direction in m. Defaults to 5.
            z_step (float, optional): Defines the resolution in vertical direction in m. Defaults to 0.1.
            base_temp (float, optional): Temperature profiles base temp at the surface in °C. Defaults to 20.
            domain_length (float, optional): Length of Model Domain in m. Defaults to 2000.
            max_depth (float, optional): Depth of model domain in m. Defaults to 30.
            tc_start (float, optional): For linear profile only: Thermocline start depth in m. Defaults to 12.
            tc_size (float, optional): For linear profile only: Thermocline depth span in m. Defaults to 15.
            tc_drop (float, optional): For linear profile only: Thermocline temperature drop in °C. Defaults to 5.
        """

        super().__init__()
        # MODEL BASE CONSTANTS
        self.X_STEP = x_step
        self.BASE_TEMP = base_temp
        self.DOMAIN_LENGTH = domain_length
        self.MAX_DEPTH = max_depth
        self.Z_STEP = z_step
        self.TC_START = tc_start
        self.TC_SIZE = tc_size
        self.TC_DROP = tc_drop
        self.USE_FILE_TEMPERATURE = False
        # Compute model sizes etc.
        print("Setting up Model parameters")
        self.z_size = int(self.MAX_DEPTH / self.Z_STEP)
        self.x_size = int(self.DOMAIN_LENGTH / self.X_STEP)
        print(f"Model dims are:")
        print(f"dim_x: {self.x_size}")
        print(f"dim_z: {self.z_size}")
        print(f"x_step: {self.X_STEP}")
        print(f"z_step: {self.Z_STEP}")
        print(f"Domain length is: {self.DOMAIN_LENGTH}")
        # Compute model arrays
        self.distances = numpy.arange(0, self.DOMAIN_LENGTH, step=self.X_STEP, dtype="f2")
        self.z = numpy.arange(0, self.MAX_DEPTH, step=self.Z_STEP, dtype="f2")

    def create_var_array_from_profile_file(self, fn: str):
        """
        Import a temperature profile from data file. This method expects the file to be in matlab format and provide
        a dict of values. Expected keys are "P_up" and "T_up"

        Args:
            fn (str): Absolute path to file
        """
        print("Using temperature profile date from file")
        self.USE_FILE_TEMPERATURE = True
        self.fn = fn
        print(f"File: {self.fn}")
        data = scipy.io.loadmat(self.fn)
        data_temp_profile = numpy.stack((data["P_up"][0], data["T_up"][0]), axis=0)
        self.f_real_temp_profile_interp = scipy.interpolate.interp1d(
                data_temp_profile[0], data_temp_profile[1], kind="linear", fill_value="extrapolate"
                )
        self.temp_profile = self.f_real_temp_profile_interp(self.z)
        self.max_temp = int(self.temp_profile.max())
        self.min_temp = int(self.temp_profile.min())
        self.levels = numpy.arange(self.min_temp, self.max_temp, step=0.01, dtype="f4")
        self.temp_array = numpy.tile(self.temp_profile, self.x_size).reshape(self.x_size, self.z_size)
    
    def create_var_array_from_linear(self):
        """
        Creates a temperature profile from tc base constants given in the class constructor and creates an array of profiles
        """
        print(f"Using modelled linear temperature drop")
        self.temp_profile = numpy.zeros_like(self.z, dtype="f4")
        tc_start_idx = int(self.TC_START / self.Z_STEP)
        tc_end_idx = int((self.TC_START + self.TC_SIZE) / self.Z_STEP)
        self.temp_profile[:tc_start_idx] = self.BASE_TEMP
        self.temp_profile[tc_start_idx:tc_end_idx] = numpy.linspace(self.BASE_TEMP, self.BASE_TEMP - self.TC_DROP, tc_end_idx - tc_start_idx)
        self.temp_profile[tc_end_idx:] = self.BASE_TEMP - self.TC_DROP
        self.temp_array = numpy.tile(self.temp_profile, self.x_size).reshape((self.x_size, self.z_size))
        self.levels = numpy.arange(self.BASE_TEMP - self.TC_DROP, self.BASE_TEMP, step=0.1)
        self.max_temp = self.BASE_TEMP
        self.min_temp = self.BASE_TEMP - self.TC_DROP

    def compute_internal_wave(self, wave_height: float=10, wave_length: float=800, wave_center: float=1000):
        """
        Add an internal wave to the data field. This method is stackable, you can create several waves in one data array

        Args:
            wave_height (float, optional): [description]. Defaults to 10.
            wave_length (float, optional): [description]. Defaults to 800.
            wave_center (float, optional): [description]. Defaults to 1000.
        """
        self.MAX_WAVE = wave_height # m, maximum internal wave height
        self.FEATURE_SIZE = wave_length # m, horizontal
        self.FEATURE_CENTER = wave_center # m, where the feature is centered
        self.max_wave_idx = int(self.MAX_WAVE / self.Z_STEP)
        # compute internal wave, i.e. shift temp profile up
        print("Computing wave like feature in temperature data")
        print(f"Feature x_position: {self.FEATURE_CENTER}")
        print(f"Feature amplitude: {self.MAX_WAVE}")
        print(f"Feature x_width: {self.FEATURE_SIZE}")
        feature_start_idx = int(self.FEATURE_CENTER / self.X_STEP - self.FEATURE_SIZE / self.X_STEP)
        feature_end_idx = int(self.FEATURE_CENTER / self.X_STEP + self.FEATURE_SIZE / self.X_STEP)
        shift_weight = numpy.sin(numpy.arange(-PI/2, 3*PI/2, 2*PI/(feature_end_idx - feature_start_idx))) + 1
        for i in range(feature_start_idx, feature_end_idx, 1):
            shift = int(self.MAX_WAVE * shift_weight[i - feature_start_idx] / self.Z_STEP)
            if shift:
                x = self.temp_array[i]
                if shift > 0:
                    x = numpy.insert(x, -1, [self.temp_array[0, -1]] * shift)
                    self.temp_array[i] = x[shift:]
                elif shift < 0:
                    x = numpy.insert(x, 0, [self.temp_array[0, 0]] * abs(shift))
                    self.temp_array[i] = x[:shift]
                else:
                    pass

    def add_noise_to_var_array(self, noise_amp: float=0.1):
        """
        Adds random noise to all samples of model data. Note that self.temp_array must exist a priori

        Args:
            noise_amp (float, optional): Scales the noise. Defaults to 0.1.
        """
        print(f"Adding random noise to data array in range +-{0.5*noise_amp:2f} ... ", end="")
        self.temp_array = self.temp_array + ((numpy.random.random_sample(size = (self.x_size, self.z_size)) - 0.5) * noise_amp)
        print("done")

    def plot_model_data(self, figsize=FIGSIZE, dpi=DPI, cmap=CMAP):
        """
        Simple control plot of the modelled data field

        Args:
            figsize (tuple, optional): Tuple of figure width and height in inch. Defaults to FIGSIZE.
            dpi (int, optional): DPI of figure. Defaults to DPI.
        """
        print(f"Making a control plot of model data, this may take a while ... ", end="")
        fig, ax = plt.subplots(1, 1, num="MODEL 1", figsize=figsize, dpi=dpi)
        ax.contourf(self.distances, self.z, self.temp_array.T, cmap=cmap, 
            vmax= self.max_temp, vmin = self.min_temp,
            levels = self.levels,
            extend="both")
        ax.set_ylim([0, self.MAX_DEPTH])
        ax.invert_yaxis()
        ax.set_ylabel("Depth [m]")
        ax.set_xlabel("Distance [m]")
        ax.set_title("Modelled data, arbitrary units")
        print("done")
        return fig
    
    def plot_model_profile(self):
        """
        Convenience method to display the used temperatue profile
        """
        fig2, ax2 = plt.subplots(1,1,num ="Temp Profile 2")
        ax2.plot(self.temp_profile, self.z)
        ax2.set_ylim([0, self.MAX_DEPTH])
        ax2.invert_yaxis()
        return fig2

    def get_model_data(self):
        """
        Convenience method to extract the modelled data array for external use

        Raises:
            AttributeError: Raised if no temp_array is available

        Returns:
            numpy.ndarray: Data array of the model instance
        """
        if "model_temp_array" in self.__dir__():
            return self.temp_array
        else:
            raise AttributeError(f"{self.__name__} has no attribute 'temp_array'")

class Model_SingleFront(DataModel):
    """
    Model class for simulating a single front in sine form somewhere in the model domain.

    Args:
        DataModel (__main__.DataModel): Model class is derived from ABC DataModel of this module.
    """

    def __init__(self, 
        x_step: float=5, 
        z_step: float=0.1,
        base_temp: float=20,
        domain_length: float=2000,
        max_depth: float=30,
        tc_start: float=12,
        tc_size: float=15,
        tc_drop: float=5,
        ) -> None:
        """
        Constructor method

        Args:
            x_step (float, optional): Defines the resolution in horizontal direction in m. Defaults to 5.
            z_step (float, optional): Defines the resolution in vertical direction in m. Defaults to 0.1.
            base_temp (float, optional): Temperature profiles base temp at the surface in °C. Defaults to 20.
            domain_length (float, optional): Length of Model Domain in m. Defaults to 2000.
            max_depth (float, optional): Depth of model domain in m. Defaults to 30.
            tc_start (float, optional): For linear profile only: Thermocline start depth in m. Defaults to 12.
            tc_size (float, optional): For linear profile only: Thermocline depth span in m. Defaults to 15.
            tc_drop (float, optional): For linear profile only: Thermocline temperature drop in °C. Defaults to 5.
        """

        super().__init__()
        # MODEL BASE CONSTANTS
        self.X_STEP = x_step
        self.BASE_TEMP = base_temp
        self.DOMAIN_LENGTH = domain_length
        self.MAX_DEPTH = max_depth
        self.Z_STEP = z_step
        self.TC_START = tc_start
        self.TC_SIZE = tc_size
        self.TC_DROP = tc_drop
        self.USE_FILE_TEMPERATURE = False
        # Compute model sizes etc.
        print("Setting up Model parameters")
        self.z_size = int(self.MAX_DEPTH / self.Z_STEP)
        self.x_size = int(self.DOMAIN_LENGTH / self.X_STEP)
        print(f"Model dims are:")
        print(f"dim_x: {self.x_size}")
        print(f"dim_z: {self.z_size}")
        print(f"x_step: {self.X_STEP}")
        print(f"z_step: {self.Z_STEP}")
        print(f"Domain length is: {self.DOMAIN_LENGTH}")
        # Compute model arrays
        self.distances = numpy.arange(0, self.DOMAIN_LENGTH, step=self.X_STEP, dtype="f2")
        self.z = numpy.arange(0, self.MAX_DEPTH, step=self.Z_STEP, dtype="f2")

    def create_var_array_from_profile_file(self, fn: str):
        """
        Import a temperature profile from data file. This method expects the file to be in matlab format and provide
        a dict of values. Expected keys are "P_up" and "T_up"

        Args:
            fn (str): Absolute path to file
        """
        print("Using temperature profile date from file")
        self.USE_FILE_TEMPERATURE = True
        self.fn = fn
        print(f"File: {self.fn}")
        data = scipy.io.loadmat(self.fn)
        data_temp_profile = numpy.stack((data["P_up"][0], data["T_up"][0]), axis=0)
        self.f_real_temp_profile_interp = scipy.interpolate.interp1d(
                data_temp_profile[0], data_temp_profile[1], kind="linear", fill_value="extrapolate"
                )
        self.temp_profile = self.f_real_temp_profile_interp(self.z)
        self.max_temp = int(self.temp_profile.max())
        self.min_temp = int(self.temp_profile.min())
        self.levels = numpy.arange(self.min_temp, self.max_temp, step=0.01, dtype="f4")
        self.temp_array = numpy.tile(self.temp_profile, self.x_size).reshape(self.x_size, self.z_size)
    
    def create_var_array_from_linear(self):
        """
        Creates a temperature profile from tc base constants given in the class constructor and creates an array of profiles
        """
        print(f"Using modelled linear temperature drop")
        self.temp_profile = numpy.zeros_like(self.z, dtype="f4")
        tc_start_idx = int(self.TC_START / self.Z_STEP)
        tc_end_idx = int((self.TC_START + self.TC_SIZE) / self.Z_STEP)
        self.temp_profile[:tc_start_idx] = self.BASE_TEMP
        self.temp_profile[tc_start_idx:tc_end_idx] = numpy.linspace(self.BASE_TEMP, self.BASE_TEMP - self.TC_DROP, tc_end_idx - tc_start_idx)
        self.temp_profile[tc_end_idx:] = self.BASE_TEMP - self.TC_DROP
        self.temp_array = numpy.tile(self.temp_profile, self.x_size).reshape((self.x_size, self.z_size))
        self.levels = numpy.arange(self.BASE_TEMP - self.TC_DROP, self.BASE_TEMP, step=0.1)
        self.max_temp = self.BASE_TEMP
        self.min_temp = self.BASE_TEMP - self.TC_DROP

    def create_var_array_from_function(self):
        """
        Creates a temperature profile from tc base constants given in the class constructor and creates an array of profiles
        """
        print(f"Using modelled linear temperature drop")


        self.temp_profile, depths = make_vertical_profile(
            depth0 = 0,
            depth1 = self.MAX_DEPTH,
            depth_step = self.Z_STEP,
            v_surface = self.BASE_TEMP,
            v_bottom = self.BASE_TEMP - self.TC_DROP,
            vkline_top = self.TC_START,
            vkline_bottom = self.TC_START + self.TC_SIZE
        )
        self.temp_array = numpy.tile(self.temp_profile, self.x_size).reshape((self.x_size, self.z_size))
        if self.TC_DROP < 0:
            self.levels = numpy.arange(self.BASE_TEMP, self.BASE_TEMP - self.TC_DROP, step=0.1)
            self.max_temp = self.BASE_TEMP - self.TC_DROP
            self.min_temp = self.BASE_TEMP
        else:
            self.levels = numpy.arange(self.BASE_TEMP - self.TC_DROP, self.BASE_TEMP, step=0.1)
            self.max_temp = self.BASE_TEMP
            self.min_temp = self.BASE_TEMP - self.TC_DROP

    def plot_model_profile(self):
        """
        Convenience method to display the used temperatue profile
        """
        fig2, ax2 = plt.subplots(1,1,num ="Temp Profile 2")
        ax2.plot(self.temp_profile, self.z)
        ax2.set_ylim([0, self.MAX_DEPTH])
        ax2.invert_yaxis()
        return fig2

    def plot_model_data(self, figsize=FIGSIZE, dpi=DPI, cmap=CMAP):
        """
        Simple control plot of the modelled data field

        Args:
            figsize (tuple, optional): Tuple of figure width and height in inch. Defaults to FIGSIZE.
            dpi (int, optional): DPI of figure. Defaults to DPI.
        """
        print(f"Making a control plot of model data, this may take a while ... ", end="")
        fig, ax = plt.subplots(1, 1, num="MODEL 1", figsize=figsize, dpi=dpi)
        ax.contourf(self.distances, self.z, self.temp_array.T, cmap=cmap, 
            vmax=self.max_temp, vmin = self.min_temp,
            levels = self.levels,
            extend="both")
        ax.set_ylim([0, self.MAX_DEPTH])
        ax.invert_yaxis()
        ax.set_ylabel("Depth [m]")
        ax.set_xlabel("Distance [m]")
        ax.set_title("Modelled data, arbitrary units")
        print("done")
        return fig

    def add_frontal_feature(self, front_center: float, front_depth_displacement: float, front_length: float):
        """
        Adds a frontal feature to modelled data field. A front in this sense is an area where
        the vertical temperature (or generally data) profile is lifted or lowered by a certain
        value: front_depth_displacement [m].


        Args:
            front_center (float): Number in m where the center of the front is located
            front_depth_displacement (float): Vertical displacement in m of the thermocline at the front
            front_length (float): Horizontal length in m of the front, defines the slope.
        """
        if front_center - front_length / 2 < 0:
            raise ValueError(
                "\nFront center must be at least half a front length away from domain start.\n"
                f" Currently the front would start at {(front_center - front_length / 2):.2f}"
                )
        if front_center + front_length / 2 > self.distances[-1]:
            raise ValueError(
                "\nFront center must be at least half a front length away from domain end.\n"
                f"Currently the front would end at {(front_center + front_length / 2):.2f}\n"
                f"The domain is only {self.distances[-1]} long."
                )
        self.FEATURE_CENTER = front_center
        self.FEATURE_SIZE = front_length
        if front_depth_displacement < 0:
            UP = False
        else:
            UP = True
        # find the start idx
        x_start = front_center - front_length/2
        i = 0
        while x_start > self.distances[i]:
            i += 1
        idx0 = i
        print(f"front starts at idx0={idx0} -> x0={self.distances[idx0]}")
        # find end index
        x_end = front_center + front_length/2
        i = 0
        while x_end > self.distances[i]:
            i += 1
        idx1 = i
        print(f"front ends at idx1={idx1} -> x1={self.distances[idx1]}")
        # length in idx for displacement curve
        delta_idx = idx1 - idx0
        # define a cosine function for displacement shape
        # using digitize converts the float values into number of z_bins to shift the data of
        # the vertical profile up/down
        shift_idx = numpy.digitize(
            -0.5 * abs(front_depth_displacement)
            * numpy.cos(numpy.arange(delta_idx) * PI / delta_idx)
            + abs(front_depth_displacement) / 2,
            numpy.arange(0, abs(front_depth_displacement), self.Z_STEP),
            right=True
            )
        # currently this only supports the upward shift
        for i, shift in zip(range(idx0, idx1, 1), shift_idx):
            if not shift:
                continue
            if UP:
                self.temp_array[i, :-shift] = self.temp_profile[shift:]
                self.temp_array[i, -shift:] = self.temp_profile[-1]
            else:
                self.temp_array[i, shift:] = self.temp_profile[:-shift]
                self.temp_array[i, :shift] = self.temp_profile[0]
        # handling the rest of the model domain
        if UP:
            self.temp_array[i+1:] = self.temp_array[i]
        else:
            self.temp_array[i+1:] = self.temp_array[i]


if __name__ == "__main__":
    sf = Model_SingleFront(max_depth=100)
    # sf.create_var_array_from_profile_file("/home/thomas/devl/tc2013/Data/TIA_20110414_part1.mat")
    sf.create_var_array_from_function()
    sf.add_frontal_feature(1000, 15, 300)
    sf.plot_model_profile()
    sf.plot_model_data(cmap="viridis")
    plt.show()

    # v, d = make_vertical_profile(v_surface=1020, v_bottom=1030)
    # plt.plot(v, d)
    # plt.gca().invert_yaxis()
    # plt.show()